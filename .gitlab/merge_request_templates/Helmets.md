Please make sure you have read our [Contributing Guidelines](https://gitlab.com/501st-legion-starsim/aux-mod-team/501st-Aux-Mod/-/blob/development/CONTRIBUTING.md) and understand the [Testing Guidelines](https://gitlab.com/501st-legion-starsim/aux-mod-team/501st-Aux-Mod/-/blob/development/CONTRIBUTING.md#testing) before creating a merge request.

A merge request specifically for custom helmets.

# Helmets
- [ ] (Check me when reviewed!) Closes #helm-issue
- [ ] Closes #other-helm-issue

# Notes
Additional Notes

/label ~feature ~helmets
