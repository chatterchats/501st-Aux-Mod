Please make sure you have read our [Contributing Guidelines](https://gitlab.com/501st-legion-starsim/aux-mod-team/501st-Aux-Mod/-/blob/development/CONTRIBUTING.md) and understand the [Testing Guidelines](https://gitlab.com/501st-legion-starsim/aux-mod-team/501st-Aux-Mod/-/blob/development/CONTRIBUTING.md#testing) before creating a merge request.

# Description
The feature(s) that are being implemented.

# Details
What issues are being closed. If there are no issues to close, detail what is being implemented.
- Including
- More
- Detail
- Is
- Better

# Testing
A full set of instructions on how to test this branch. Keep it simple so others can understand how to test your changes.

# Videos/Images
Any media you have to show the features for this branch.

# Notes
Additional Notes.

/label ~feature