class cfgPatches
{
    class Aux501_Patch_grenades_special_bananas
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = {};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_Bruhnana",
            "Aux501_Weapons_Mags_Joeynana",
            "Aux501_Weapons_Mags_Copenana"
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_Thermal_Detonator;
    
    class Aux501_Weapons_Mags_Bruhnana: Aux501_Weapons_Mags_Thermal_Detonator
    {
        displayName = "Bruhnana";
        displayNameShort = "Bruhnana";
        descriptionShort = "Clone Banana";
        picture = "\z\ace\addons\common\data\icon_banana_ca.paa";
        ammo = "Aux501_Weapons_Ammo_Grenades_Bruhnana";
        model = "\kobra\442_weapons\explosive\thermal_det.p3d";
    };
    class Aux501_Weapons_Mags_Joeynana: Aux501_Weapons_Mags_Bruhnana
    {
        scope = 0;
        displayName = "Joeynana";
        displayNameShort = "Joeynana";
        descriptionShort = "Cocksucka Banana";
        ammo = "Aux501_Weapons_Ammo_Grenades_Joeynana";
    };
    class Aux501_Weapons_Mags_Copenana: Aux501_Weapons_Mags_Bruhnana
    {
        displayName = "Copenana";
        displayNameShort = "Copenana";
        descriptionShort = "Cocksucka Banana";
        ammo = "Aux501_Weapons_Ammo_Grenades_Copenana";
    };
};

class CfgAmmo
{
    class Grenade;

    class Aux501_Weapons_Ammo_Grenades_Bruhnana: Grenade
    {
        simulation = "shotShell";
        soundFly[] = {"\Aux501\Weapons\Grenades\data\sounds\ohmygod.ogg",800,1,500};
        SoundSetExplosion[] = {"Aux501_soundset_Bruhnana_exp"};
        model = "\z\ace\addons\common\data\banana.p3d";
        explosionTime = 1.5;
        hit = 0.1;
        indirectHit = 0.1;
        indirectHitRange = 0.1;
        timetolive = 59;
    };
    class Aux501_Weapons_Ammo_Grenades_Copenana: Aux501_Weapons_Ammo_Grenades_Bruhnana
    {
        soundFly[] = {"Aux501\Weapons\Grenades\data\sounds\copetune.wss",800,1,500};
        SoundSetExplosion[] = {"Aux501_soundset_Copenana_exp"};
        explosionTime = 28.35;
    };
};

class CfgSoundShaders
{
    class Aux501_SoundShader_Grenade_Bruhnana
    {
        samples[] = 
        {
            {"Aux501\Weapons\Grenades\data\sounds\bruh.wss",1}
        };
        volume = 8;
        range = 800;
    };
    class Aux501_SoundShader_Grenade_copenana
    {
        samples[] = 
        {
            {"Aux501\Weapons\Grenades\data\sounds\HobCope.wss",1},
            {"Aux501\Weapons\Grenades\data\sounds\M0rkCope.wss",1}
        };
        volume = 3;
        range = 800;
    };
};

class CfgSoundSets
{
    class Aux501_soundset_Bruhnana_exp
    {
        soundShaders[]=
        {
            "Aux501_SoundShader_Grenade_Bruhnana"
        };
        volumeFactor = 1;
        volumeCurve="InverseSquare2Curve";
        spatial = 1;
        doppler = 0;
        loop = 0;
        sound3DProcessingType="ExplosionLight3DProcessingType";
        distanceFilter="explosionDistanceFreqAttenuationFilter";
    };
    class Aux501_soundset_Copenana_exp: Aux501_soundset_Bruhnana_exp
    {
        soundShaders[]=
        {
            "Aux501_SoundShader_Grenade_copenana"
        };
    };
};