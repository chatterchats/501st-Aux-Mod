/*
 * Author: M3ales
 * Description: Plays an end sound on an object, and cleans up any existing loop sound source.
 *
 * Input:
 * _object - The object on which the sound will be played (Object)
 * _endSound - The sound to be played (String)
 * _endDuration - The duration of the end sound (Number)
 * _distance - The distance at which the sound will be audible (Number)
 *
 * Usage:
 * [_object, _endSound, _endDuration, _distance] call fnc_endSound;
 *
 * Example:
 * [myObject, "myEndSound", 5, 100] call fnc_endSoundLocal;
 */

#include "function_macros.hpp"

params["_object", "_endSound", "_endDuration", "_distance"];

if(!hasInterface) exitWith {
	LOG("No Interface to play sounds from");
};

if(isNil "_object" || !alive _object) exitWith {
	LOG("No target object to play remote sound");
};

private _currentLoopSource = _object getVariable [QGVAR(loopSoundSource), objNull];
if(!(isNil "_currentLoopSource") && !(_currentLoopSource isEqualTo objNull)) then {
	LOG("Current Loop Source Exists, Deleting");
	deleteVehicle _currentLoopSource;
	_object setVariable [QGVAR(loopSoundSource), objNull, false];
};

private _currentSource = "#dynamicsound" createVehicleLocal ASLToAGL getPosWorld _object;
_currentSource attachTo [_object, [0, 0, 0]];

[_currentSource, player] say3D [_endSound, _distance, 1, false];
[
	{
		params["_currentSource"];
		detach _currentSource;
		deleteVehicle _currentSource;
	},
	[_currentSource],
	(_endDuration + 1)
] call CBA_fnc_waitAndExecute;