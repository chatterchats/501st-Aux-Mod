class cfgPatches
{
    class Aux501_Patch_grenades_special
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[] =
        {
            "Aux501_Patch_Weapons",
            "ace_interact_menu",
            "cba_settings"
        };
        units[] = {};
        weapons[] = {};
        magazines[] = {};
    };
};