class cfgPatches
{
    class Aux501_Patch_grenades
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[] =
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = {};
    };
};

class CfgWeapons
{
    class Default;
    class GrenadeLauncher: Default
    {
        class ThrowMuzzle;
    };
    class Throw: GrenadeLauncher
    {
        muzzles[] += 
        {
            "Aux501_Weaps_Thermal_Detonator_ThrowMuzzle",
            "Aux501_Weaps_Thermal_Imploder_ThrowMuzzle",
            "Aux501_Weaps_Thermal_Impacter_ThrowMuzzle",
            "Aux501_Weaps_Ctype_ThrowMuzzle",
            "Aux501_Weaps_CISDetonator_ThrowMuzzle",
            "Aux501_Weaps_Flashnade_ThrowMuzzle",
            "Aux501_Weaps_smoke_white_ThrowMuzzle",
            "Aux501_Weaps_smoke_purple_ThrowMuzzle",
            "Aux501_Weaps_smoke_yellow_ThrowMuzzle",
            "Aux501_Weaps_smoke_red_ThrowMuzzle",
            "Aux501_Weaps_smoke_green_ThrowMuzzle",
            "Aux501_Weaps_smoke_blue_ThrowMuzzle",
            "Aux501_Weaps_smoke_orange_ThrowMuzzle",
            "Aux501_Weaps_bruhnana_ThrowMuzzle",
            "Aux501_Weaps_copenana_ThrowMuzzle",
            "Aux501_Weaps_Shield_Squad_ThrowMuzzle",
            "Aux501_Weaps_Shield_Trench_ThrowMuzzle",
            "Aux501_Weaps_Shield_Personal_ThrowMuzzle",
            "Aux501_Weaps_smoke_mash_ThrowMuzzle"
        };
        class Aux501_Weaps_Thermal_Detonator_ThrowMuzzle: ThrowMuzzle
        {
            magazines[] = 
            {
                "Aux501_Weapons_Mags_Thermal_Detonator"
            };
        };
        class Aux501_Weaps_Thermal_Imploder_ThrowMuzzle: ThrowMuzzle
        {
            magazines[] = 
            {
                "Aux501_Weapons_Mags_Thermal_Imploder"
            };
        };
        class Aux501_Weaps_Thermal_Impacter_ThrowMuzzle: ThrowMuzzle
        {
            magazines[] = 
            {
                "Aux501_Weapons_Mags_Thermal_Impacter"
            };
        };
        class Aux501_Weaps_Ctype_ThrowMuzzle: ThrowMuzzle
        {
            magazines[] = 
            {
                "Aux501_Weapons_Mags_Ctype"
            };
        };
        class Aux501_Weaps_CISDetonator_ThrowMuzzle: ThrowMuzzle
        {
            magazines[] = 
            {
                "Aux501_Weapons_Mags_CISDetonator"
            };
        };
        class Aux501_Weaps_Flashnade_ThrowMuzzle: ThrowMuzzle
        {
            magazines[] = 
            {
                "Aux501_Weapons_Mags_flashnade"
            };
        };
        class Aux501_Weaps_smoke_white_ThrowMuzzle: ThrowMuzzle
        {
            magazines[] = 
            {
                "Aux501_Weapons_Mags_Smoke_White"
            };
        };
        class Aux501_Weaps_smoke_purple_ThrowMuzzle: ThrowMuzzle
        {
            magazines[] = 
            {
                "Aux501_Weapons_Mags_Smoke_Purple"
            };
        };
        class Aux501_Weaps_smoke_yellow_ThrowMuzzle: ThrowMuzzle
        {
            magazines[] = 
            {
                "Aux501_Weapons_Mags_Smoke_Yellow"
            };
        };
        class Aux501_Weaps_smoke_red_ThrowMuzzle: ThrowMuzzle
        {
            magazines[] = 
            {
                "Aux501_Weapons_Mags_Smoke_Red"
            };
        };
        class Aux501_Weaps_smoke_green_ThrowMuzzle: ThrowMuzzle
        {
            magazines[] = 
            {
                "Aux501_Weapons_Mags_Smoke_Green"
            };
        };
        class Aux501_Weaps_smoke_blue_ThrowMuzzle: ThrowMuzzle
        {
            magazines[] = 
            {
                "Aux501_Weapons_Mags_Smoke_Blue"
            };
        };
        class Aux501_Weaps_smoke_orange_ThrowMuzzle: ThrowMuzzle
        {
            magazines[] = 
            {
                "Aux501_Weapons_Mags_Smoke_Orange"
            };
        };
        class Aux501_Weaps_bruhnana_ThrowMuzzle: ThrowMuzzle
        {
            magazines[] = 
            {
                "Aux501_Weapons_Mags_Bruhnana"
            };
        };
        class Aux501_Weaps_copenana_ThrowMuzzle: ThrowMuzzle
        {
            magazines[] = 
            {
                "Aux501_Weapons_Mags_Copenana"
            };
        };
        class Aux501_Weaps_Shield_Squad_ThrowMuzzle: ThrowMuzzle
        {
            magazines[] = 
            {
                "Aux501_Weapons_Mags_Grenades_Squad_Shield"
            };
        };
        class Aux501_Weaps_Shield_Trench_ThrowMuzzle: ThrowMuzzle
        {
            magazines[] = 
            {
                "Aux501_Weapons_Mags_Grenades_Trench_Shield"
            };
        };
        class Aux501_Weaps_Shield_Personal_ThrowMuzzle: ThrowMuzzle
        {
            magazines[] = 
            {
                "Aux501_Weapons_Mags_Grenades_Shield_Personal"
            };
        };
        class Aux501_Weaps_smoke_mash_ThrowMuzzle: ThrowMuzzle
        {
            magazines[] = 
            {
                "Aux501_Weapons_Mags_Smoke_MASH"
            };
        };
    };
};