class cfgPatches
{
    class Aux501_Patch_Valken38X
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_cows_DMS_TI",
            "Aux501_cows_DMS_2_TI",
            "Aux501_cows_DMS_3_TI",
            "Aux501_cows_DMS_4_TI",
            "Aux501_cows_DMS_5_TI",

            "Aux501_Weaps_Valken38X",
            "Aux501_Weaps_Valken38Y"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_30mw10"
        };
        ammo[] = 
        {
            "Aux501_Weapons_Ammo_30mw"
        };
    };
};

class CowsSlot;
class MuzzleSlot;
class PointerSlot;
class UnderBarrelSlot;

class cfgWeapons
{
    class ItemCore;
    class InventoryOpticsItem_Base_F;
    class optic_DMS: ItemCore
    {
        class ItemInfo;
    };

    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class Single;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_Valken38X: Aux501_rifle_base_stunless
    {
        scope = 2;
        displayName = "[501st] Valken38X";
        baseWeapon = "Aux501_Weaps_Valken38X";
        picture = "\Aux501\Weapons\Republic\Valken38x\data\textures\UI\valken_ui_ca.paa";
        model = "\Aux501\Weapons\Republic\Valken38x\Aux501_valken38x.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\IBL\weapons\DLT19\anims\DLT19_handanim.rtm"};
        recoil = "3AS_recoil_DC15A";
        reloadAction = "ReloadMagazine";
        maxRecoilSway = 0;
        swayDecaySpeed = 0;
        inertia = 0.5;
        dexterity = 1.5;
        initSpeed = -1;
        maxZeroing = 2500;
        magazines[] =
        {
            "Aux501_Weapons_Mags_20mwdp30", 
            "Aux501_Weapons_Mags_30mw10"
        };
        modes[] = {"Single","aicqb","aiclose","aimedium","aifar","aiopticmode1","aiopticmode2"};
        class Single: Single
        {
            reloadTime = 0.2;		
            dispersion = 0.0006;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "DefaultRifle";
                begin1[] = {"kobra\442_weapons\sounds\sniper\sniper1.wss",+3db,1,2200};
                soundBegin[] = {"begin1",1};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "DefaultRifle";
                begin1[] = {"kobra\442_weapons\sounds\sniper\sniper1.wss",+3db,1,2200};
                soundBegin[] = {"begin1",1};
            };
        };
        class aicqb: Single
        {
            showToPlayer = 0;
            dispersion = 0.00073;
            minRange = 25;
            minRangeProbab = 1;
            midRange = 50;
            midRangeProbab = 1;
            maxRange = 100;
            maxRangeProbab = 0.5;
            aiRateOfFire = 0.1;
            aiRateOfFireDistance = 50;
        };
        class aiclose: aicqb
        {
            minRange = 50;
            minRangeProbab = 0.5;
            midRange = 150;
            midRangeProbab = 1;
            maxRange = 250;
            maxRangeProbably = 0.5;
            aiRateOfFireDistance = 150;
        };
        class aimedium: aicqb
        {
            minRange = 150;
            minRangeProbab = 0.5;
            midRange = 250;
            midRangeProbab = 1;
            maxRange = 350;
            maxRangeProbab = 0.1;
            aiRateOfFireDistance = 250;
            requiredOpticType = 0;
        };
        class aifar: aicqb
        {
            minRange = 250;
            minRangeProbab = 0.5;
            midRange = 350;
            midRangeProbab = 1;
            maxRange = 600;
            maxRangeProbab = 0.5;
            aiRateOfFireDistance = 350;
            requiredOpticType = 0;
        };
        class aiopticmode1: aicqb
        {
            minRange = 400;
            minRangeProbab = 0.5;
            midRange = 500;
            midRangeProbab = 1;
            maxRange = 700;
            maxRangeProbab = 0.5;
            aiRateOfFireDistance = 500;
            requiredOpticType = 1;
        };
        class aiopticmode2: aicqb
        {
            minRange = 500;
            minRangeProbab = 0.5;
            midRange = 700;
            midRangeProbab = 1;
            maxRange = 900;
            maxRangeProbab = 0.5;
            aiRateOfFireDistance = 700;
            requiredOpticType = 1;
        };
        class OpticsModes
        {
            class sight
            {
                opticsID = 1;
                useModelOptics = 1;
                opticsPPEffects[] = {""};
                opticsDisablePeripherialVision = 0;
                opticsZoomMin = 0.25;
                opticsZoomMax = 0.5;
                opticsZoomInit = 0.75;
                discreteInitIndex = 0;
                distanceZoomMin = 200;
                distanceZoomMax = 200;
                memoryPointCamera = "eye";
                visionMode[] = {};
                opticsFlare = "false";
                cameraDir = "";
            };
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                scope = 0;
                compatibleItems[] = 
                {
                    "Aux501_cows_DMS_TI",
                    "Aux501_cows_DMS_2_TI",
                    "Aux501_cows_DMS_3_TI",
                    "Aux501_cows_DMS_4_TI",
                    "Aux501_cows_DMS_5_TI"
                };
            };
            class MuzzleSlot: MuzzleSlot
            {
                linkProxy = "\A3\data_f\proxies\weapon_slots\MUZZLE";
                displayName = "$str_a3_cfgweapons_abr_base_f_weaponslotsinfo_muzzleslot0";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint = "Center";
                iconPosition[] = {0.24,0.35};
                iconScale = 0.2;
                compatibleItems[] =
                {
                    "Aux501_muzzle_flash"
                };
            };
            class PointerSlot: PointerSlot
            {
                linkProxy = "\A3\data_f\proxies\weapon_slots\SIDE";
                displayName = "Pointer Slot";
                compatibleItems[] = 
                {
                    "acc_flashlight",
                    "acc_pointer_IR"
                }; 
            };
        };
    };
    class Aux501_Weaps_Valken38Y: Aux501_Weaps_Valken38X
    {
        scope = 2;
        displayName = "[501st] Valken38Y";
        baseWeapon = "Aux501_Weaps_Valken38Y";
        picture = "\MRC\JLTS\weapons\DW32S\data\ui\DW32S_ui_ca.paa";
        model = "\MRC\JLTS\weapons\DW32S\DW32S.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\weapons\DW32S\data\DW32S_co.paa"};
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\DW32S\anims\DW32S_handanim.rtm"};
        class GunParticles
        {
            class FirstEffect
            {
                directionName = "Konec hlavne";
                effectName = "RifleAssaultCloud";
                positionName = "Usti hlavne";
            };
        };
    };

    //Optics
    class Aux501_cows_DMS_TI: optic_DMS
    {
        author = "501st Aux Team";
        scope = 2;
        displayName = "DMS LR 6-12x Scope A";
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_lr_01_ca.paa";
        model = "\Aux501\Weapons\Republic\Valken38x\Aux501_ValkenScope.p3d";
        descriptionShort = "Long Range Scope";
        inertia = 0.2;
        class ItemInfo: ItemInfo
        {
            mass = 12;
            optics = 1;
            modelOptics = "Aux501\Weapons\Accessories\data\Aux501_scope_DMS";
            class OpticsModes
            {
                class Scope
                {
                    opticsID = 1;
                    useModelOptics = 1;
                    opticsPPEffects[] = {"OpticsCHAbera2","OpticsBlur3"};
                    opticsZoomMin = "0.25/12";
                    opticsZoomMax = "0.25/6";
                    opticsZoomInit = "0.25/6";
                    discreteinitIndex = 0;
                    discretefov[] = {"0.25/6","0.25/12"};
                    discreteDistanceInitIndex = 1;
                    distanceZoomMin = 300;
                    distanceZoomMax = 1200;
                    memoryPointCamera = "opticView";
                    visionMode[] = {"Normal","NVG","Ti"};
                    thermalMode[] = {0,1};
                    opticsFlare = 1;
                    opticsDisplayName = "DMS-A";
                    opticsDisablePeripherialVision = 0;
                    cameraDir = "";
                };
            };
        };
    };
    class Aux501_cows_DMS_2_TI: Aux501_cows_DMS_TI
    {
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_lr_02_ca.paa";
        displayName = "DMS LR 6-12x Scope B";
        class ItemInfo: ItemInfo
        {
            modelOptics = "Aux501\Weapons\Accessories\data\Aux501_scope2_DMS";
            opticsDisplayName = "DMS-B";
        };
    };
    class Aux501_cows_DMS_3_TI: Aux501_cows_DMS_TI
    {
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_lr_03_ca.paa";
        displayName = "DMS LR 6-12x Scope C";
        class ItemInfo: ItemInfo
        {
            modelOptics = "Aux501\Weapons\Accessories\data\Aux501_scope3_DMS";
            opticsDisplayName = "DMS-C";
        };
    };
    class Aux501_cows_DMS_4_TI: Aux501_cows_DMS_TI
    {
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_lr_03_ca.paa";
        displayName = "DMS LR 6-12x Scope D";
        class ItemInfo: ItemInfo
        {
            modelOptics = "Aux501\Weapons\Accessories\data\Aux501_scope4_DMS";
            opticsDisplayName = "DMS-D";
        };
    };
    class Aux501_cows_DMS_5_TI: Aux501_cows_DMS_TI
    {
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_lr_03_ca.paa";
        displayName = "DMS LR 6-12x Scope E";
        class ItemInfo: ItemInfo
        {
            modelOptics = "\kobra\442_weapons\weapons\773\773_retical.p3d";
            opticsDisplayName = "DMS-E";
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_20mw40;

    class Aux501_Weapons_Mags_30mw10: Aux501_Weapons_Mags_20mw40
    {
        displayName = "[501st] 10Rnd 30MW Cell";
        displayNameShort = "10Rnd 30MW";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_valken38_ca.paa";
        count = 10;
        ammo = "Aux501_Weapons_Ammo_30mw";
        initSpeed = 1500;
        descriptionShort = "Valken 38x High power magazine";
        model = "\MRC\JLTS\weapons\DC15S\DC15S_mag.p3d";
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_base_blue;
    
    class Aux501_Weapons_Ammo_30mw: Aux501_Weapons_Ammo_base_blue
    {
        hit = 45;
        typicalSpeed = 1500;
        caliber = 2.4;
        airFriction = 0;
        waterFriction = -0.009;
    };
};