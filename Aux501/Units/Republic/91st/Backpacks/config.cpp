class cfgPatches
{
    class Aux501_Patch_Units_Republic_91_Backpack
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Backpacks_Standard",
            "Aux501_Patch_Units_Republic_501_Infantry_Backpacks_LR_Large"
        };
        units[] = 
        {
            "Aux501_Units_Republic_91_Backpacks_Standard", 
            "Aux501_Units_Republic_91_Backpacks_LR_Large"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501_Infantry_Backpacks_Standard;
    class Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large;

    class Aux501_Units_Republic_91_Backpacks_Standard: Aux501_Units_Republic_501_Infantry_Backpacks_Standard
    {
        displayName = "{91st} Backpack 01";
        hiddenSelectionsTextures[] = 
        {
            "Aux501\Units\Republic\91st\Backpacks\textures\91_backpack_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_91st_Backpacks";
            variant = "standard";
        };
    };

    class Aux501_Units_Republic_91_Backpacks_LR_Large: Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large
    {
        displayName = "{91st} RTO LR Large 01";
        hiddenSelectionsTextures[] = 
        {
            "Aux501\Units\Republic\91st\Backpacks\textures\91_backpack_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_91st_Backpacks";
            variant = "rto";
        };
    };
};

class XtdGearModels
{
    class CfgVehicles
    {
        class Aux501_ACEX_Gear_Republic_91st_Backpacks
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "standard",
                    "rto"
                };
                class standard
                {
                    label = "Standard";
                };
                class rto
                {
                    label = "RTO";
                };
            };
        };
    };
};