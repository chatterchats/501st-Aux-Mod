class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Backpacks_LR_Medium
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Backpacks_LR_Large"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Backpacks_LR_Medium",
            "Aux501_Units_Republic_501_Infantry_Backpacks_LR_Medium_Blue",
            "Aux501_Units_Republic_501_Infantry_Backpacks_LR_Medium_Red"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large;

    class Aux501_Units_Republic_501_Infantry_Backpacks_LR_Medium: Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large
    {
        displayName = "[501st] INF Backpack LR Medium 01 - White";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_RTO_pack_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneRTOPack.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_RTO_pack_co.paa"};
        tf_dialog = "JLTS_clone_lr_programmer_radio_dialog";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_LR_Medium";
            variant = "white";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_LR_Medium_Blue: Aux501_Units_Republic_501_Infantry_Backpacks_LR_Medium
    {
        displayName = "[501st] INF Backpack LR Medium 02 - Blue";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Backpacks\LR_Medium\data\textures\inf_LR_medium_blue_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_LR_Medium";
            variant = "blue";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_LR_Medium_Red: Aux501_Units_Republic_501_Infantry_Backpacks_LR_Medium
    {
        displayName = "[501st] MED Backpack LR Medium 01";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Backpacks\LR_Medium\data\textures\inf_LR_medium_red_co.paa"};
        maximumload = 1000;
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_LR_Medium";
            variant = "medic";
        };
    };
};

class XtdGearModels
{
    class CfgVehicles
    {
        class Aux501_ACEX_Gear_Republic_501st_Backpack_LR_Medium
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "white",
                    "blue",
                    "medic"
                };
                class white
                {
                    label = "White";
                };
                class blue
                {
                    label = "Blue";
                };
                class medic
                {
                    label = "Medic";
                };
            };
        };
    };
};