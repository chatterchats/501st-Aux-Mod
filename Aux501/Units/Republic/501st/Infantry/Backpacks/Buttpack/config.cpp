class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Backpacks_Beltbag
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Backpacks"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Backpacks_Beltbag"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501_Infantry_Backpacks_base;

    class Aux501_Units_Republic_501_Infantry_Backpacks_Beltbag: Aux501_Units_Republic_501_Infantry_Backpacks_base
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] INF Backpack 02 - Beltbag";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_belt_bag_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBeltBag.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_belt_bag_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpacks_Basic";
            variant = "beltbag";
        };
    };
};