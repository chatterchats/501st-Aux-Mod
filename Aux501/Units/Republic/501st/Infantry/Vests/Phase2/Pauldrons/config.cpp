class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_Pauldrons
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_Jr_NCOs"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon",
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2",
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_3",
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_4",

            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Company",
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Company_2",
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Company_3",
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Company_4",

            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion",
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion_2",
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion_3",
            "Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion_4",

            "Aux501_Units_Republic_501_Infantry_Vests_2LT",
            "Aux501_Units_Republic_501_Infantry_Vests_2LT_2",
            "Aux501_Units_Republic_501_Infantry_Vests_2LT_3",
            "Aux501_Units_Republic_501_Infantry_Vests_1LT_4",

            "Aux501_Units_Republic_501_Infantry_Vests_1LT",
            "Aux501_Units_Republic_501_Infantry_Vests_1LT_2",
            "Aux501_Units_Republic_501_Infantry_Vests_1LT_3",
            "Aux501_Units_Republic_501_Infantry_Vests_1LT_4",

            "Aux501_Units_Republic_501_Infantry_Vests_Captain",
            "Aux501_Units_Republic_501_Infantry_Vests_Captain_2",
            "Aux501_Units_Republic_501_Infantry_Vests_Captain_3",
            "Aux501_Units_Republic_501_Infantry_Vests_Captain_4",

            "Aux501_Units_Republic_501_Infantry_Vests_Major",
            "Aux501_Units_Republic_501_Infantry_Vests_Major_2",
            "Aux501_Units_Republic_501_Infantry_Vests_Major_3",
            "Aux501_Units_Republic_501_Infantry_Vests_Major_4"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_CP;
    
    class Aux501_Units_Republic_501_Infantry_Vests_CS: Aux501_Units_Republic_501_Infantry_Vests_CP
    {
        class ItemInfo;
    };

    //Platoon CS-M
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon: Aux501_Units_Republic_501_Infantry_Vests_CS
    {
        displayname = "[501st] INF VEST 27 - CS-M";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestPauldron_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_plt_csm_vest_co.paa"};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer.p3d";
            hiddenSelections[] = {"camo1"};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "csm";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        displayname = "[501st] INF VEST 28 - CS-M";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneVestCommander.p3d";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "csm";
            variant = "pouch";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_3: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        displayname = "[501st] INF VEST 29 - CS-M";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestLieutenant.p3d";
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneVestLieutenant.p3d";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "csm";
            variant = "nokama";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_4: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        displayname = "[501st] INF VEST 30 - CS-M";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer2.p3d";
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneVestOfficer2.p3d";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "csm";
            variant = "reverse";
        };
    };

    //Company CS-M
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Company: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        displayname = "[501st] INF VEST 31 - CS-M (C)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_comp_csm_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "c_csm";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Company_2: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2
    {
        displayname = "[501st] INF VEST 32 - CS-M (C)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_comp_csm_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "c_csm";
            variant = "pouch";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Company_3: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_3
    {
        displayname = "[501st] INF VEST 33 - CS-M (C)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_comp_csm_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "c_csm";
            variant = "nokama";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Company_4: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_4
    {
        displayname = "[501st] INF VEST 34 - CS-M (C)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_comp_csm_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "c_csm";
            variant = "reverse";
        };
    };

    //Battalion CS-M
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        displayname = "[501st] INF VEST 35 - CS-M (B)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_bn_csm_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "b_csm";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion_2: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2
    {
        displayname = "[501st] INF VEST 36 - CS-M (B)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_bn_csm_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "b_csm";
            variant = "pouch";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion_3: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_3
    {
        displayname = "[501st] INF VEST 37 - CS-M (B)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_bn_csm_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "b_csm";
            variant = "nokama";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion_4: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_4
    {
        displayname = "[501st] INF VEST 38 - CS-M (B)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_bn_csm_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "b_csm";
            variant = "reverse";
        };
    };

    //Officers

    //2nd LT
    class Aux501_Units_Republic_501_Infantry_Vests_2LT: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        displayname = "[501st] INF VEST 39 - 2nd Lt";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_2LT_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "second_Lt";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_2LT_2: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2
    {
        displayname = "[501st] INF VEST 40 - 2nd Lt";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_2LT_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "second_Lt";
            variant = "pouch";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_2LT_3: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_3
    {
        displayname = "[501st] INF VEST 41 - 2nd Lt";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_2LT_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "second_Lt";
            variant = "nokama";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_2LT_4: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_4
    {
        displayname = "[501st] INF VEST 42 - 2nd Lt";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_2LT_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "second_Lt";
            variant = "reverse";
        };
    };

    //1st LT
    class Aux501_Units_Republic_501_Infantry_Vests_1LT: Aux501_Units_Republic_501_Infantry_Vests_2LT
    {
        displayname = "[501st] INF VEST 43 - 1st Lt";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_1LT_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "first_Lt";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_1LT_2: Aux501_Units_Republic_501_Infantry_Vests_2LT_2
    {
        displayname = "[501st] INF VEST 44 - 1st Lt";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_1LT_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "first_Lt";
            variant = "pouch";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_1LT_3: Aux501_Units_Republic_501_Infantry_Vests_2LT_3
    {
        displayname = "[501st] INF VEST 45 - 1st Lt";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_1LT_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "first_Lt";
            variant = "nokama";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_1LT_4: Aux501_Units_Republic_501_Infantry_Vests_2LT_4
    {
        displayname = "[501st] INF VEST 46 - 1st Lt";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_1LT_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "first_Lt";
            variant = "reverse";
        };
    };

    //Captain
    class Aux501_Units_Republic_501_Infantry_Vests_Captain: Aux501_Units_Republic_501_Infantry_Vests_2LT
    {
        displayname = "[501st] INF VEST 47 - Captain";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_capt_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "captain";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Captain_2: Aux501_Units_Republic_501_Infantry_Vests_2LT_2
    {
        displayname = "[501st] INF VEST 48 - Captain";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_capt_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "captain";
            variant = "pouch";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Captain_3: Aux501_Units_Republic_501_Infantry_Vests_2LT_3
    {
        displayname = "[501st] INF VEST 49 - Captain";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_capt_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "captain";
            variant = "nokama";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Captain_4: Aux501_Units_Republic_501_Infantry_Vests_2LT_4
    {
        displayname = "[501st] INF VEST 50 - Captain";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_capt_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "captain";
            variant = "reverse";
        };
    };

    //Major
    class Aux501_Units_Republic_501_Infantry_Vests_Major: Aux501_Units_Republic_501_Infantry_Vests_2LT
    {
        displayname = "[501st] INF VEST 51 - Major";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_maj_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "major";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Major_2: Aux501_Units_Republic_501_Infantry_Vests_2LT_2
    {
        displayname = "[501st] INF VEST 52 - Major";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_maj_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "major";
            variant = "pouch";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Major_3: Aux501_Units_Republic_501_Infantry_Vests_2LT_3
    {
        displayname = "[501st] INF VEST 53 - Major";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_maj_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "major";
            variant = "nokama";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Major_4: Aux501_Units_Republic_501_Infantry_Vests_2LT_4
    {
        displayname = "[501st] INF VEST 54 - Major";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_maj_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "infantry";
            rank = "major";
            variant = "reverse";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "mos",
                "rank",      
                "variant"
            };
            class mos
            {
                label = "MOS";
                values[] = 
                {
                    "infantry",
                    "medic",
                    "rto"
                };
                class infantry
                {
                    label = "GI";
                };
                class medic
                {
                    label = "Medic";
                };
                class rto
                {
                    label = "RTO";
                };
            };
            class rank
            {
                label = "Rank";
                values[] = 
                {
                    "csm",
                    "c_csm",
                    "b_csm",

                    "second_Lt",
                    "first_Lt",
                    "captain",
                    "major"
                };
                class csm
                {
                    label = "CS-M";
                };
                class c_csm
                {
                    label = "CO CS-M";
                };
                class b_csm
                {
                    label = "BN CS-M";
                };
                class second_Lt
                {
                    label = "2nd Lt";
                };
                class first_Lt
                {
                    label = "1st Lt";
                };
                class captain
                {
                    label = "Captain";
                };
                class major
                {
                    label = "Major";
                };
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "standard",
                    "pouch",
                    "nokama",
                    "reverse",
                    "Nopauldron"
                };
                class standard
                {
                    label = "Standard";
                };
                class pouch
                {
                    label = "Pouch";
                };
                class nokama
                {
                    label = "No Kama";
                };
                class reverse
                {
                    label = "Reverse";
                };
            };
        };
    };
};