
class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_Jr_NCOs
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper",
            "Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper_Bag",

            "Aux501_Units_Republic_501_Infantry_Vests_Lance_CP",
            "Aux501_Units_Republic_501_Infantry_Vests_Lance_CP_bag",
            

            "Aux501_Units_Republic_501_Infantry_Vests_CP",
            "Aux501_Units_Republic_501_Infantry_Vests_CP_bag",

            "Aux501_Units_Republic_501_Infantry_Vests_snr_CP",
            "Aux501_Units_Republic_501_Infantry_Vests_snr_CP_bag",

            "Aux501_Units_Republic_501_Infantry_Vests_CS",
            "Aux501_Units_Republic_501_Infantry_Vests_snr_CS"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_Base;
    
    class Aux501_Units_Republic_501_Infantry_Vests_Nanoweave: Aux501_Units_Republic_501_Infantry_Vests_Base
    {
        class ItemInfo;
    };

    //Vet Trooper
    class Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper: Aux501_Units_Republic_501_Infantry_Vests_Nanoweave
    {
        displayname = "[501st] INF VEST 14 - Vet. CT";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestRecon.p3d";
        hiddenSelections[] = {"camo2"};
        hiddenSelectionsTextures[] = {"MRC\JLTS\characters\CloneArmor\data\Clone_vest_heavy_co.paa"};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneVestRecon.p3d";
            hiddenSelections[] = {"camo2"};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Jr_NCO_Vests";
            mos = "infantry";
            rank = "vet_ct";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper_Bag: Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper
    {
        displayname = "[501st] INF VEST 15 - Vet. CT";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_vet_ct_vest_co.paa"
        };
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
            hiddenSelections[]= {"camo1","camo2"};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Jr_NCO_Vests";
            mos = "infantry";
            rank = "vet_ct";
            variant = "bag";
        };
    };

    //CLC
    class Aux501_Units_Republic_501_Infantry_Vests_Lance_CP: Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper
    {
        displayname = "[501st] INF VEST 16 - CLC";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_lance_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Jr_NCO_Vests";
            mos = "infantry";
            rank = "clc";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Lance_CP_bag: Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper_Bag
    {
        displayname = "[501st] INF VEST 17 - CLC";
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_lance_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Jr_NCO_Vests";
            mos = "infantry";
            rank = "clc";
            variant = "bag";
        };
    };

    //CP
    class Aux501_Units_Republic_501_Infantry_Vests_CP: Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper
    {
        displayname = "[501st] INF VEST 18 - CP";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestReconNCO.p3d";
        hiddenSelections[] = {"camo2"};
        hiddenSelectionsTextures[] = {"MRC\JLTS\characters\CloneArmor\data\Clone_vest_heavy_co.paa"};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneVestReconNCO.p3d";
            hiddenSelections[] = {"camo2"};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Jr_NCO_Vests";
            mos = "infantry";
            rank = "cp";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CP_bag: Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper
    {
        displayname = "[501st] INF VEST 19 - CP";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborneNCO.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_vet_ct_vest_co.paa"
        };
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborneNCO.p3d";
            hiddenSelections[]= {"camo1","camo2"};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Jr_NCO_Vests";
            mos = "infantry";
            rank = "cp";
            variant = "bag";
        };
    };
    
    class Aux501_Units_Republic_501_Infantry_Vests_snr_CP: Aux501_Units_Republic_501_Infantry_Vests_CP
    {
        displayname = "[501st] INF VEST 20 - Sr. CP";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_snr_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Jr_NCO_Vests";
            mos = "infantry";
            rank = "sr_cp";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_snr_CP_bag: Aux501_Units_Republic_501_Infantry_Vests_CP_bag
    {
        displayname = "[501st] INF VEST 21 - Sr. CP";
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_snr_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Jr_NCO_Vests";
            mos = "infantry";
            rank = "sr_cp";
            variant = "bag";
        };
    };

    //CS
    class Aux501_Units_Republic_501_Infantry_Vests_CS: Aux501_Units_Republic_501_Infantry_Vests_CP
    {
        displayname = "[501st] INF VEST 22 - CS";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestReconOfficer.p3d";
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "MRC\JLTS\characters\CloneArmor\data\Clone_vest_officer_co.paa",
            "MRC\JLTS\characters\CloneArmor\data\Clone_vest_heavy_co.paa"
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneVestReconOfficer.p3d";
            hiddenSelections[] = {"camo1","camo2"};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Jr_NCO_Vests";
            mos = "infantry";
            rank = "cs";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_snr_CS: Aux501_Units_Republic_501_Infantry_Vests_CS
    {
        displayname = "[501st] INF VEST 24 - Sr. CS";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_2LT_vest_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_snr_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Jr_NCO_Vests";
            mos = "infantry";
            rank = "sr_cs";
            variant = "standard";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_P2_Jr_NCO_Vests
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "mos",
                "rank",      
                "variant"
            };
            class mos
            {
                label = "MOS";
                values[] = 
                {
                    "infantry",
                    "medic",
                    "rto"
                };
                class infantry
                {
                    label = "GI";
                };
                class medic
                {
                    label = "Medic";
                };
                class rto
                {
                    label = "RTO";
                };
            };
            class rank
            {
                label = "Rank";
                values[] = 
                {
                    "vet_ct",
                    "clc",

                    "cp",
                    "sr_cp",
                    "cs",
                    "sr_cs"
                };
                class vet_ct
                {
                    label = "Vet. CT";
                };
                class clc
                {
                    label = "CLC";
                };
                class cp
                {
                    label = "CP";
                };
                class sr_cp
                {
                    label = "Sr. CP";
                };
                class cs
                {
                    label = "CS";
                };
                class sr_cs
                {
                    label = "Sr. CS";
                };
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "standard",
                    "bag"
                };
                class standard
                {
                    label = "Standard";
                };
                class bag
                {
                    label = "Bag";
                };
            };
        };
    };
};