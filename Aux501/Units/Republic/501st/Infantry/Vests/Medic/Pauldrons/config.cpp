class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Medic_Vests_Phase2_Pauldrons
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_Pauldrons"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Medic_Vests_CSM_Battalion",
            "Aux501_Units_Republic_501_Medic_Vests_CSM_Battalion_2",
            "Aux501_Units_Republic_501_Medic_Vests_CSM_Battalion_3",
            "Aux501_Units_Republic_501_Medic_Vests_CSM_Battalion_4"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion;
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion_2;
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion_3;
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion_4;

    //Battalion CS-M
    class Aux501_Units_Republic_501_Medic_Vests_CSM_Battalion: Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion
    {
        displayname = "[501st] MED VEST 08 - CS-M (B)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_bn_csm_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "medic";
            rank = "b_csm";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Medic_Vests_CSM_Battalion_2: Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion_2
    {
        displayname = "[501st] MED VEST 09 - CS-M (B)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_bn_csm_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "medic";
            rank = "b_csm";
            variant = "pouch";
        };
    };
    class Aux501_Units_Republic_501_Medic_Vests_CSM_Battalion_3: Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion_3
    {
        displayname = "[501st] MED VEST 10 - CS-M (B)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_bn_csm_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "medic";
            rank = "b_csm";
            variant = "nokama";
        };
    };
    class Aux501_Units_Republic_501_Medic_Vests_CSM_Battalion_4: Aux501_Units_Republic_501_Infantry_Vests_CSM_Battalion_4
    {
        displayname = "[501st] MED VEST 11 - CS-M (B)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_bn_csm_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Pauldron_Vests";
            mos = "medic";
            rank = "b_csm";
            variant = "reverse";
        };
    };
};