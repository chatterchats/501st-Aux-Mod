class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Helmets_Medic
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Helmets_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Medic_Barc_CM",
            "Aux501_Units_Republic_501_Medic_Barc_CMC"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Blank_Helmet;
    class Aux501_Units_Republic_501_Infantry_Helmet_Trooper: Aux501_Units_Republic_501_Infantry_Blank_Helmet
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501_Medic_Barc_CMC: Aux501_Units_Republic_501_Infantry_Helmet_Trooper
    {
        displayName = "[501st] MED HELM BARC 01 - CM-C";
        picture = "\MRC\JLTS\characters\CloneArmor2\data\ui\CloneHelmetBARC_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor2\CloneHelmetBARC.p3d";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Medic\data\textures\cm_c_barc_helmet_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Medic\data\textures\cm_c_barc_helmet_co.paa"
        };
        hiddenSelectionsMaterials[]= 
        {
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Medic\data\BARC_Helmet.rvmat",
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Phase2\data\Standard_Visor.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor2\CloneHelmetBARC.p3d";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_MED_Helmet";
            variant = "barc";
            rank = "cmc";
        };
    };
    class Aux501_Units_Republic_501_Medic_Barc_CM: Aux501_Units_Republic_501_Medic_Barc_CMC
    {  
        displayName = "[501st] MED HELM BARC 02 - CM";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Medic\data\textures\cm_barc_helmet_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_MED_Helmet";
            variant = "barc";
            rank = "cm";
        };
    };
};