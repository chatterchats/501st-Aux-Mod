class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_base;
   
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV: Aux501_Republic_501_Infantry_Equipment_NVGs_base
    {
        scope = 2;
        scopearsenal = 2;
        picture = "\MRC\JLTS\Core_mod\data\ui\nvg_chip_1_ui_ca.paa";
        displayName = "[501st] INF NVG 01 - NVG";
        model = "\MRC\JLTS\weapons\E5S\E5S_mag.p3d";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVG_chips";
            variant = "nvg";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] INF NVG 02 - TI";
        picture = "\MRC\JLTS\Core_mod\data\ui\nvg_chip_2_ui_ca.paa";
        visionMode[] = {"Normal","TI"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVG_chips";
            variant = "thermals";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_P2_NVG_chips
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "nvg",
                    "thermals"
                };
                class nvg
                {
                    label = "NVGs";
                };
                class thermals
                {
                    label = "Thermals";
                };
            };
        };
    };
};