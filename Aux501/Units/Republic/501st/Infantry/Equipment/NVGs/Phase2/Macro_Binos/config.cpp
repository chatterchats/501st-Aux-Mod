class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Macro_Binos
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_down",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_501",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_501_down",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_blue",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_blue_down",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_red",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_red_down",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_gray",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_gray_down",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_blue_full",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_blue_full_down",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_red_full",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_red_full_down",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_gray_full",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_gray_full_down",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_black_full",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_black_full_down"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_base;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI: Aux501_Republic_501_Infantry_Equipment_NVGs_base
    {
        class ItemInfo;
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] INF MacroBino 00 - White";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_nvg_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneNVG_off.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_nvg_co.paa"};
        class ItemInfo: ItemInfo
        {
            hiddenSelections[] = {"camo1"};
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneNVG_on.p3d";
            modelOff = "\MRC\JLTS\characters\CloneArmor\CloneNVG_off.p3d";
            mass = 20;
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_toggle";
            color = "white";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_down: Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino
    {
        displayName = "[501st] INF MacroBino 00 - White - Down";
        model = "\MRC\JLTS\characters\CloneArmor\CloneNVG_on.p3d";
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneNVG_on.p3d";
            modelOff = "\MRC\JLTS\characters\CloneArmor\CloneNVG_on.p3d";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_down";
            color = "white";
        };
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_501: Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino
    {
        displayName = "[501st] INF MacroBino 01";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\Macro_Binos\data\textures\inf_macrobino_501st_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_toggle";
            color = "FiveOhFirst";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_501_down: Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_down
    {
        displayName = "[501st] INF MacroBino 01 - Down";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\Macro_Binos\data\textures\inf_macrobino_501st_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_down";
            color = "FiveOhFirst";
        };
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_blue: Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino
    {
        displayName = "[501st] INF MacroBino 02 - Blue";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\Macro_Binos\data\textures\inf_macrobino_blue_on_white_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_toggle";
            color = "blue";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_blue_down: Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_down
    {
        displayName = "[501st] INF MacroBino 02 - Blue - Down";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\Macro_Binos\data\textures\inf_macrobino_blue_on_white_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_down";
            color = "blue";
        };
    };
    
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_blue_full: Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino
    {
        displayName = "[501st] INF MacroBino 03 - Full Blue";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\Macro_Binos\data\textures\inf_macrobino_full_blue_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_toggle";
            color = "full_blue";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_blue_full_down: Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_down
    {
        displayName = "[501st] INF MacroBino 03 - Full Blue (Down)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\Macro_Binos\data\textures\inf_macrobino_full_blue_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_down";
            color = "full_blue";
        };
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_red: Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino
    {
        displayName = "[501st] INF MacroBino 04 - Red";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\Macro_Binos\data\textures\inf_macrobino_red_on_white_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_toggle";
            color = "red";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_red_down: Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_down
    {
        displayName = "[501st] INF MacroBino 04 - Red - Down";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\Macro_Binos\data\textures\inf_macrobino_red_on_white_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_down";
            color = "red";
        };
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_red_full: Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino
    {
        displayName = "[501st] INF MacroBino 05 - Full Red";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\Macro_Binos\data\textures\inf_macrobino_full_red_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_toggle";
            color = "full_red";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_red_full_down: Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_down
    {
        displayName = "[501st] INF MacroBino 05 - Full Red (Down)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\Macro_Binos\data\textures\inf_macrobino_full_red_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_down";
            color = "full_red";
        };
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_gray: Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino
    {
        displayName = "[501st] INF MacroBino 06 - Gray";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\Macro_Binos\data\textures\inf_macrobino_gray_on_white_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_toggle";
            color = "gray";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_gray_down: Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_down
    {
        displayName = "[501st] INF MacroBino 06 - Gray - Down";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\Macro_Binos\data\textures\inf_macrobino_gray_on_white_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_down";
            color = "gray";
        };
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_gray_full: Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino
    {
        displayName = "[501st] INF MacroBino 07 - Full Gray";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\Macro_Binos\data\textures\inf_macrobino_full_gray_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_toggle";
            color = "full_gray";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_gray_full_down: Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_down
    {
        displayName = "[501st] INF MacroBino 07 - Full Gray - Down";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\Macro_Binos\data\textures\inf_macrobino_full_gray_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_down";
            color = "full_gray";
        };
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_black_full: Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino
    {
        displayName = "[501st] INF MacroBino 08 - Full Black";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor2\data\Clone_PurgeTrooper_nvg_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_toggle";
            color = "black";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_black_full_down: Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_down
    {
        displayName = "[501st] INF MacroBino 08 - Full Black - Down";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor2\data\Clone_PurgeTrooper_nvg_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_down";
            color = "black";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant",
                "color"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "visor_toggle",
                    "visor_down"
                };
                class visor_toggle
                {
                    label = "Standard";
                };
                class visor_down
                {
                    label = "Down";
                };
            };
            class color
            {
                label = "Color";
                values[] = 
                {
                    "white",
                    "FiveOhFirst",
                    "blue",
                    "full_blue",
                    "red",
                    "full_red",
                    "gray",                    
                    "full_gray",
                    "black"
                };
                class white
                {
                    label = "White";
                };
                class FiveOhFirst
                {
                    label = "501st";
                };
                class blue
                {
                    label = "Blue";
                };
                class full_blue
                {
                    label = "Full Blue";
                };
                class red
                {
                    label = "Red";
                };
                class full_red
                {
                    label = "Full Red";
                };
                class gray
                {
                    label = "Gray";
                };
                class full_gray
                {
                    label = "Full Gray";
                };
                class black
                {
                    label = "Black";
                };
            };
        };
    };
};