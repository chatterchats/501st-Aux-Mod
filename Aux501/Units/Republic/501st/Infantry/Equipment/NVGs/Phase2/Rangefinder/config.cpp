
class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Rangefinder
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_white",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_blue",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_red",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_yellow"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_base;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI: Aux501_Republic_501_Infantry_Equipment_NVGs_base
    {
        class ItemInfo;
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] INF Rangefinder 00 - Black ";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_nvg_range_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneNVGRange_off.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\Rangefinder\data\textures\inf_rangefinder_black_co.paa"};
        class ItemInfo: ItemInfo
        {
            hiddenSelections[] = {"camo1"};
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneNVGRange_on.p3d";
            modelOff = "\MRC\JLTS\characters\CloneArmor\CloneNVGRange_off.p3d";
            mass = 20;
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Rangefinder";
            color = "black";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_white: Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder
    {
        displayName = "[501st] INF Rangefinder 01 - White";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_nvg_range_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Rangefinder";
            color = "white";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_blue: Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder
    {
        displayName = "[501st] INF Rangefinder 03 - Blue";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\Rangefinder\data\textures\inf_rangefinder_blue_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Rangefinder";
            color = "blue";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_red: Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder
    {
        displayName = "[501st] INF Rangefinder 04 - Red";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_ARCHammer_rangefinder_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Rangefinder";
            color = "red";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_yellow: Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder
    {
        displayName = "[501st] INF Rangefinder 05 - Yellow";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_ARCBlitz_rangefinder_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Rangefinder";
            color = "yellow";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Rangefinder
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "color"
            };
            class color
            {
                label = "Color";
                values[] = 
                {
                    "black",
                    "white",
                    "blue",
                    "red",
                    "yellow"
                };
                class black
                {
                    label = "Black";
                };
                class white
                {
                    label = "White";
                };
                class blue
                {
                    label = "Blue";
                };
                class red
                {
                    label = "Red";
                };
                class yellow
                {
                    label = "Yellow";
                };
            };
        };
    };
};