class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear_Shoulder_Antenna
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Shoulder_Antenna",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Shoulder_Antenna_removeblue",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Shoulder_Antenna_removered",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Shoulder_Antenna_removegreen",

            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Shoulder_Antenna_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Shoulder_Antenna_removeblue_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Shoulder_Antenna_removered_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Shoulder_Antenna_removegreen_nohud"
        };
        weapons[] = {};
    };
};

class CfgGlasses
{
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen;

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud;

    //Hud//

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Shoulder_Antenna: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display
    {
        displayname = "[501st] FW HUD Antenna 01 - CT";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\Misc\Shoulder_Antenna\data\textures\UI\Aux501_FW_shoulder_antenna_ui_ca.paa";
        model = "\SWLB_CEE\data\SWLB_CEE_Wolffe_Vest.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = 
        {
            "",
            "swlb_clones\data\mc_camo1_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_on";
            colorblind = "clear";
            variant = "antenna";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Shoulder_Antenna_removeblue: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue
    {
        displayname = "[501st] FW HUD Antenna 01 - Blue";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\Misc\Shoulder_Antenna\data\textures\UI\Aux501_FW_shoulder_antenna_ui_ca.paa";
        model = "\SWLB_CEE\data\SWLB_CEE_Wolffe_Vest.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = 
        {
            "",
            "swlb_clones\data\mc_camo1_co.paa"
            
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_on";
            colorblind = "no_blue";
            variant = "antenna";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Shoulder_Antenna_removered: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered
    {
        displayname = "[501st] FW HUD Antenna 01 - Red";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\Misc\Shoulder_Antenna\data\textures\UI\Aux501_FW_shoulder_antenna_ui_ca.paa";
        model = "\SWLB_CEE\data\SWLB_CEE_Wolffe_Vest.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = 
        {
            "",
            "swlb_clones\data\mc_camo1_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_on";
            colorblind = "no_red";
            variant = "antenna";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Shoulder_Antenna_removegreen: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen
    {
        displayname = "[501st] FW HUD Antenna 01 - Green";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\Misc\Shoulder_Antenna\data\textures\UI\Aux501_FW_shoulder_antenna_ui_ca.paa";
        model = "\SWLB_CEE\data\SWLB_CEE_Wolffe_Vest.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = 
        {
            "",
            "swlb_clones\data\mc_camo1_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_on";
            colorblind = "no_green";
            variant = "antenna";
        };
    };
    
    //No Hud//

    //Suspenders
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Shoulder_Antenna_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud
    {
        displayname = "[501st] FW HUDless Antenna 01 - CT";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\Misc\Shoulder_Antenna\data\textures\UI\Aux501_FW_shoulder_antenna_ui_ca.paa";
        model = "\SWLB_CEE\data\SWLB_CEE_Wolffe_Vest.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = 
        {
            "",
            "swlb_clones\data\mc_camo1_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_off";
            colorblind = "clear";
            variant = "antenna";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Shoulder_Antenna_removeblue_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud
    {
        displayname = "[501st] FW HUDless Antenna 01 - Blue";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\Misc\Shoulder_Antenna\data\textures\UI\Aux501_FW_shoulder_antenna_ui_ca.paa";
        model = "\SWLB_CEE\data\SWLB_CEE_Wolffe_Vest.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = 
        {
            "",
            "swlb_clones\data\mc_camo1_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_off";
            colorblind = "no_blue";
            variant = "antenna";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Shoulder_Antenna_removered_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud
    {
        displayname = "[501st] FW HUDless Antenna 01 - Red";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\Misc\Shoulder_Antenna\data\textures\UI\Aux501_FW_shoulder_antenna_ui_ca.paa";
        model = "\SWLB_CEE\data\SWLB_CEE_Wolffe_Vest.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = 
        {
            "",
            "swlb_clones\data\mc_camo1_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_off";
            colorblind = "no_red";
            variant = "antenna";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Shoulder_Antenna_removegreen_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud
    {
        displayname = "[501st] FW HUDless Antenna 01 - Green";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\Misc\Shoulder_Antenna\data\textures\UI\Aux501_FW_shoulder_antenna_ui_ca.paa";
        model = "\SWLB_CEE\data\SWLB_CEE_Wolffe_Vest.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = 
        {
            "",
            "swlb_clones\data\mc_camo1_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_off";
            colorblind = "no_green";
            variant = "antenna";
        };
    };
};