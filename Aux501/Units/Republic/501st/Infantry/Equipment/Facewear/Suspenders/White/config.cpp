class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear_Suspenders_White
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Suspenders_White",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Suspenders_White_removeblue",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Suspenders_White_removered",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Suspenders_White_removegreen",
            

            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Suspenders_White_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Suspenders_White_removeblue_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Suspenders_White_removered_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Suspenders_White_removegreen_nohud"
        };
        weapons[] = {};
    };
};

class CfgGlasses
{
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen;

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud;

    //Hud//

    //Suspenders
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Suspenders_White: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display
    {
        displayname = "[501st] FW HUD VEST 03 - Vet. CT";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestSuspender_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestSuspender.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_501stJet_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Suspenders";
            hud = "hud_on";
            colorblind = "clear";
            variant = "white";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Suspenders_White_removeblue: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue
    {
        displayname = "[501st] FW HUD VEST 03 - Vet. CT - Blue";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestSuspender_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestSuspender.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_501stJet_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Suspenders";
            hud = "hud_on";
            colorblind = "no_blue";
            variant = "white";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Suspenders_White_removered: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered
    {
        displayname = "[501st] FW HUD VEST 03 - Vet. CT - Red";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestSuspender_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestSuspender.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_501stJet_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Suspenders";
            hud = "hud_on";
            colorblind = "no_red";
            variant = "white";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Suspenders_White_removegreen: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen
    {
        displayname = "[501st] FW HUD VEST 03 - Vet. CT - Green";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestSuspender_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestSuspender.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_501stJet_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Suspenders";
            hud = "hud_on";
            colorblind = "no_green";
            variant = "white";
        };
    };
    
    //No Hud//

    //Suspenders
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Suspenders_White_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud
    {
        displayname = "[501st] FW HUDless VEST 03 - Vet. CT";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestSuspender_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestSuspender.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_501stJet_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Suspenders";
            hud = "hud_off";
            colorblind = "clear";
            variant = "white";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Suspenders_White_removeblue_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud
    {
        displayname = "[501st] FW HUDless VEST 03 - Vet. CT - Blue";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestSuspender_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestSuspender.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_501stJet_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Suspenders";
            hud = "hud_off";
            colorblind = "no_blue";
            variant = "white";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Suspenders_White_removered_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud
    {
        displayname = "[501st] FW HUDless VEST 03 - Vet. CT - Red";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestSuspender_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestSuspender.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_501stJet_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Suspenders";
            hud = "hud_off";
            colorblind = "no_red";
            variant = "white";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Suspenders_White_removegreen_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud
    {
        displayname = "[501st] FW HUDless VEST 03 - Vet. CT - Green";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestSuspender_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestSuspender.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_501stJet_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Suspenders";
            hud = "hud_off";
            colorblind = "no_green";
            variant = "white";
        };
    };
};