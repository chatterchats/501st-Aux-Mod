class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear_Poncho_Black
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removeblue",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removered",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removegreen",

            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removeblue_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removered_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removegreen_nohud"
        };
        weapons[] = {};
    };
};

class CfgGlasses
{
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen;

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud;

    //Hud//

    //Ponchos
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display
    {
        displayname = "[501st] FW HUD Poncho 01";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestSuspender_ui_ca.paa";
        model = "\ls_equipment_bluefor\_misc\poncho\ls_misc_poncho";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\ls_armor_greenfor\vest\misc\poncho\data\partner_black_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Ponchos";
            hud = "hud_on";
            colorblind = "clear";
            variant = "black";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removeblue: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue
    {
        displayname = "[501st] FW HUD Poncho 01 - Blue";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestSuspender_ui_ca.paa";
        model = "\ls_equipment_bluefor\_misc\poncho\ls_misc_poncho";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\ls_armor_greenfor\vest\misc\poncho\data\partner_black_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Ponchos";
            hud = "hud_on";
            colorblind = "no_blue";
            variant = "black";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removered: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered
    {
        displayname = "[501st] FW HUD Poncho 01 - Red";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestSuspender_ui_ca.paa";
        model = "\ls_equipment_bluefor\_misc\poncho\ls_misc_poncho";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\ls_armor_greenfor\vest\misc\poncho\data\partner_black_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Ponchos";
            hud = "hud_on";
            colorblind = "no_red";
            variant = "black";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removegreen: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen
    {
        displayname = "[501st] FW HUD Poncho 01 - Green";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestSuspender_ui_ca.paa";
        model = "\ls_equipment_bluefor\_misc\poncho\ls_misc_poncho";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\ls_armor_greenfor\vest\misc\poncho\data\partner_black_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Ponchos";
            hud = "hud_on";
            colorblind = "no_green";
            variant = "black";
        };
    };
    
    //No Hud//

    //Ponchos
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud
    {
        displayname = "[501st] FW HUDless Poncho 01";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestSuspender_ui_ca.paa";
        model = "\ls_equipment_bluefor\_misc\poncho\ls_misc_poncho";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\ls_armor_greenfor\vest\misc\poncho\data\partner_black_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Ponchos";
            hud = "hud_off";
            colorblind = "clear";
            variant = "black";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removeblue_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud
    {
        displayname = "[501st] FW HUDless Poncho 01 - Blue";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestSuspender_ui_ca.paa";
        model = "\ls_equipment_bluefor\_misc\poncho\ls_misc_poncho";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\ls_armor_greenfor\vest\misc\poncho\data\partner_black_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Ponchos";
            hud = "hud_off";
            colorblind = "no_blue";
            variant = "black";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removered_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud
    {
        displayname = "[501st] FW HUDless Poncho 01 - Red";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestSuspender_ui_ca.paa";
        model = "\ls_equipment_bluefor\_misc\poncho\ls_misc_poncho";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\ls_armor_greenfor\vest\misc\poncho\data\partner_black_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Ponchos";
            hud = "hud_off";
            colorblind = "no_red";
            variant = "black";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removegreen_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud
    {
        displayname = "[501st] FW HUDless Poncho 01 - Green";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestSuspender_ui_ca.paa";
        model = "\ls_equipment_bluefor\_misc\poncho\ls_misc_poncho";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\ls_armor_greenfor\vest\misc\poncho\data\partner_black_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Ponchos";
            hud = "hud_off";
            colorblind = "no_green";
            variant = "black";
        };
    };
};