class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Med_Equipment_Facewear_Heavy_Bag_Standard
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Med_Bag",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Med_Bag_removeblue",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Med_Bag_removered",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Med_Bag_removegreen",

            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Med_Bag_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Med_Bag_removeblue_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Med_Bag_removered_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Med_Bag_removegreen_nohud"
        };
        weapons[] = {};
    };
};

class CfgGlasses
{
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removeblue;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removered;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removegreen;

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removeblue_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removered_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removegreen_nohud;

    //Hud//

    //Medic Bag
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Med_Bag: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag
    {
        hiddenSelectionsTextures[] = 
        {
            "\SWLB_clones\data\heavy_accessories_medic_co.paa",
            "",
            "",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Heavy_Bag";
            hud = "hud_on";
            colorblind = "clear";
            variant = "medic";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Med_Bag_removeblue: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removeblue
    {
        hiddenSelectionsTextures[] = 
        {
            "\SWLB_clones\data\heavy_accessories_medic_co.paa",
            "",
            "",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Heavy_Bag";
            hud = "hud_on";
            colorblind = "no_blue";
            variant = "medic";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Med_Bag_removered: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removered
    {
        hiddenSelectionsTextures[] = 
        {
            "\SWLB_clones\data\heavy_accessories_medic_co.paa",
            "",
            "",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Heavy_Bag";
            hud = "hud_on";
            colorblind = "no_red";
            variant = "medic";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Med_Bag_removegreen: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removegreen
    {
        hiddenSelectionsTextures[] = 
        {
            "\SWLB_clones\data\heavy_accessories_medic_co.paa",
            "",
            "",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Heavy_Bag";
            hud = "hud_on";
            colorblind = "no_green";
            variant = "medic";
        };
    };
    
    //No Hud//

    //Medic Bag
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Med_Bag_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_nohud
    {
        hiddenSelectionsTextures[] = 
        {
            "\SWLB_clones\data\heavy_accessories_medic_co.paa",
            "",
            "",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Heavy_Bag";
            hud = "hud_off";
            colorblind = "clear";
            variant = "medic";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Med_Bag_removeblue_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removeblue_nohud
    {
        hiddenSelectionsTextures[] = 
        {
            "\SWLB_clones\data\heavy_accessories_medic_co.paa",
            "",
            "",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Heavy_Bag";
            hud = "hud_off";
            colorblind = "no_blue";
            variant = "medic";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Med_Bag_removered_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removered_nohud
    {
        hiddenSelectionsTextures[] = 
        {
            "\SWLB_clones\data\heavy_accessories_medic_co.paa",
            "",
            "",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Heavy_Bag";
            hud = "hud_off";
            colorblind = "no_red";
            variant = "medic";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Med_Bag_removegreen_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removegreen_nohud
    {
        hiddenSelectionsTextures[] = 
        {
            "\SWLB_clones\data\heavy_accessories_medic_co.paa",
            "",
            "",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Heavy_Bag";
            hud = "hud_off";
            colorblind = "no_green";
            variant = "medic";
        };
    };
};