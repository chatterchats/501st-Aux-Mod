class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear_Red_Kama
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Red_Kama",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Red_Kama_removeblue",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Red_Kama_removered",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Red_Kama_removegreen",

            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Red_Kama_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Red_Kama_removeblue_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Red_Kama_removered_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Red_Kama_removegreen_nohud"
        };
        weapons[] = {};
    };
};

class CfgGlasses
{
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Blue_Kama;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Blue_Kama_removeblue;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Blue_Kama_removered;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Blue_Kama_removegreen;

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Blue_Kama_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Blue_Kama_removeblue_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Blue_Kama_removered_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Blue_Kama_removegreen_nohud;

    //Hud//

    //Sr. CS+
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Red_Kama: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Blue_Kama
    {
        displayname = "[501st] FW HUD VEST 09 - Kama";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_bn_csm_vest_co.paa",
            ""
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Kamas";
            hud = "hud_on";
            colorblind = "clear";
            mos = "medic";
            rank = "sr_cs";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Red_Kama_removeblue: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Blue_Kama_removeblue
    {
        displayname = "[501st] FW HUD VEST 09 - Kama - Blue";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_bn_csm_vest_co.paa",
            ""
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Kamas";
            hud = "hud_on";
            colorblind = "no_blue";
            mos = "medic";
            rank = "sr_cs";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Red_Kama_removered: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Blue_Kama_removered
    {
        displayname = "[501st] FW HUD VEST 09 - Kama - Red";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_bn_csm_vest_co.paa",
            ""
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Kamas";
            hud = "hud_on";
            colorblind = "no_red";
            mos = "medic";
            rank = "sr_cs";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Red_Kama_removegreen: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Blue_Kama_removegreen
    {
        displayname = "[501st] FW HUD VEST 09 - Kama - Green";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_bn_csm_vest_co.paa",
            ""
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Kamas";
            hud = "hud_on";
            colorblind = "no_green";
            mos = "medic";
            rank = "sr_cs";
        };
    };
    
    //No Hud//

    //Sr. CS+
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Red_Kama_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Blue_Kama_nohud
    {
        displayname = "[501st] FW HUDless VEST 09 - Kama";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_bn_csm_vest_co.paa",
            ""
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Kamas";
            hud = "hud_off";
            colorblind = "clear";
            mos = "medic";
            rank = "sr_cs";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Red_Kama_removeblue_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Blue_Kama_removeblue_nohud
    {
        displayname = "[501st] FW HUDless VEST 09 - Kama - Blue";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_bn_csm_vest_co.paa",
            ""
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Kamas";
            hud = "hud_off";
            colorblind = "no_blue";
            mos = "medic";
            rank = "sr_cs";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Red_Kama_removered_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Blue_Kama_removered_nohud
    {
        displayname = "[501st] FW HUDless VEST 09 - Kama - Red";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_bn_csm_vest_co.paa",
            ""
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Kamas";
            hud = "hud_off";
            colorblind = "no_red";
            mos = "medic";
            rank = "sr_cs";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Red_Kama_removegreen_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Blue_Kama_removegreen_nohud
    {
        displayname = "[501st] FW HUDless VEST 09 - Kama - Green";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_bn_csm_vest_co.paa",
            ""
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Kamas";
            hud = "hud_off";
            colorblind = "no_green";
            mos = "medic";
            rank = "sr_cs";
        };
    };
};