class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_Company_CSM_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Battalion_CSM_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Major_Uniform_Vehicle"
        };
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_Uniform_Base",
            "Aux501_Units_Republic_501st_Standard_Uniform",
            "Aux501_Units_Republic_501st_Cadet_Uniform",
            "Aux501_Units_Republic_501st_Trooper_Uniform",
            "Aux501_Units_Republic_501st_Snr_Trooper_Uniform",
            "Aux501_Units_Republic_501st_Vet_Trooper_Uniform",
            "Aux501_Units_Republic_501st_Lance_CP_Uniform",

            "Aux501_Units_Republic_501st_CP_Uniform",
            "Aux501_Units_Republic_501st_Snr_CP_Uniform",
            "Aux501_Units_Republic_501st_CS_Uniform",
            "Aux501_Units_Republic_501st_Snr_CS_Uniform",

            "Aux501_Units_Republic_501st_Platoon_CSM_Uniform",
            "Aux501_Units_Republic_501st_Company_CSM_Uniform",
            "Aux501_Units_Republic_501st_Battalion_CSM_Uniform",

            "Aux501_Units_Republic_501st_2LT_Uniform",
            "Aux501_Units_Republic_501st_1LT_Uniform",
            "Aux501_Units_Republic_501st_Captain_Uniform",
            "Aux501_Units_Republic_501st_Major_Uniform"
        };
    };
};

class CfgWeapons
{
    class Uniform_Base;
    class UniformItem;

    class Aux501_Units_Republic_501st_Uniform_Base: Uniform_Base
    {
        scope = 1;
        scopeArsenal = 1;
        author = "501st Aux Team";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_uniform_ca.paa";
        model = "\SWLB_groundholders\SWLB_clone_uniform_gh.p3d";
        class ItemInfo: UniformItem
        {
            uniformClass = "Aux501_Units_Republic_501st_Unit_Base";
            armor = 100;
            armorStructural = 5;
            explosionShielding = 1.1;
            impactDamageMultiplier = -100;
            modelSides[] = {6};
            uniformType = "Neopren";
            containerClass = "Supply100";
            mass = 40;
        };
    };

    class Aux501_Units_Republic_501st_Standard_Uniform: Aux501_Units_Republic_501st_Uniform_Base
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] INF P2 ARMR 00 - Blank";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Recruit_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            mos = "infantry";
            rank = "blank";
            unit = "default";
            style = "standard";
        };
    };
    class Aux501_Units_Republic_501st_Cadet_Uniform: Aux501_Units_Republic_501st_Standard_Uniform
    {
        displayName = "[501st] INF P2 ARMR 01 - CR-C";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Cadet_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "standard";
            rank = "cr_c";
        };
    };
    class Aux501_Units_Republic_501st_Trooper_Uniform: Aux501_Units_Republic_501st_Standard_Uniform
    {
        picture = "\SWLB_units\data\ui\icon_SWLB_clone_501stTrooper_uniform_ca.paa";
        displayName = "[501st] INF P2 ARMR 02 - CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Trooper_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "standard";
            rank = "ct";
        };
    };
    class Aux501_Units_Republic_501st_Snr_Trooper_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 03 - Sr. CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "standard";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_Vet_Trooper_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 04 - Vet. CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "standard";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501st_Lance_CP_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 05 - CLC";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Lance_CP_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "standard";
            rank = "clc";
        };
    };
    
    //NCOs
    class Aux501_Units_Republic_501st_CP_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 06 - CP";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_CP_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "standard";
            rank = "cp";
        };
    };
    class Aux501_Units_Republic_501st_Snr_CP_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 07 - Sr. CP";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Snr_CP_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "standard";
            rank = "sr_cp";
        };
    };
    class Aux501_Units_Republic_501st_CS_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 08 - CS";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_CS_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "standard";
            rank = "cs";
        };
    };
    class Aux501_Units_Republic_501st_Snr_CS_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 09 - Sr. CS";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Snr_CS_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "standard";
            rank = "sr_cs";
        };
    };
    
    //CS-Ms
    class Aux501_Units_Republic_501st_Platoon_CSM_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 10 - CS-M";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Platoon_CSM_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "standard";
            rank = "csm";
        };
    };
    class Aux501_Units_Republic_501st_Company_CSM_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 11 - CO CS-M";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Company_CSM_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "standard";
            rank = "c_csm";
        };
    };
    class Aux501_Units_Republic_501st_Battalion_CSM_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 12 - BN CS-M";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Battalion_CSM_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "standard";
            rank = "b_csm";
        };
    };
    
    //Officers
    class Aux501_Units_Republic_501st_2LT_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 13 - 2nd Lt";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_2LT_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "standard";
            rank = "second_Lt";
        };
    };
    class Aux501_Units_Republic_501st_1LT_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 14 - 1st Lt";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_1LT_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "standard";
            rank = "first_Lt";
        };
    };
    class Aux501_Units_Republic_501st_Captain_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 15 - Captain";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Captain_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "standard";
            rank = "captain";
        };
    };
    class Aux501_Units_Republic_501st_Major_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 16 - Major";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Major_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "standard";
            rank = "major";
        };
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Unit_Base;

    class Aux501_Units_Republic_501st_Company_CSM_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        uniformClass = "Aux501_Units_Republic_501st_Company_CSM_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_comp_csm_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_comp_csm_armor_lower_co.paa"
        };
    };

    class Aux501_Units_Republic_501st_Battalion_CSM_Uniform_Vehicle: Aux501_Units_Republic_501st_Company_CSM_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501st_Battalion_CSM_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_bn_csm_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_bn_csm_armor_lower_co.paa"
        };
    };

    class Aux501_Units_Republic_501st_Major_Uniform_Vehicle: Aux501_Units_Republic_501st_Company_CSM_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501st_Major_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_maj_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_maj_armor_lower_co.paa"
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "mos",
                "style",
                "rank",
                "unit"
            };
            class mos
            {
                label = "MOS";
                hiddenselection = "camo1";
                values[] = 
                {
                    "infantry",
                    "medic",
                    "rto",
                    "arc"
                };
                class infantry
                {
                    label = "GI";
                };
                class medic
                {
                    label = "Medic";
                };
                class rto
                {
                    label = "RTO";
                };
                class arc
                {
                    label = "ARC";
                };
            };
            class rank
            {
                label = "Rank";
                hiddenselection = "camo2";
                values[] = 
                {
                    "blank",
                    "cr_c",
                    "ct",
                    "sr_ct",
                    "vet_ct",
                    "clc",

                    "cp",
                    "sr_cp",
                    "cs",
                    "sr_cs",
                    "csm",
                    "c_csm",
                    "b_csm",

                    "second_Lt",
                    "first_Lt",
                    "captain",
                    "major"
                };
                class blank
                {
                    label = "Blank";
                };
                class cr_c
                {
                    label = "CR-C";
                };
                class ct
                {
                    label = "CT";
                };
                class sr_ct
                {
                    label = "Sr. CT";
                };
                class vet_ct
                {
                    label = "Vet. CT";
                };
                class clc
                {
                    label = "CLC";
                };
                class cp
                {
                    label = "CP";
                };
                class sr_cp
                {
                    label = "Sr. CP";
                };
                class cs
                {
                    label = "CS";
                };
                class sr_cs
                {
                    label = "Sr. CS";
                };
                class csm
                {
                    label = "CS-M";
                };
                class c_csm
                {
                    label = "CO CS-M";
                };
                class b_csm
                {
                    label = "BN CS-M";
                };
                class second_Lt
                {
                    label = "2nd Lt";
                };
                class first_Lt
                {
                    label = "1st Lt";
                };
                class captain
                {
                    label = "Captain";
                };
                class major
                {
                    label = "Major";
                };
            };
            class unit
            {
                label = "Unit";
                values[] = 
                {
                    "default",

                    "squad_1_1",
                    "squad_1_2",
                    "squad_1_3",

                    "squad_2_1",
                    "squad_2_2",
                    "squad_2_3",

                    "squad_3_1",
                    "squad_3_2",
                    "squad_3_3"
                };
   
                class default
                {
                    label = "Default";
                };
                class squad_1_1
                {
                    label = "1-1";
                };
                class squad_1_2
                {
                    label = "1-2";
                };
                class squad_1_3
                {
                    label = "1-3";
                };

                class squad_2_1
                {
                    label = "2-1";
                };
                class squad_2_2
                {
                    label = "2-2";
                };
                class squad_2_3
                {
                    label = "2-3";
                };

                class squad_3_1
                {
                    label = "3-1";
                };
                class squad_3_2
                {
                    label = "3-2";
                };
                class squad_3_3
                {
                    label = "3-3";
                };
            };
            class style
            {
                label = "Style";
                values[] = 
                {
                    "standard",
                    "grenade_1x",
                    "grenade_2x",
                    "recon",
                    "officer_alt"
                };
                class standard
                {
                    label = "Standard";
                };
                class grenade_1x
                {
                    label = "1x Nade";
                };
                class grenade_2x
                {
                    label = "2x Nade";
                };
                class recon
                {
                    label = "Recon";
                };
                class officer_alt
                {
                    label = "Officer Alt";
                };
            };
        };
    };
};