class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Warrant_Officers_Uniforms
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_WO1_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_WO2_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_WO3_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_WO4_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_WO5_Uniform_Vehicle"
        };
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_WO1_Uniform",
            "Aux501_Units_Republic_501st_WO2_Uniform",
            "Aux501_Units_Republic_501st_WO3_Uniform",
            "Aux501_Units_Republic_501st_WO4_Uniform",
            "Aux501_Units_Republic_501st_WO5_Uniform"
        };
    };
};

class CfgWeapons
{
    class Uniform_Base;

    class Aux501_Units_Republic_501st_Uniform_Base: Uniform_Base
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501st_WO1_Uniform: Aux501_Units_Republic_501st_Uniform_Base
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] WO P2 ARMR 01 - WO1";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_WO1_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_WO_Uniforms";
            rank = "wo1";
            style = "standard";
        };
    };

    class Aux501_Units_Republic_501st_WO2_Uniform: Aux501_Units_Republic_501st_WO1_Uniform
    {
        displayName = "[501st] WO P2 ARMR 02 - WO2";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_WO2_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_WO_Uniforms";
            rank = "wo2";
            style = "standard";
        };
    };
    class Aux501_Units_Republic_501st_WO3_Uniform: Aux501_Units_Republic_501st_WO1_Uniform
    {
        displayName = "[501st] WO P2 ARMR 03 - WO3";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_WO3_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_WO_Uniforms";
            rank = "wo3";
            style = "standard";
        };
    };

    class Aux501_Units_Republic_501st_WO4_Uniform: Aux501_Units_Republic_501st_WO1_Uniform
    {
        displayName = "[501st] WO P2 ARMR 04 - WO4";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_WO4_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_WO_Uniforms";
            rank = "wo4";
            style = "standard";
        };
    };

    class Aux501_Units_Republic_501st_WO5_Uniform: Aux501_Units_Republic_501st_WO1_Uniform
    {
        displayName = "[501st] WO P2 ARMR 05 - WO5";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_WO5_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_WO_Uniforms";
            rank = "wo5";
            style = "standard";
        };
    };
};

class cfgVehicles
{
    class Aux501_Units_Republic_501st_Unit_Base;

    class Aux501_Units_Republic_501st_WO1_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo1_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_WO1_Uniform";
    };

    class Aux501_Units_Republic_501st_WO2_Uniform_Vehicle: Aux501_Units_Republic_501st_WO1_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo2_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_WO2_Uniform";
    };

    class Aux501_Units_Republic_501st_WO3_Uniform_Vehicle: Aux501_Units_Republic_501st_WO1_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo3_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo3_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_WO3_Uniform";
    };

    class Aux501_Units_Republic_501st_WO4_Uniform_Vehicle: Aux501_Units_Republic_501st_WO1_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo4_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo3_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_WO4_Uniform";
    };

    class Aux501_Units_Republic_501st_WO5_Uniform_Vehicle: Aux501_Units_Republic_501st_WO1_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo5_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo3_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_WO5_Uniform";
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_WO_Uniforms
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "rank",
                "style"
            };
            class rank
            {
                label = "Rank";
                values[] = 
                {
                    "wo1",
                    "wo2",
                    "wo3",
                    "wo4",
                    "wo5"
                };
                class wo1
                {
                    label = "WO1";
                };
                class wo2
                {
                    label = "WO2";
                };
                class wo3
                {
                    label = "WO3";
                };
                class wo4
                {
                    label = "WO4";
                };
                class wo5
                {
                    label = "WO5";
                };
            };
            class style
            {
                label = "Style";
                values[] = 
                {
                    "standard",
                    "grenade_1x",
                    "grenade_2x",
                    "recon"
                };
                class standard
                {
                    label = "Standard";
                };
                class grenade_1x
                {
                    label = "1x Nade";
                };
                class grenade_2x
                {
                    label = "2x Nade";
                };
                class recon
                {
                    label = "Recon";
                };
            };
        };
    };
};