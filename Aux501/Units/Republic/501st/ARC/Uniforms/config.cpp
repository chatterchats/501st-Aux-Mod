class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_ARC_Uniforms
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_ARC_Trooper_Uniform",
            "Aux501_Units_Republic_501st_ARC_NCO_Uniform",
            "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_1_1",
            "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_1_2",
            "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_1_3",
            "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_2_1",
            "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_2_2",
            "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_2_3",
            "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_3_1",
            "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_3_2",
            "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_3_3"
        };
    };
};

class CfgWeapons
{
    class Uniform_Base;

    class Aux501_Units_Republic_501st_Uniform_Base: Uniform_Base
    {
        class ItemInfo;
    };

    //ARC Troopers
    class Aux501_Units_Republic_501st_ARC_Trooper_Uniform: Aux501_Units_Republic_501st_Uniform_Base
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] ARC P2 ARMR 01 - CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_ARC_Trooper_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "arc";
            style = "standard";
            rank = "ct";
        };
    };

    class Aux501_Units_Republic_501st_ARC_NCO_Uniform: Aux501_Units_Republic_501st_ARC_Trooper_Uniform
    {
        displayName = "[501st] ARC P2 ARMR 02 - NCO";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_ARC_CP_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "arc";
            style = "standard";
            rank = "cp";
        };
    };
};