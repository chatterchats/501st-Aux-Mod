class cfgPatches
{
    class Aux501_Patch_Units_Civilians
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = {};
    };
};

class CfgFactionClasses
{
    class Aux501_FactionClasses_Civilians
    {
        displayName = "[501st] Civilians";
        priority = 1;
        side = 3;
        icon = "";
        flag = "";
    };
};

class CfgEditorCategories
{
    class Aux501_Editor_Category_Civilians
    {
       displayName = "[501st] Civilians";
    };
};

class CfgEditorSubcategories
{
    class Aux501_Editor_Subcategory_Republic
    {
       displayName = "Republic";
    };
    class Aux501_Editor_Subcategory_Confederacy
    {
       displayName = "Confederacy";
    };
};