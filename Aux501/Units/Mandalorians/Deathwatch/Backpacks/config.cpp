class cfgPatches
{
    class Aux501_Patch_Units_Mandalorian_Deathwatch_Backpacks
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_Mandalorian_Backpacks_Jumppack"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501_Infantry_Backpacks_base;

    class Aux501_Units_Mandalorian_Backpacks_Jumppack: Aux501_Units_Republic_501_Infantry_Backpacks_base
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[Mando] Jetpack 01 - Black";
        picture = "\MRC\JLTS\characters\CloneArmor2\data\ui\Clone_jumppack_jt12_ui_ca.paa";
        model = "\ls_equipment_greenfor\backpack\mandalorian\journeymanJet\ls_greenfor_journeymanJetpack_backpack";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\ls_equipment_greenfor\backpack\mandalorian\journeymanJet\data\backpack_co.paa"};
    };
    class Aux501_Units_Mandalorian_Backpacks_Jumppack_AT: Aux501_Units_Mandalorian_Backpacks_Jumppack
    {
        scope = 1;
        scopeArsenal = 1;
        class TransportMagazines
        {
            class _xx_at_mag
            {
                count = 4;
                magazine = "Aux501_Weapons_Mags_hh12_at";
            };
            class _xx_he_mag
            {
                count = 2;
                magazine = "Aux501_Weapons_Mags_hh12_he";
            };
        };
    };
    class Aux501_Units_Mandalorian_Backpacks_Jumppack_AA: Aux501_Units_Mandalorian_Backpacks_Jumppack_AT
    {
        class TransportMagazines
        {
            class _xx_aa_mag
            {
                count = 3;
                magazine = "Aux501_Weapons_Mags_hh12_aa";
            };
        };
    };
};