class cfgPatches
{
    class Aux501_Patch_Units_CIS_Humans
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_CIS_Humans_Soldier_Unit",
            "Aux501_Units_CIS_Humans_Heavy_Unit",
            "Aux501_Units_CIS_Humans_Marksman_Unit",
            "Aux501_Units_CIS_Humans_Breacher_Unit",
            "Aux501_Units_CIS_Humans_AT_Unit",
            "Aux501_Units_CIS_Humans_AA_Unit",
            "Aux501_Units_CIS_Humans_NCO_Unit",
            "Aux501_Units_CIS_Humans_Crew_Unit",
            "Aux501_Units_CIS_Humans_Commander_Unit"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class O_Soldier_base_F;
    class O_Soldier_F: O_Soldier_base_F
    {
        class HitPoints;
    };
    
    class Aux501_Units_CIS_Humans_Base: O_Soldier_F
    {
        scope = 0;
        scopeCurator = 0;
        author = "501st Aux Team";
        identityTypes[] = 
        {
            "LanguageENG_F",
            "Head_EURO",
            "Head_African",
            "Head_Asian",
            "Head_Greek",
            "Head_m_mirialan",
            "Head_m_zelosian",
            "Head_m_zeltron"
        };
        faceType = "Man_A3";
        side = 0;
        genericNames = "EnglishMen";
        faction = "Aux501_FactionClasses_Confederacy";
        editorCategory = "Aux501_Editor_Category_Confederacy";
        editorSubcategory = "Aux501_Editor_Subcategory_Human_Legion";
        vehicleClass = "Men";
        portrait = "";
        picture = "";
        icon = "iconMan";
        accuracy = 2.3;
        threat[] = {1,0.1,0.1};
        camouflage = 1.4;
        minFireTime = 7;
        canCarryBackPack = 1;
        armor = 5;
        armorStructural = 4;
        explosionShielding = 0.4;
        impactEffectsBlood = "ImpactMetal";
		impactEffectsNoBlood = "ImpactPlastic";
        canBleed = 0;
        class HitPoints: HitPoints
        {
            class HitFace
            {
                armor = 1;
                material = -1;
                name = "face_hub";
                passThrough = 0.8;
                radius = 0.08;
                explosionShielding = 0.1;
                minimalHit = 0.01;
            };
            class HitNeck: HitFace
            {
                armor = 1;
                material = -1;
                name = "neck";
                passThrough = 0.8;
                radius = 0.1;
                explosionShielding = 0.5;
                minimalHit = 0.01;
            };
            class HitHead: HitNeck
            {
                armor = 1;
                material = -1;
                name = "head";
                passThrough = 0.8;
                radius = 0.2;
                explosionShielding = 0.5;
                minimalHit = 0.01;
                depends = "HitFace max HitNeck";
            };
            class HitPelvis: HitHead
            {
                armor = 6;
                material = -1;
                name = "pelvis";
                passThrough = 0.8;
                radius = 0.24;
                explosionShielding = 1;
                visual = "injury_body";
                minimalHit = 0.01;
                depends = "0";
            };
            class HitAbdomen: HitPelvis
            {
                armor = 5;
                material = -1;
                name = "spine1";
                passThrough = 0.8;
                radius = 0.16;
                explosionShielding = 1;
                visual = "injury_body";
                minimalHit = 0.01;
            };
            class HitDiaphragm: HitAbdomen
            {
                armor = 5;
                material = -1;
                name = "spine2";
                passThrough = 0.8;
                radius = 0.18;
                explosionShielding = 2.4;
                visual = "injury_body";
                minimalHit = 0.01;
            };
            class HitChest: HitDiaphragm
            {
                armor = 5;
                material = -1;
                name = "spine3";
                passThrough = 0.8;
                radius = 0.18;
                explosionShielding = 2.4;
                visual = "injury_body";
                minimalHit = 0.01;
            };
            class HitBody: HitChest
            {
                armor = 1000;
                material = -1;
                name = "body";
                passThrough = 1;
                radius = 0;
                explosionShielding = 2.4;
                visual = "injury_body";
                minimalHit = 0.01;
                depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
            };
            class HitArms: HitBody
            {
                armor = 5;
                material = -1;
                name = "arms";
                passThrough = 1;
                radius = 0.1;
                explosionShielding = 0.3;
                visual = "injury_hands";
                minimalHit = 0.01;
                depends = "0";
            };
            class HitHands: HitArms
            {
                armor = 5;
                material = -1;
                name = "hands";
                passThrough = 1;
                radius = 0.1;
                explosionShielding = 0.3;
                visual = "injury_hands";
                minimalHit = 0.01;
                depends = "HitArms";
            };
            class HitLegs: HitHands
            {
                armor = 5;
                material = -1;
                name = "legs";
                passThrough = 1;
                radius = 0.14;
                explosionShielding = 0.3;
                visual = "injury_legs";
                minimalHit = 0.01;
                depends = "0";
            };
            class Incapacitated: HitLegs
            {
                armor = 1000;
                material = -1;
                name = "body";
                passThrough = 1;
                radius = 0;
                explosionShielding = 1;
                visual = "";
                minimalHit = 0;
                depends = "(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
            };
        };
        role = "Rifleman";
        cost = 100000;
        canHideBodies = 0;
        detectSkill = 12;
        canDeactivateMines = 0;
        class SpeechVariants
        {
            class Default
            {
                speechSingular[] = {"veh_infantry_s"};
                speechPlural[] = {"veh_infantry_p"};
            };
        };
        textSingular = "$STR_A3_namesound_veh_infantry_s";
        textPlural = "$STR_A3_namesound_veh_infantry_p";
        nameSound = "veh_infantry_s";
        maxSpeed = 24;
        maxTurnAngularVelocity = 3;
        lyingLimitSpeedHiding = 0.8;
        crouchProbabilityHiding = 0.8;
        lyingLimitSpeedCombat = 1.8;
        crouchProbabilityCombat = 0.4;
        crouchProbabilityEngage = 0.75;
        lyingLimitSpeedStealth = 2;
        items[] = {"FirstAidKit"};
        respawnItems[] = {"FirstAidKit"};
    };

    class Aux501_Units_CIS_Humans_Soldier_Unit: Aux501_Units_CIS_Humans_Base
    {
        scope = 2;
        scopeCurator = 2;
        displayName = "Grunt";
        model = "SFA_Equipment_S\Uniform\Sith_Trooper\Sith_Trooper.p3d";
        hiddenSelections[] = {"camo1","camo2","camo3","insignia"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\CIS\Human_Legion\Uniforms\data\textures\CIS_Human_Trooper_Uniform_CO.paa",
            "SFA_Equipment_S\Uniform\Sith_Trooper\data\Undersuit_co.paa",
            "SFA_Equipment_S\Uniform\Sith_Trooper\data\gloves_co.paa"
        };
        uniformClass = "Aux501_Units_CIS_Humans_Soldier_Uniform";
        backpack = "Aux501_Units_CIS_Humans_Backpack_standard";
        editorPreview = "";
        weapons[] = {"Aux501_Weaps_E5","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_E5","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        linkeditems[] = 
        {
            "Aux501_Units_CIS_Humans_Helmets_Infantry",

            "Aux501_Units_CIS_Humans_Vests_Standard",
            
            "ls_jn_goggles_facewear",
            "JLTS_droid_comlink",
            "JLTS_NVG_droid_chip_1"
        };
        respawnlinkeditems[] = 
        {
            "Aux501_Units_CIS_Humans_Helmets_Infantry",

            "Aux501_Units_CIS_Humans_Vests_Standard",
            
            "ls_jn_goggles_facewear",
            "JLTS_droid_comlink",
            "JLTS_NVG_droid_chip_1"
        };
    };
    class Aux501_Units_CIS_Humans_Heavy_Unit: Aux501_Units_CIS_Humans_Soldier_Unit
    {
        displayName = "Support Gunner";
        icon = "JLTS_iconManSupportGunner";
        weapons[] = {"Aux501_Weaps_E5C","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150"
        };
        respawnWeapons[] = {"Aux501_Weaps_E5C","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150"
        };
        linkeditems[] = 
        {
            "Aux501_Units_CIS_Humans_Helmets_Infantry",

            "Aux501_Units_CIS_Humans_Vests_Heavy",
            
            "ls_jn_goggles_facewear",
            "JLTS_droid_comlink",
            "JLTS_NVG_droid_chip_1"
        };
        respawnlinkeditems[] = 
        {
            "Aux501_Units_CIS_Humans_Helmets_Infantry",

            "Aux501_Units_CIS_Humans_Vests_Heavy",
            
            "ls_jn_goggles_facewear",
            "JLTS_droid_comlink",
            "JLTS_NVG_droid_chip_1"
        };
    };
    class Aux501_Units_CIS_Humans_Marksman_Unit: Aux501_Units_CIS_Humans_Soldier_Unit
    {
        displayName = "Marksman";
        icon = "JLTS_iconManSniper";
        weapons[]=
        {
            "Aux501_Weaps_E5S",
            "Throw",
            "Put"
        };
        respawnWeapons[]=
        {
            "Aux501_Weaps_E5S",
            "Throw",
            "Put"
        };
        magazines[]=
        {
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnMagazines[]=
        {
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_CISDetonator"
        };
    };
    class Aux501_Units_CIS_Humans_Breacher_Unit: Aux501_Units_CIS_Humans_Soldier_Unit
    {
        displayName = "Breacher";
        icon = "iconMan";
        weapons[] = {"Aux501_Weaps_SBB3","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_SBB3","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_CISDetonator"
        };
    };
    class Aux501_Units_CIS_Humans_AT_Unit: Aux501_Units_CIS_Humans_Heavy_Unit
    {
        displayName = "AT Soldier";
        threat[] = {0.6,1,0.3};
        icon = "iconManAT";
        backpack = "Aux501_Units_CIS_Humans_Backpack_AT";
        weapons[] = {"Aux501_Weaps_E5","Aux501_Weaps_e60r_at","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_e60r_at",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_E5","Aux501_Weaps_e60r_at","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_e60r_at",
            "Aux501_Weapons_Mags_CISDetonator"
        };
    };
    class Aux501_Units_CIS_Humans_AA_Unit: Aux501_Units_CIS_Humans_AT_Unit
    {
        displayName = "AA Soldier";
        threat[] = {0.2,0.1,1};
        backpack = "Aux501_Units_CIS_Humans_Backpack_AA";
        weapons[] = {"Aux501_Weaps_E5","Aux501_Weaps_e60r_aa","Throw","Put"};
        magazines[]=
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_e60r_aa",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_E5","Aux501_Weaps_e60r_aa","Throw","Put"};
        respawnMagazines[] =
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_e60r_aa",
            "Aux501_Weapons_Mags_CISDetonator"
        };
    };
    class Aux501_Units_CIS_Humans_NCO_Unit: Aux501_Units_CIS_Humans_Soldier_Unit
    {
        displayName = "NCO";
        cost = 125000;
        icon = "iconManLeader";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\CIS\Human_Legion\Uniforms\data\textures\CIS_Human_NCO_Uniform_CO.paa",
            "SFA_Equipment_S\Uniform\Sith_Trooper\data\Undersuit_co.paa",
            "SFA_Equipment_S\Uniform\Sith_Trooper\data\gloves_co.paa"
        };
        uniformClass = "Aux501_Units_CIS_Humans_NCO_Uniform";
        linkeditems[] = 
        {
            "Aux501_Units_CIS_Humans_Helmets_Infantry",

            "Aux501_Units_CIS_Humans_Vests_NCO",
            
            "ls_jn_goggles_facewear",
            "JLTS_droid_comlink",
            "JLTS_NVG_droid_chip_1"
        };
        respawnlinkeditems[] = 
        {
            "Aux501_Units_CIS_Humans_Helmets_Infantry",

            "Aux501_Units_CIS_Humans_Vests_NCO",
            
            "ls_jn_goggles_facewear",
            "JLTS_droid_comlink",
            "JLTS_NVG_droid_chip_1"
        };
    }; 
    
    class Aux501_Units_CIS_Humans_Crew_Unit: Aux501_Units_CIS_Humans_Soldier_Unit
    {
        displayName = "Vehicle Crew";
        model = "\IBL\characters\pilot\pilot_uniform.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\IBL\characters\pilot\data\pilot_uniform_1_co.paa"};
        uniformClass = "Aux501_Units_CIS_Humans_Crew_Uniform";
        linkeditems[] = 
        {
            "Aux501_Units_CIS_Humans_Helmets_Infantry",

            "Aux501_Units_CIS_Humans_Vests_Crew",
            
            "SWLB_CEE_Clone_Scarf",
            "ls_jn_goggles_nvg",
            "JLTS_droid_comlink"
        };
        respawnlinkeditems[] = 
        {
            "Aux501_Units_CIS_Humans_Helmets_Infantry",

            "Aux501_Units_CIS_Humans_Vests_Crew",
            
            "SWLB_CEE_Clone_Scarf",
            "ls_jn_goggles_nvg",
            "JLTS_droid_comlink"
        };
    };

    class Aux501_Units_CIS_Humans_Commander_Unit: Aux501_Units_CIS_Humans_Soldier_Unit
    {
        role = "Rifleman";
        cost = 600000;
        camouflage = 1.6;
        icon = "iconManOfficer";
        displayName = "Commander";
        backpack = "";
        class SpeechVariants
        {
            class Default
            {
                speechSingular[] = {"veh_infantry_officer_s"};
                speechPlural[] = {"veh_infantry_officer_p"};
            };
        };
        textSingular = "$STR_A3_namesound_veh_infantry_officer_s";
        textPlural = "$STR_A3_namesound_veh_infantry_officer_p";
        nameSound = "veh_infantry_officer_s";
        editorPreview = "";
        model = "\OPTRE_UNSC_Units\Army\officer.p3d";
        hiddenSelections[] = {"camo1","camo2","insignia","odst","bar_1","bar_2","awards","nametag"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\CIS\Human_Legion\Uniforms\data\textures\CIS_Human_Officer_Uniform_CO.paa",
            "\Aux501\Units\CIS\Human_Legion\Uniforms\data\textures\CIS_Human_Officer_Rank_CO.paa",
            "",
            "",
            "",
            "",
            "",
            ""
        };
        uniformClass = "Aux501_Units_CIS_Humans_Commander_Uniform";
        linkedItems[] = 
        {
            "Aux501_Units_CIS_Humans_Helmets_Commander_Cap",

            "ItemMap",
            "SWLB_comlink_droid"
        };
        respawnLinkedItems[] = 
        {
            "Aux501_Units_CIS_Humans_Helmets_Commander_Cap",

            "ItemMap",
            "SWLB_comlink_droid"
        };
        weapons[] = {"Aux501_Weaps_RG4D","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30"
        };
        respawnWeapons[] = {"Aux501_Weaps_RG4D","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30"
        };
        class HitPoints: HitPoints
        {
            class HitNeck: HitNeck
            {
                armor = 2;
            };
            class HitPelvis: HitPelvis
            {
                armor = 2;
            };
            class HitAbdomen: HitAbdomen
            {
                armor = 2;
            };
            class HitDiaphragm: HitDiaphragm
            {
                armor = 2;
            };
            class HitChest: HitChest
            {
                armor = 2;
            };
            class HitArms: HitArms
            {
                armor = 2;
            };
            class HitHands: HitHands
            {
                armor = 2;
            };
            class HitLegs: HitLegs
            {
                armor = 2;
            };
        };
        class Wounds
        {
            tex[] = {};
            mat[] = 
            {
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat"
            };
        };
    };
};

class CfgGroups
{
    class East
    {
        class Aux501_Group_Faction_Confederacy
        {
            name = "[501st] Confederacy";
            class Aux501_Group_Editor_Category_Human_Legion
            {
                name = "Human Legion";
                class Aux501_cis_humans_infantry_section
                {
                    name = "Infantry Section";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    rarityGroup = 1;
                    class Unit0
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_Humans_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1: Unit0
                    {
                        vehicle = "Aux501_Units_CIS_Humans_AT_Unit";
                        rank = "CORPORAL";
                        position[] = {5,-5,0};
                    };
                    class Unit2: Unit1
                    {
                        vehicle = "Aux501_Units_CIS_Humans_Soldier_Unit";
                        rank = "PRIVATE";
                        position[] = {-5,-5,0};
                    };
                    class Unit3: Unit2
                    {
                        vehicle = "Aux501_Units_CIS_Humans_Heavy_Unit";
                        position[] = {10,-10,0};
                    };
                    class Unit4: Unit2
                    {
                        position[] = {-10,-10,0};
                    };
                    class Unit5: Unit2
                    {
                        position[] = {15,-15,0};
                    };
                    class Unit6: Unit2
                    {
                        vehicle = "Aux501_Units_CIS_Humans_Breacher_Unit";
                        position[] = {-15,-15,0};
                    };
                    class Unit7: Unit3
                    {
                        vehicle = "Aux501_Units_CIS_Humans_Marksman_Unit";
                        position[] = {20,-20,0};
                    };
                    class Unit8: Unit2
                    {
                        vehicle = "Aux501_Units_CIS_Humans_Heavy_Unit";
                        position[] = {-20,-20,0};
                    };
                };
                class Aux501_cis_humans_weapons_team: Aux501_cis_humans_infantry_section
                {
                    name = "Weapons Team";
                    class Unit0: Unit0{};
                    class Unit1: Unit1
                    {
                        vehicle = "Aux501_Units_CIS_Humans_Soldier_Unit";
                    };
                    class Unit2: Unit2{};
                    class Unit3: Unit3
                    {
                        vehicle = "Aux501_Units_CIS_Humans_AA_Unit";
                    };
                    class Unit4: Unit4{};
                    class Unit5: Unit5
                    {
                        vehicle = "Aux501_Units_CIS_Humans_AT_Unit_heavy";
                    };
                    class Unit6: Unit6{};

                    class Unit7: Unit7{};
                    
                };
                class Aux501_cis_humans_infantry_team: Aux501_cis_humans_infantry_section
                {
                    name = "Infantry Team";
                    class Unit0: Unit0{};
                    class Unit1: Unit1{};
                    class Unit2: Unit2{};
                    class Unit3: Unit3{};
                };
                class Aux501_cis_humans_at_team: Aux501_cis_humans_infantry_team
                {
                    name = "AT Team";
                    class Unit0: Unit0{};
                    class Unit1: Unit1
                    {
                        vehicle = "Aux501_Units_CIS_Humans_AT_Unit";
                    };
                    class Unit2: Unit2
                    {
                        vehicle = "Aux501_Units_CIS_Humans_AT_Unit";
                    };
                    class Unit3: Unit3
                    {
                        vehicle = "Aux501_Units_CIS_Humans_AT_Unit";
                    };
                };
                class Aux501_cis_humans_aa_team: Aux501_cis_humans_at_team
                {
                    name = "AA Team";
                    class Unit0: Unit0{};
                    class Unit1: Unit1
                    {
                        vehicle = "Aux501_Units_CIS_Humans_AA_Unit";
                    };
                    class Unit2: Unit2
                    {
                        vehicle = "Aux501_Units_CIS_Humans_AA_Unit";
                    };
                    class Unit3: Unit3
                    {
                        vehicle = "Aux501_Units_CIS_Humans_AA_Unit";
                    };
                };
                class Aux501_cis_humans_infantry_sentry: Aux501_cis_humans_aa_team
                {
                    name = "Infantry Sentry";
                    class Unit0: Unit0
                    {
                        rank = "CORPORAL";
                    };
                    class Unit1: Unit1
                    {
                        vehicle = "Aux501_Units_CIS_Humans_Soldier_Unit";
                        rank = "PRIVATE";
                    };
                };
                class Aux501_cis_humans_sniper_team: Aux501_cis_humans_infantry_section
                {
                    name = "Sniper Team";
                    class Unit0: Unit0
                    {
                        vehicle = "Aux501_Units_CIS_Humans_Marksman_Unit";
                    };
                    class Unit1: Unit1
                    {
                        vehicle = "Aux501_Units_CIS_Humans_Marksman_Unit";
                    };
                };
                class Aux501_cis_humans_command: Aux501_cis_humans_infantry_team
                {
                    name = "Command Section";
                    class Unit0
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_Humans_Commander_Unit";
                        rank = "CAPTAIN";
                        position[] = {0,0,0};
                    };
                    class Unit1: Unit0
                    {
                        vehicle = "Aux501_Units_CIS_Humans_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {5,-5,0};
                    };
                    class Unit2
                    {
                        vehicle = "Aux501_Units_CIS_Humans_Breacher_Unit";
                        rank = "PRIVATE";
                        position[] = {-5,-5,0};
                    };
                    class Unit3:Unit2
                    {
                        vehicle = "RD501_opfor_unit_BX_shield";
                        position[] = {10,-10,0};
                    };
                    class Unit4:Unit3
                    {
                        position[] = {-10,-10,0};
                    };
                };
            };
        };
    };
};