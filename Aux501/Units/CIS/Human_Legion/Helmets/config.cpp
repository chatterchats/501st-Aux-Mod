class cfgPatches
{
    class Aux501_Patch_Units_CIS_Humans_Helmets
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Helmets_Phase2",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_CIS_Humans_Helmets_Commander_Cap",
            "Aux501_Units_CIS_Humans_Helmets_Infantry"
        };
    };
};

class cfgWeapons
{
    class H_HelmetB;

    class H_ParadeDressCap_01_CSAT_F;

    class Aux501_Units_Republic_501_Infantry_Helmet_Base: H_HelmetB
    {
        class Iteminfo;
    };

    class Aux501_Units_CIS_Humans_Helmets_Commander_Cap: H_ParadeDressCap_01_CSAT_F
    {
        author = "501st Aux Team";
        displayName = "[CIS] HMN CAP 01 - Commander";
        picture = "\Aux501\Units\CIS\Human_Legion\Helmets\data\UI\CIS_Human_Commander_Cap_UI_ca.paa";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\CIS\Human_Legion\Helmets\data\textures\CIS_Human_Officer_Cap_CO.paa"};
        hiddenSelectionsMaterials[] = {"\Aux501\Units\CIS\Human_Legion\Helmets\data\textures\CIS_Human_Officer_Cap.rvmat"};
    };

    class Aux501_Units_CIS_Humans_Helmets_Infantry: Aux501_Units_Republic_501_Infantry_Helmet_Base
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[CIS] HMN HELM 01";
        model = "\ls_armor_redfor\helmet\jn\trooper\ls_jn_trooper_helmet";
        picture = "\Aux501\Units\CIS\Human_Legion\Helmets\data\UI\CIS_Human_Trooper_Helmet_UI_ca.paa";
        subItems[] = {};
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\CIS\Human_Legion\Helmets\data\textures\CIS_Human_Trooper_Helmet_CO.paa"};
        hiddenSelectionsMaterials[] = {};
        class Iteminfo: ItemInfo
        {
            mass = 10;
            uniformModel = "\ls_armor_redfor\helmet\jn\trooper\ls_jn_trooper_helmet";
            hiddenselections[] = {"camo1"};
            modelsides[] = {0,1,2,3};
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointname = "HitHead";
                    armor = 20;
                    passThrough = 0.1;
                };
            };
        };
    };
};