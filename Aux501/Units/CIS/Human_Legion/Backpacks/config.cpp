class cfgPatches
{
    class Aux501_Patch_Units_CIS_Humans_Backpacks
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_CIS_Humans_Backpack_standard",
            "Aux501_Units_CIS_Humans_Backpack_AT",
            "Aux501_Units_CIS_Humans_Backpack_AA"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501_Infantry_Backpacks_base;

    class Aux501_Units_CIS_Humans_Backpack_standard: Aux501_Units_Republic_501_Infantry_Backpacks_base
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[CIS] HMN Backpack - Standard";
        picture = "\Aux501\Units\CIS\Human_Legion\Backpacks\data\UI\CIS_Human_Trooper_Backpack_UI_ca.paa";
        model = "SFA_Equipment_R\Backpack\Trooper\Trooper_Radio_backpack.p3d";
        hiddenSelectionsTextures[] = {"\Aux501\Units\CIS\Human_Legion\Backpacks\data\Textures\CIS_Human_Trooper_Backpack_CO.paa"};
        maximumLoad = 280;
    };
    class Aux501_Units_CIS_Humans_Backpack_AT: Aux501_Units_CIS_Humans_Backpack_standard
    {
        scope = 1;
        scopearsenal = 1;
        class TransportMagazines
        {
            class _xx_at_mag
            {
                count = 4;
                magazine = "Aux501_Weapons_Mags_e60r_at";
            };
            class _xx_he_mag
            {
                count = 2;
                magazine = "Aux501_Weapons_Ammo_e60r_he";
            };
        };
    };
    class Aux501_Units_CIS_Humans_Backpack_AA: Aux501_Units_CIS_Humans_Backpack_AT
    {
        class TransportMagazines
        {
            class _xx_aa_mag
            {
                count = 3;
                magazine = "Aux501_Weapons_Mags_e60r_aa";
            };
        };
    };
};