class cfgPatches
{
    class Aux501_Patch_Units_CIS_Humans_Uniforms
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_CIS_Humans_Soldier_Uniform",
            "Aux501_Units_CIS_Humans_NCO_Uniform",
            "Aux501_Units_CIS_Humans_Crew_Uniform",
            "Aux501_Units_CIS_Humans_Commander_Uniform"
        };
    };
};

class cfgWeapons
{
    class Uniform_Base;

    class Aux501_Units_Republic_501st_Uniform_Base: Uniform_Base
    {
        class ItemInfo;
    };

    class Aux501_Units_CIS_Humans_Soldier_Uniform: Aux501_Units_Republic_501st_Uniform_Base
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[CIS] HMN ARMR 01 - Grunt";
        picture = "\Aux501\Units\CIS\Human_Legion\Uniforms\data\UI\CIS_Humans_Grunt_Uniform_UI_ca.paa";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_Humans_Soldier_Unit";
            containerClass = "Supply40";
            mass = 50;
            modelSides[] = {6};

            armor = 50;
            armorStructural = 2;
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_Human_Uniforms";
            variant = "standard";
        };
    };
    class Aux501_Units_CIS_Humans_NCO_Uniform: Aux501_Units_CIS_Humans_Soldier_Uniform
    {
        displayName = "[CIS] HMN ARMR 02 - NCO";
        picture = "\Aux501\Units\CIS\Human_Legion\Uniforms\data\UI\CIS_Humans_NCO_Uniform_UI_ca.paa";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_Humans_NCO_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_Human_Uniforms";
            variant = "nco";
        };
    };
    class Aux501_Units_CIS_Humans_Crew_Uniform: Aux501_Units_CIS_Humans_Soldier_Uniform
    {
        displayName = "[CIS] HMN ARMR 03 - Crew";
        picture = "\Aux501\Units\CIS\Human_Legion\Uniforms\data\UI\CIS_Humans_Crew_Uniform_UI_ca.paa";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_Humans_Crew_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_Human_Uniforms";
            variant = "crew";
        };
    };

    class Aux501_Units_CIS_Humans_Commander_Uniform: Aux501_Units_CIS_Humans_Soldier_Uniform
    {
        displayName = "[CIS] HMN ARMR 04 - Commander";
        picture = "\Aux501\Units\CIS\Human_Legion\Uniforms\data\UI\CIS_Humans_Commander_Uniform_UI_ca.paa";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_Humans_Commander_Unit";
            containerClass = "Supply60";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_Human_Uniforms";
            variant = "commander";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        ////Human Legion Uniform////
        class Aux501_ACEX_Gear_CIS_Human_Uniforms
        {
            label = "";
            author = "501st Aux Team";
            options[]=
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[]=
                {
                    "standard",
                    "nco",
                    "crew",
                    "commander"
                };
                class standard
                {
                    label = "Standard";
                };
                class nco
                {
                    label = "NCO";
                };
                class crew
                {
                    label = "Crew";
                };
                class commander
                {
                    label = "Commander";
                };
            };
        };
    };
};