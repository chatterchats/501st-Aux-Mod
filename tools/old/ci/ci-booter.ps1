param(
    [string]$arma,
    [string]$local,
    [string]$output,
    [string]$route
)

$mods = @(
    "@3AS (Beta Test)",
    "@ace",
    "@CBA_A3",
    "@Legion Base - Stable",
    "@Just Like The Simulations - The Great War",
    "@Kobra Mod Pack - Main",
    "@Operation TREBUCHET",
    "@WebKnight Droids",
    "@DBA CIS",
    "@DBA Core",
    "@Black Legion Imperial Assets Mod",
    "@StarForge Armory",
    "@GARC Asset Pack"
)

$noMods = $False
$extensions = @()
$whitelist = @()
$Preload = $False
$initRunners = 4
$noClean = $True
$randomOutput = $False
$noPurge = $True

$params = @{
    "Route"= $route;
    "Arma"= $Arma;
    "Mods" = $Mods;
    "NoMods" = $NoMods;
    "Output" = $output;
    "Local" = $Local;
    "Extensions" = $Extensions;
    "Whitelist" = $Whitelist;
    "Preload" = $Preload;
    "Runners" = $initRunners;
    "Noclean" = $Noclean;
    "RandomOutput" = $randomOutput;
    "NoPurge" = $noPurge;
    "Log"=$True;
    "EnvVarName"="VPD_VIRTUAL_INSTANCE_ID";
    "CreateIfNotCreated"=$True;
}

$createInstancePath = (Resolve-Path "$PSScriptRoot\..\shared\create-instance.ps1").Path
& $createInstancePath @params
