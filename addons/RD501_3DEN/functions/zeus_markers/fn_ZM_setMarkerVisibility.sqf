params ["_opacity"];

if (not (isNil "RD501_3DEN_ZM_ZeusMapMarkerList")) then {
	{
		_x setMarkerAlphaLocal _opacity;
	} forEach RD501_3DEN_ZM_ZeusMapMarkerList;
};