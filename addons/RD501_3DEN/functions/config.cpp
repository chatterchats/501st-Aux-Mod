#include "..\config_macros.hpp"

class CfgPatches
{
	class RD501_patch_3den_functions
	{
		author=DANKAUTHORS;
		requiredAddons[]={};
		requiredVersion=0.1;
		units[]={};
		weapons[]={};
	};
};

class CfgFunctions
{
    class RD501_3DEN
    {
        class zeusMarkers
        {
            file = "RD501_3DEN\functions\zeus_markers";

            class ZM_markerExpression {};
            class ZM_postInitLocal {};
            class ZM_setMarkerVisibility {};
        };

        class startup
        {
            file = "RD501_3DEN\functions\startup";

            class postInitLocal {};
        };
    };
};

class Extended_PostInit_EventHandlers {
    class RD501_3DEN_PostInit {
        clientInit = "_this call RD501_3DEN_fnc_postInitLocal";
    };
};