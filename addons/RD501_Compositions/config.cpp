class CfgPatches 
{
    class Aux501_Compositions_Patch
    {
        units[] = {};
        weapons[] = {};
        requiredVersion = 0.1;
        requiredAddons[] = {"A3_Modules_F", "A3_Modules_F_Curator"};
    };
};

class CfgGroups 
{
    class Empty 
    {
        class Aux501_Compositions_Republic 
        {
            name = "[Aux501] Republic";
            class Aux501_Compositions_Republic_Bases 
            {
                name = "Bases";
                class Aux501_Compositions_Republic_Base_1
                {
                    name="Razor FARP (Large)";
                    side = 8;
                    icon = "\a3\Ui_f\data\Map\Markers\NATO\n_unknown.paa";
                    faction = "Aux501_Compositions_Republic";
                    #include "data\republic\bases\large_farp.hpp"
                };
            };
            class Aux501_Compositions_Republic_Ships 
            {
                name = "Ships";
                /*class Aux501_Compositions_Republic_Ship_0 {
                    name="Example Ship";
                    side = 8;
                    icon = "\a3\Ui_f\data\Map\Markers\NATO\n_unknown.paa";
                    #include "data\republic\ships\example_ship.hpp"
                };*/
            };
            class Aux501_Compositions_Republic_FOBs
            {
                name = "Forward Operating Bases";
                class Aux501_Compositions_Republic_FOB_1 
                {
                    name="FOB Aurek";
                    side = 8;
                    icon = "\a3\Ui_f\data\Map\Markers\NATO\n_unknown.paa";
                    #include "data\republic\FOBs\el_fob_aurek.hpp"
                };
            };
            class Aux501_Compositions_Republic_OPs
            {
                name = "Outposts";
                class Aux501_Compositions_Republic_OP_0 
                {
                    name="OP Turbotank";
                    side = 8;
                    icon = "\a3\Ui_f\data\Map\Markers\NATO\n_unknown.paa";
                    #include "data\republic\Outposts\an_outpost_turbotank.hpp"
                    scope = 2;
                    scopecurator = 2;
                };
                class Aux501_Compositions_Republic_OP_1: Aux501_Compositions_Republic_OP_0
                {
                    name="OP Aurek";
                    #include "data\republic\Outposts\sc_outpost_aurek.hpp"
                };
                class Aux501_Compositions_Republic_OP_2: Aux501_Compositions_Republic_OP_0
                {
                    name = "Recon Outpost 01";
                    #include "data\republic\Outposts\Recon_Outpost_01.hpp"
                };
                class Aux501_Compositions_Republic_OP_FARP_01: Aux501_Compositions_Republic_OP_0
                {
                    name = "FARP";
                    #include "data\republic\Outposts\FARP_01.hpp"
                };
            };
            class Aux501_Compositions_Republic_HQs
            {
                name = "HQs";
            };
            class Aux501_Compositions_Republic_Checkpoints
            {
                name = "Checkpoints";
                class Aux501_Compositions_Republic_Checkpoints
                {
                    name = "Checkpoint 1";
                    side = 8;
                    icon = "\a3\Ui_f\data\Map\Markers\NATO\n_unknown.paa";
                    #include "data\republic\Checkpoints\Checkpoint_01.hpp"
                    scope = 2;
                    scopecurator = 2;
                };
            };
        };
        class Aux501_Compositions_CIS 
        {
            name = "[Aux501] Confederacy";
            class Aux501_Compositions_CIS_Bases 
            {
                name = "Bases";
                class Aux501_Compositions_CIS_Base_1 
                {
                    name="Compound Aurek";
                    side = 8;
                    icon = "\a3\Ui_f\data\Map\Markers\NATO\n_unknown.paa";
                    #include "data\cis\bases\base_compound_aurek.hpp"
                };
                class Aux501_Compositions_CIS_Base_2: Aux501_Compositions_CIS_Base_1
                {
                    name="Internment Camp (Large)";
                    #include "data\cis\bases\base_interment_camp_large.hpp"
                };
                class Aux501_Compositions_CIS_Base_3: Aux501_Compositions_CIS_Base_1
                {
                    name="Armor Repair Depot";
                    #include "data\cis\bases\base_armor_repair_depot.hpp"
                };
            };
            class Aux501_Compositions_CIS_Ships 
            {
                name = "Ships";
                class Aux501_Compositions_CIS_Ship_0 
                {
                    name="Example Ship";
                    side = 8;
                    icon = "\a3\Ui_f\data\Map\Markers\NATO\n_unknown.paa";
                    #include "data\cis\ships\example_ship.hpp"
                };
            };
            class Aux501_Compositions_CIS_Checkpoints
            {
                name = "Checkpoints";
                class Aux501_Compositions_CIS_Checkpoint_0 
                {
                    name="Checkpoint (Medium)";
                    side = 8;
                    icon = "\a3\Ui_f\data\Map\Markers\NATO\n_unknown.paa";
                    #include "data\cis\Checkpoints\an_checkpoint_medium.hpp"
                };
            };
            class Aux501_Compositions_CIS_Ouposts
            {
                name = "Outposts";
                class Aux501_Compositions_CIS_Outpost_0 
                {
                    name = "OP Aurek";
                    side = 8;
                    icon = "\a3\Ui_f\data\Map\Markers\NATO\n_unknown.paa";
                    #include "data\cis\Outposts\an_outpost_aurek.hpp"
                };
                class Aux501_Compositions_CIS_Outpost_1
                {
                    name = "OP Besh";
                    side = 8;
                    icon = "\a3\Ui_f\data\Map\Markers\NATO\n_unknown.paa";
                    #include "data\cis\Outposts\an_outpost_besh.hpp"
                };
                class Aux501_Compositions_CIS_Outpost_2
                {
                    name = "OP Cresh";
                    side = 8;
                    icon = "\a3\Ui_f\data\Map\Markers\NATO\n_unknown.paa";
                    #include "data\cis\Outposts\an_outpost_cresh.hpp"
                };
            };
            class Aux501_Compositions_CIS_FOBs
            {
                name = "Forward Operating Bases";
                class Aux501_Compositions_CIS_FOB_1 
                {
                    scope = 2;
                    scopeCurator = 2;
                    name = "FOB Aurek (Armor)";
                    side = 8;
                    faction = "Aux501_Compositions_CIS";
                    icon = "\a3\Ui_f\data\Map\Markers\NATO\n_unknown.paa";
                    #include "data\cis\FOBs\an_fob_aurek_armor.hpp"
                };
                class Aux501_Compositions_CIS_FOB_2: Aux501_Compositions_CIS_FOB_1
                {
                    name = "FOB Aurek";
                    #include "data\cis\FOBs\an_fob_aurek.hpp"
                };
                class Aux501_Compositions_CIS_FOB_3: Aux501_Compositions_CIS_FOB_1
                {
                    name = "FOB Besh (Armor)";
                    #include "data\cis\FOBs\an_fob_besh_armor.hpp"
                };
            };
            class Aux501_Compositions_CIS_HQs
            {
                name = "Headquarters";
                class Aux501_Compositions_CIS_HQ_0 
                {
                    name = "Aurek HQ";
                    side = 8;
                    icon = "\a3\Ui_f\data\Map\Markers\NATO\n_unknown.paa";
                    #include "data\cis\HQs\an_hq_aurek.hpp"
                };
                class Aux501_Compositions_CIS_HQ_1
                {
                    name= "Aurek HQ (Prison)";
                    side = 8;
                    icon = "\a3\Ui_f\data\Map\Markers\NATO\n_unknown.paa";
                    #include "data\cis\HQs\an_hq_aurek_prison.hpp"
                };
            };
            class Aux501_Compositions_CIS_Fortifications
            {
                name = "Fortifications";
                class Aux501_Compositions_CIS_Bunkers_1
                {
                    name="Small Bunker";
                    side = 8;
                    icon = "\a3\Ui_f\data\Map\Markers\NATO\n_unknown.paa";
                    #include "data\cis\installations\small_bunker.hpp"
                };
                class Aux501_Compositions_CIS_Bunkers_2: Aux501_Compositions_CIS_Bunkers_1
                {
                    name= "Supply Bunker (Small)";
                    #include "data\cis\installations\installation_supply_bunker_small.hpp"
                };
                class Aux501_Compositions_CIS_Dispenser_1: Aux501_Compositions_CIS_Bunkers_1
                {
                    name="Triple Dispenser";
                    #include "data\cis\installations\installation_fortified_triple_dispenser_large.hpp"
                };
                class Aux501_Compositions_CIS_Dispenser_2: Aux501_Compositions_CIS_Bunkers_1
                {
                    name="Double Dispenser";
                    #include "data\cis\installations\installation_fortified_double_dispenser_medium.hpp"
                };
                class Aux501_Compositions_CIS_Dispenser_3: Aux501_Compositions_CIS_Bunkers_1
                {
                    name="Single Dispenser (Medium)";
                    #include "data\cis\installations\installation_fortified_dispenser_medium.hpp"
                };
                class Aux501_Compositions_CIS_Dispenser_4: Aux501_Compositions_CIS_Bunkers_1
                {
                    name="Single Dispenser (Small)";
                    #include "data\cis\installations\installation_fortified_dispenser_small.hpp"
                };
                class Aux501_Compositions_CIS_Dispenser_5: Aux501_Compositions_CIS_Bunkers_1
                {
                    name="Fortified Bunker";
                    #include "data\cis\installations\installation_fortified_bunker_medium.hpp"
                };
            };
        };
        class Aux501_Compositions_Unaligned
        {
            name = "[Aux501] Unaligned";
            class Aux501_Compositions_Unaligned_Fortifications
            {
                name = "Fortifications";
                class Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Large Trench 1";
                    side = 8;
                    icon = "\a3\Ui_f\data\Map\Markers\NATO\n_unknown.paa";
                    #include "data\Unaligned\an_trench_large.hpp"
                };
                class Aux501_Compositions_CIS_Trenches_2: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Large Trench 2";
                    #include "data\Unaligned\el_trench_large_01.hpp"
                };
                class Aux501_Compositions_CIS_Trenches_3: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Large Trench 3";
                    #include "data\Unaligned\el_trench_large_02.hpp"
                };
                class Aux501_Compositions_CIS_Trenches_4: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Medium Trench 1";
                    #include "data\Unaligned\an_trench_medium.hpp"
                };
                class Aux501_Compositions_CIS_Trenches_5: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Medium Trench 2";
                    #include "data\Unaligned\el_trench_medium_01.hpp"
                };
                class Aux501_Compositions_CIS_Trenches_6: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Medium Trench 3";
                    #include "data\Unaligned\el_trench_medium_02.hpp"
                };
                class Aux501_Compositions_CIS_Trenches_7: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Medium Trench 4";
                    #include "data\Unaligned\el_trench_medium_03.hpp"
                };
                class Aux501_Compositions_CIS_Trenches_8: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Small Trench 1";
                    #include "data\Unaligned\an_trench_small.hpp"
                };
                class Aux501_Compositions_CIS_Trenches_9: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Small Trench 2";
                    #include "data\Unaligned\el_trench_small_01.hpp"
                };
                class Aux501_Compositions_CIS_Trenches_10: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Small Trench 3";
                    #include "data\Unaligned\el_trench_small_02.hpp"
                };
                class Aux501_Compositions_CIS_Trenches_11: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Tiny Trench";
                    #include "data\Unaligned\an_trench_tiny.hpp"
                };
                class Aux501_Compositions_AA_Position_01: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "AA Position 01";
                    #include "data\Unaligned\AA_Position_01.hpp"
                };
                class Aux501_Compositions_AA_Position_02: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "AA Position 02";
                    #include "data\Unaligned\AA_Position_02.hpp"
                };
                class Aux501_Compositions_AA_Radar_Position: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "AA Radar Position";
                    #include "data\Unaligned\AA_Radar_Position.hpp"
                };
                class Aux501_Compositions_Armor_Position_01: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Armor Position 01";
                    #include "data\Unaligned\Armor_Position_01.hpp"
                };
                class Aux501_Compositions_Armor_Position_02: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Armor Position 02";
                    #include "data\Unaligned\Armor_Position_02.hpp"
                };
                class Aux501_Compositions_Armor_Position_03: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Armor Position 03";
                    #include "data\Unaligned\Armor_Position_03.hpp"
                };
                class Aux501_Compositions_Armor_Position_04: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Armor Position 04";
                    #include "data\Unaligned\Armor_Position_04.hpp"
                };
                class Aux501_Compositions_Armor_Position_05: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Armor Position 05";
                    #include "data\Unaligned\Armor_Position_05.hpp"
                };
                class Aux501_Compositions_Bunker_Line_01: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Bunker Line 01";
                    #include "data\Unaligned\Bunker_Line_01.hpp"
                };
                class Aux501_Compositions_Bunker_Line_02: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Bunker Line 02";
                    #include "data\Unaligned\Bunker_Line_02.hpp"
                };
                class Aux501_Compositions_Bunker_Line_03: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Bunker Line 03";
                    #include "data\Unaligned\Bunker_Line_03.hpp"
                };
                class Aux501_Compositions_Bunker_Position_01: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Bunker Position 01";
                    #include "data\Unaligned\Bunker_Post_01.hpp"
                };
                class Aux501_Compositions_Bunker_Position_02: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Bunker Position 02";
                    #include "data\Unaligned\Bunker_Post_02.hpp"
                };
                class Aux501_Compositions_Bunker_Position_03: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Bunker Position 03";
                    #include "data\Unaligned\Bunker_Post_03.hpp"
                };
                class Aux501_Compositions_Mortar_Pit_01: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Mortar Pit 01";
                    #include "data\Unaligned\Mortar_Pit_01.hpp"
                };
                class Aux501_Compositions_Mortar_Pit_02: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Mortar Pit 02";
                    #include "data\Unaligned\Mortar_Pit_02.hpp"
                };
                class Aux501_Compositions_Mortar_Pit_03: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Mortar Pit 03";
                    #include "data\Unaligned\Mortar_Pit_03.hpp"
                };
                class Aux501_Compositions_Mortar_Pit_04: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Mortar Pit 04";
                    #include "data\Unaligned\Mortar_Pit_04.hpp"
                };
                class Aux501_Compositions_Roadblock_01: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Roadblock 01";
                    #include "data\Unaligned\Roadblock_01.hpp"
                };
                class Aux501_Compositions_Roadblock_02: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Roadblock 02";
                    #include "data\Unaligned\Roadblock_02.hpp"
                };
                class Aux501_Compositions_Roadblock_03: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Roadblock 03";
                    #include "data\Unaligned\Roadblock_03.hpp"
                };
                class Aux501_Compositions_Roadblock_04: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Roadblock 04";
                    #include "data\Unaligned\Roadblock_04.hpp"
                };
                class Aux501_Compositions_Artillery_Battery_01: Aux501_Compositions_CIS_Trenches_1
                {
                    name= "Artillery Battery";
                    #include "data\Unaligned\Artillery_Battery_01.hpp"
                };
            };
            class Aux501_Compositions_Unaligned_Fortifications_Mud
            {
                name = "Fortifications (Mud)";
                class Aux501_Compositions_CIS_Trenches_Mud_1
                {
                    name= "Large Trench";
                    side = 8;
                    icon = "\a3\Ui_f\data\Map\Markers\NATO\n_unknown.paa";
                    #include "data\Unaligned\an_trench_large_mud.hpp"
                };
                class Aux501_Compositions_CIS_Trenches_Mud_2: Aux501_Compositions_CIS_Trenches_Mud_1
                {
                    name= "Medium Trench";
                    #include "data\Unaligned\an_trench_medium_mud.hpp"
                };
                class Aux501_Compositions_CIS_Trenches_Mud_3: Aux501_Compositions_CIS_Trenches_Mud_1
                {
                    name= "Small Trench";
                    #include "data\Unaligned\an_trench_small_mud.hpp"
                };
                class Aux501_Compositions_CIS_Trenches_Mud_4: Aux501_Compositions_CIS_Trenches_Mud_1
                {
                    name= "Tiny Trench";
                    #include "data\Unaligned\an_trench_tiny_mud.hpp"
                };
            };
            class Aux501_Compositions_Unaligned_Fortifications_Snow
            {
                name = "Fortifications (Snow)";
                class Aux501_Compositions_CIS_Trenches_Snow_1
                {
                    name= "Large Trench";
                    side = 8;
                    icon = "\a3\Ui_f\data\Map\Markers\NATO\n_unknown.paa";
                    #include "data\Unaligned\an_trench_large_snow.hpp"
                };
                class Aux501_Compositions_CIS_Trenches_Snow_2: Aux501_Compositions_CIS_Trenches_Snow_1
                {
                    name= "Medium Trench";
                    #include "data\Unaligned\an_trench_medium_snow.hpp"
                };
                class Aux501_Compositions_CIS_Trenches_Snow_3: Aux501_Compositions_CIS_Trenches_Snow_1
                {
                    name= "Small Trench";
                    #include "data\Unaligned\an_trench_small_snow.hpp"
                };
                class Aux501_Compositions_CIS_Trenches_Snow_4: Aux501_Compositions_CIS_Trenches_Snow_1
                {
                    name= "Tiny Trench";
                    #include "data\Unaligned\an_trench_tiny_snow.hpp"
                };
            };
            class Aux501_Compositions_Unaligned_Checkpoints
            {
                name = "Checkpoints";
                class Aux501_Compositions_CIS_Checkpoints_1
                {
                    name= "Large Checkpoint";
                    side = 8;
                    icon = "\a3\Ui_f\data\Map\Markers\NATO\n_unknown.paa";
                    #include "data\Unaligned\an_checkpoint_large.hpp"
                };
                class Aux501_Compositions_CIS_Checkpoints_2: Aux501_Compositions_CIS_Checkpoints_1
                {
                    name= "Medium Checkpoint 1";
                    #include "data\Unaligned\an_checkpoint_medium_1.hpp"
                };
                class Aux501_Compositions_CIS_Checkpoints_3: Aux501_Compositions_CIS_Checkpoints_1
                {
                    name= "Medium Checkpoint 2";
                    #include "data\Unaligned\an_checkpoint_medium_2.hpp"
                };
                class Aux501_Compositions_CIS_Checkpoints_4: Aux501_Compositions_CIS_Checkpoints_1
                {
                    name= "Small Checkpoint 1";
                    #include "data\Unaligned\an_checkpoint_small_1.hpp"
                };
                class Aux501_Compositions_CIS_Checkpoints_5: Aux501_Compositions_CIS_Checkpoints_1
                {
                    name= "Small Checkpoint 2";
                    #include "data\Unaligned\an_checkpoint_small_1.hpp"
                };
            };
            class Aux501_Compositions_Unaligned_Outposts
            {
                name = "Ouposts";
                class Aux501_Compositions_Unaligned_Outposts_1
                {
                    name= "Large Camp";
                    side = 8;
                    icon = "\a3\Ui_f\data\Map\Markers\NATO\n_unknown.paa";
                    #include "data\Unaligned\Outposts\an_camp_large.hpp"
                };
                class Aux501_Compositions_Unaligned_Outposts_2: Aux501_Compositions_Unaligned_Outposts_1
                {
                    name= "Medium Camp";
                    #include "data\Unaligned\Outposts\an_camp_medium.hpp"
                };
                class Aux501_Compositions_Unaligned_Outposts_3: Aux501_Compositions_Unaligned_Outposts_1
                {
                    name= "Small Camp";
                    #include "data\Unaligned\Outposts\an_camp_small.hpp"
                };
                class Aux501_Compositions_Unaligned_Outposts_4: Aux501_Compositions_Unaligned_Outposts_1
                {
                    name= "Motor Pool Aurek";
                    #include "data\Unaligned\Outposts\an_motorpool_aurek.hpp"
                };
                class Aux501_Compositions_Unaligned_Prison_Camp_01: Aux501_Compositions_Unaligned_Outposts_1
                {
                    name= "Prison Camp";
                    #include "data\Unaligned\Prison_Camp_01.hpp"
                };
                class Aux501_Compositions_Unaligned_Prison_Post_01: Aux501_Compositions_Unaligned_Outposts_1
                {
                    name= "Prison Post 01";
                    #include "data\Unaligned\Prison_Post_01.hpp"
                };
                class Aux501_Compositions_Unaligned_Prison_Post_02: Aux501_Compositions_Unaligned_Outposts_1
                {
                    name= "Prison Post 02";
                    #include "data\Unaligned\Prison_Post_02.hpp"
                };
                class Aux501_Compositions_Unaligned_Prison_Post_03: Aux501_Compositions_Unaligned_Outposts_1
                {
                    name= "Prison Post 03";
                    #include "data\Unaligned\Prison_Post_03.hpp"
                };
            };
        };
    };
};