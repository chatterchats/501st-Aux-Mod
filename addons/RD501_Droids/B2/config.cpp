#define unit_addon B2
class CfgPatches
{
    class rd501_patch_name_B2_units
    {
        requiredAddons[]=
        {
            "RD501_patch_units"
        };
        requiredVersion=0.1;
        units[]=
        {
            "RD501_opfor_unit_B2_droid_Standard",
            "RD501_opfor_unit_B2_droid_Super",
            "RD501_opfor_unit_B2_droid_gren",
            "RD501_opfor_unit_B2_droid_rocket"
        };
        weapons[]=
        {
            
        };
    };
};

class CfgWeapons
{	
    class DefaultEventhandlers;
    class U_I_CombatUniform;
    class UniformItem;
    class RD501_opfor_uniform_B2_armor: U_I_CombatUniform
    {
        scope=2;
        displayName="[CIS] B2 Chassis";
        genericNames = "3ASDroidNames";
        editorPreview = "";
        picture = "\RD501_Droids\B2\B2_armor_ui.paa";
        model="\lsd_armor_redfor\uniform\cis\b2\lsd_cis_b2_uniform.p3d";
        class ItemInfo: UniformItem
        {
            uniformModel="-";
            uniformClass="RD501_opfor_unit_B2_droid_Standard";
            containerClass="Supply100";
            mass=80;
        };
        hiddenSelections[]=
        {
            "camo_arms",
            "legs",
            "torso"
        };
        hiddenSelectionsTextures[]=
        {
            "RD501_Droids\data\B2\standard\Reskinb2_Torso.paa",
            "RD501_Droids\data\B2\standard\Reskinb2_Legs.paa",
            "RD501_Droids\data\B2\standard\Reskinb2_Arms.paa"
        };
        hiddenSelectionsMaterials[]=
        {
            "RD501_Droids\data\B2\standard\arms.rvmat",
            "RD501_Droids\data\B2\standard\legs.rvmat",
            "RD501_Droids\data\B2\standard\torso.rvmat"            
        };
        JLTS_isDroid = 1; 
        JLTS_hasEMPProtection = 0; 
        JLTS_deathSounds = "DeathDroid";
        class EventHandlers : DefaultEventhandlers {};
    };
    class RD501_opfor_uniform_B2_heavy_armor:RD501_opfor_uniform_B2_Armor
    {
        displayName="[CIS] B2 Heavy Chassis";
        picture = "\RD501_Droids\B2\B2_armor_ui.paa";
        armor=100;
        class ItemInfo: UniformItem
        {
            uniformModel="-";
            uniformClass="RD501_opfor_unit_B2_droid_Super";
            containerClass="Supply100";
            mass=80;
        };
        hiddenSelections[]=
        {
            "camo_arms",
            "legs",
            "torso"
        };
        hiddenSelectionsTextures[]=
        {
            "RD501_Droids\data\B2\heavy\super_b2_torso.paa",
            "RD501_Droids\data\B2\heavy\super_b2_legs.paa",
            "RD501_Droids\data\B2\heavy\super_b2_arms.paa"
        };
        hiddenSelectionsMaterials[]=
        {
            "RD501_Droids\data\B2\standard\arms.rvmat",
            "RD501_Droids\data\B2\standard\legs.rvmat",
            "RD501_Droids\data\B2\standard\torso.rvmat"            
        };
    };
    class RD501_opfor_uniform_B2_gren_armor:RD501_opfor_uniform_B2_armor
    {
        scope=2;
        displayName="[CIS] B2 Gren Chassis";
        picture = "\RD501_Droids\B2\B2_armor_ui.paa";
        model="\lsd_armor_redfor\uniform\cis\b2\lsd_cis_b2_uniform.p3d";
        class ItemInfo: UniformItem
        {
            uniformModel="-";
            uniformClass="RD501_opfor_unit_B2_droid_gren";
            containerClass="Supply100";
            mass=80;
        };
        hiddenSelections[]=
        {
            "camo_arms",
            "legs",
            "torso"
        };
        hiddenSelectionsTextures[]=
        {
            "RD501_Droids\data\B2\standard\Reskinb2_Arms.paa",
            "RD501_Droids\data\B2\standard\Reskinb2_Legs.paa",
            "RD501_Droids\data\B2\gren\b2gren_Torso.paa"
        };
        hiddenSelectionsMaterials[]=
        {
            "RD501_Droids\data\B2\standard\arms.rvmat",
            "RD501_Droids\data\B2\standard\legs.rvmat",
            "RD501_Droids\data\B2\standard\torso.rvmat"            
        };
    };
    class RD501_opfor_uniform_B2_rocket_armor:RD501_opfor_uniform_B2_Armor
    {
        displayName="[CIS] B2 Rocket Chassis";
        picture = "\RD501_Droids\B2\B2_armor_ui.paa";
        armor=100;
        class ItemInfo: UniformItem
        {
            uniformModel="-";
            uniformClass="RD501_opfor_unit_B2_droid_rocket";
            containerClass="Supply200";
            mass=80;
        };
        hiddenSelections[]=
        {
            "camo_arms",
            "legs",
            "torso"
        };
        hiddenSelectionsTextures[]=
        {
            "RD501_Droids\data\B2\heavy\super_b2_torso.paa",
            "RD501_Droids\data\B2\rocket\rocket_b2_legs.paa",
            "RD501_Droids\data\B2\rocket\rocket_b2_arms.paa"
        };
        hiddenSelectionsMaterials[]=
        {
            "RD501_Droids\data\B2\standard\arms.rvmat",
            "RD501_Droids\data\B2\standard\legs.rvmat",
            "RD501_Droids\data\B2\standard\torso.rvmat"            
        };
    };
};
class CfgVehicles
{
    class lsd_cis_b2_standard;

    class RD501_opfor_unit_B2_droid_Standard: lsd_cis_b2_standard
    {
        JLTS_isDroid = 1;
		JLTS_hasEMPProtection = 0;
        faction="RD501_CIS_Faction";
        editorSubcategory="RD501_Editor_Category_B2";
        picture = "\RD501_Droids\B2\B2_armor_ui.paa";
        editorPreview = "";
        identityTypes[] = {"lsd_voice_b1Droid"};
        displayname = "B2 Super Battle Droid";
        scope=2;
        uniformClass="RD501_opfor_uniform_B2_armor";
        impactEffectsBlood = "ImpactMetal";
        impactEffectsNoBlood = "ImpactPlastic";
        canBleed = 0;
        class HitPoints
        {
            class HitFace
            {
                armor = 50;
                material = -1;
                name = "face_hub";
                passThrough = 0.8;
                radius = 0.08;
                explosionShielding = 0.1;
                minimalHit = 0.01;
            };
            class HitNeck: HitFace
            {
                armor = 50;
                material = -1;
                name = "neck";
                passThrough = 0.8;
                radius = 0.1;
                explosionShielding = 0.5;
                minimalHit = 0.01;
            };
            class HitHead: HitNeck
            {
                armor = 20;
                material = -1;
                name = "head";
                passThrough = 0.8;
                radius = 0.2;
                explosionShielding = 0.5;
                minimalHit = 0.01;
                depends = "HitFace max HitNeck";
            };
            class HitPelvis: HitHead
            {
                armor = 30;
                material = -1;
                name = "pelvis";
                passThrough = 0.8;
                radius = 0.24;
                explosionShielding = 1;
                visual = "injury_body";
                minimalHit = 0.01;
                depends = "0";
            };
            class HitAbdomen: HitPelvis
            {
                armor = 40;
                material = -1;
                name = "spine1";
                passThrough = 0.8;
                radius = 0.2;
                explosionShielding = 1;
                visual = "injury_body";
                minimalHit = 0.01;
            };
            class HitDiaphragm: HitAbdomen
            {
                armor = 40;
                material = -1;
                name = "spine2";
                passThrough = 0.8;
                radius = 0.3;
                explosionShielding = 6;
                visual = "injury_body";
                minimalHit = 0.01;
            };
            class HitChest: HitDiaphragm
            {
                armor = 50;
                material = -1;
                name = "spine3";
                passThrough = 0.8;
                radius = 0.5;
                explosionShielding = 6;
                visual = "injury_body";
                minimalHit = 0.01;
            };
            class HitSensor: HitChest
            {
                armor = 10;
                radius = 0.1;
                name = "sensor_hit";
            };
            class HitBody: HitChest
            {
                armor = 60;
                material = -1;
                name = "body";
                passThrough = 1;
                explosionShielding = 6;
                visual = "injury_body";
                minimalHit = 0.01;
                depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
            };
            class HitArms: HitBody
            {
                armor = 10;
                material = -1;
                name = "arms";
                passThrough = 1;
                radius = 0.1;
                explosionShielding = 1;
                visual = "injury_hands";
                minimalHit = 0.01;
                depends = "0";
            };
            class HitHands: HitArms
            {
                armor = 100;
                material = -1;
                name = "hands";
                passThrough = 1;
                radius = 0.1;
                explosionShielding = 1;
                visual = "injury_hands";
                minimalHit = 0.01;
                depends = "HitArms";
            };
            class HitLegs: HitHands
            {
                armor = 100;
                material = -1;
                name = "legs";
                passThrough = 1;
                radius = 0.14;
                explosionShielding = 1;
                visual = "injury_legs";
                minimalHit = 0.01;
                depends = "0";
            };
            class Incapacitated: HitLegs
            {
                armor = 1000;
                material = -1;
                name = "body";
                passThrough = 1;
                radius = 0;
                explosionShielding = 1;
                visual = "";
                minimalHit = 0;
                depends = "(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
            };
            class HitLeftArm
            {
                armor = 20;
                material = -1;
                name = "hand_l";
                passThrough = 1;
                radius = 0.08;
                explosionShielding = 1;
                visual = "injury_hands";
                minimalHit = 0.01;
            };
            class HitRightArm: HitLeftArm
            {
                name = "hand_r";
                armor = 20;
            };
            class HitLeftLeg
            {
                armor = 20;
                material = -1;
                name = "leg_l";
                passThrough = 1;
                radius = 0.1;
                explosionShielding = 1;
                visual = "injury_legs";
                minimalHit = 0.01;
            };
            class HitRightLeg: HitLeftLeg
            {
                name = "leg_r";
            };
        };
        armor=200;
        armorStructural=1.1;
        explosionShielding=1;
        class EventHandlers;
        weapons[]=
        {
            "Aux501_Weaps_B2_blaster",
            "Put"
        };
        magazines[]=
        {
            "Aux501_Weapons_Mags_B2_blaster60",
            "Aux501_Weapons_Mags_B2_blaster60",
            "Aux501_Weapons_Mags_B2_blaster60",
            "Aux501_Weapons_Mags_B2_blaster60",
            "Aux501_Weapons_Mags_B2_rocket3"
        };
        respawnWeapons[]=
        {
            "Aux501_Weaps_B2_blaster",
            "Put"
        };
        respawnMagazines[]=
        {
            "Aux501_Weapons_Mags_B2_blaster60",
            "Aux501_Weapons_Mags_B2_blaster60",
            "Aux501_Weapons_Mags_B2_blaster60",
            "Aux501_Weapons_Mags_B2_blaster60",
            "Aux501_Weapons_Mags_B2_rocket3"
        };
        linkedItems[]=
        {
            "ItemMap",
            "ItemGPS",
            "ItemCompass",
            "ItemWatch",
            "JLTS_droid_comlink",
            "JLTS_NVG_droid_chip_2"
        };
        items[] = 
        {
            
        };
        respawnItems[] = 
        {
            
        };
        model="\lsd_armor_redfor\uniform\cis\b2\lsd_cis_b2_uniform.p3d";
        hiddenSelections[] = {"camo_arms","legs","torso"};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Droids\data\B2\standard\arms.rvmat",
            "RD501_Droids\data\B2\standard\legs.rvmat",
            "RD501_Droids\data\B2\standard\torso.rvmat"            
        };
        hiddenSelectionsTextures[]=
        {
            "RD501_Droids\data\B2\standard\Reskinb2_Arms.paa",
            "RD501_Droids\data\B2\standard\Reskinb2_Legs.paa",
            "RD501_Droids\data\B2\standard\Reskinb2_Torso.paa"            
        };
    };
    class RD501_opfor_unit_B2_droid_Super: RD501_opfor_unit_B2_droid_Standard
    {
        displayname = "B2-H Heavy Super Battledroid";
        picture = "\RD501_Droids\B2\B2_armor_ui.paa";
        uniformClass= "RD501_opfor_uniform_B2_heavy_armor";
        armor = 600;
        hiddenSelections[] = {"camo_arms","legs","torso"};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Droids\data\B2\standard\arms.rvmat",
            "RD501_Droids\data\B2\standard\legs.rvmat",
            "RD501_Droids\data\B2\standard\torso.rvmat"            
        };
        hiddenSelectionsTextures[]=
        {
            "RD501_Droids\data\B2\heavy\super_b2_torso.paa",
            "RD501_Droids\data\B2\heavy\super_b2_legs.paa",
            "RD501_Droids\data\B2\heavy\super_b2_arms.paa"
        };
    };
    class RD501_opfor_unit_B2_droid_gren: RD501_opfor_unit_B2_droid_Standard
    {
        faction="RD501_CIS_Faction";
        editorSubcategory="RD501_Editor_Category_B2";
        picture = "\RD501_Droids\B2\B2_armor_ui.paa";
        identityTypes[] = {"lsd_voice_b1Droid"};
        displayname = "B2-G Super Grenadier Droid";
        scope=2;
        uniformClass="RD501_opfor_B2_gren_Armor";
        impactEffectsBlood = "ImpactMetal";
        impactEffectsNoBlood = "ImpactPlastic";
        canBleed = 0;
        class HitPoints
        {
            class HitFace
            {
                armor = 50;
                material = -1;
                name = "face_hub";
                passThrough = 0.8;
                radius = 0.08;
                explosionShielding = 0.1;
                minimalHit = 0.01;
            };
            class HitNeck: HitFace
            {
                armor = 50;
                material = -1;
                name = "neck";
                passThrough = 0.8;
                radius = 0.1;
                explosionShielding = 0.5;
                minimalHit = 0.01;
            };
            class HitHead: HitNeck
            {
                armor = 20;
                material = -1;
                name = "head";
                passThrough = 0.8;
                radius = 0.2;
                explosionShielding = 0.5;
                minimalHit = 0.01;
                depends = "HitFace max HitNeck";
            };
            class HitPelvis: HitHead
            {
                armor = 30;
                material = -1;
                name = "pelvis";
                passThrough = 0.8;
                radius = 0.24;
                explosionShielding = 1;
                visual = "injury_body";
                minimalHit = 0.01;
                depends = "0";
            };
            class HitAbdomen: HitPelvis
            {
                armor = 40;
                material = -1;
                name = "spine1";
                passThrough = 0.8;
                radius = 0.2;
                explosionShielding = 1;
                visual = "injury_body";
                minimalHit = 0.01;
            };
            class HitDiaphragm: HitAbdomen
            {
                armor = 40;
                material = -1;
                name = "spine2";
                passThrough = 0.8;
                radius = 0.3;
                explosionShielding = 6;
                visual = "injury_body";
                minimalHit = 0.01;
            };
            class HitChest: HitDiaphragm
            {
                armor = 50;
                material = -1;
                name = "spine3";
                passThrough = 0.8;
                radius = 0.5;
                explosionShielding = 6;
                visual = "injury_body";
                minimalHit = 0.01;
            };
            class HitSensor: HitChest
            {
                armor = 10;
                radius = 0.1;
                name = "sensor_hit";
            };
            class HitBody: HitChest
            {
                armor = 60;
                material = -1;
                name = "body";
                passThrough = 1;
                explosionShielding = 6;
                visual = "injury_body";
                minimalHit = 0.01;
                depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
            };
            class HitArms: HitBody
            {
                armor = 10;
                material = -1;
                name = "arms";
                passThrough = 1;
                radius = 0.1;
                explosionShielding = 1;
                visual = "injury_hands";
                minimalHit = 0.01;
                depends = "0";
            };
            class HitHands: HitArms
            {
                armor = 100;
                material = -1;
                name = "hands";
                passThrough = 1;
                radius = 0.1;
                explosionShielding = 1;
                visual = "injury_hands";
                minimalHit = 0.01;
                depends = "HitArms";
            };
            class HitLegs: HitHands
            {
                armor = 100;
                material = -1;
                name = "legs";
                passThrough = 1;
                radius = 0.14;
                explosionShielding = 1;
                visual = "injury_legs";
                minimalHit = 0.01;
                depends = "0";
            };
            class Incapacitated: HitLegs
            {
                armor = 1000;
                material = -1;
                name = "body";
                passThrough = 1;
                radius = 0;
                explosionShielding = 1;
                visual = "";
                minimalHit = 0;
                depends = "(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
            };
            class HitLeftArm
            {
                armor = 20;
                material = -1;
                name = "hand_l";
                passThrough = 1;
                radius = 0.08;
                explosionShielding = 1;
                visual = "injury_hands";
                minimalHit = 0.01;
            };
            class HitRightArm: HitLeftArm
            {
                name = "hand_r";
                armor = 20;
            };
            class HitLeftLeg
            {
                armor = 20;
                material = -1;
                name = "leg_l";
                passThrough = 1;
                radius = 0.1;
                explosionShielding = 1;
                visual = "injury_legs";
                minimalHit = 0.01;
            };
            class HitRightLeg: HitLeftLeg
            {
                name = "leg_r";
            };
        };
        armorStructural=1.1;
        explosionShielding=0.40000001;
        class EventHandlers;
        weapons[]=
        {
            "Aux501_Weaps_B2_GL",
            "Put"
        };
        magazines[]=
        {
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3"
        };
        respawnWeapons[]=
        {
            "Aux501_Weaps_B2_GL",
            "Put"
        };
        respawnMagazines[]=
        {
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3"
        };
        linkedItems[]=
        {
            "ItemMap",
            "ItemCompass",
            "ItemWatch",
            "JLTS_droid_comlink",
            "JLTS_NVG_droid_chip_2"
        };
        model="\lsd_armor_redfor\uniform\cis\b2\lsd_cis_b2_uniform.p3d";
        hiddenSelections[] = {"camo_arms","legs","torso"};
        hiddenSelectionsMaterials[]=
        {
            "RD501_Droids\data\B2\standard\arms.rvmat",
            "RD501_Droids\data\B2\standard\legs.rvmat",
            "RD501_Droids\data\B2\standard\torso.rvmat"            
        };
        hiddenSelectionsTextures[]=
        {
            "RD501_Droids\data\B2\standard\Reskinb2_Arms.paa",
            "RD501_Droids\data\B2\standard\Reskinb2_Legs.paa",
            "RD501_Droids\data\B2\gren\b2gren_Torso.paa"
        };
    };
    class RD501_opfor_unit_B2_droid_rocket: RD501_opfor_unit_B2_droid_Super
    {
        displayname = "B2-HA Rocket Battledroid";
        picture = "\RD501_Droids\B2\B2_armor_ui.paa";
        uniformClass= "RD501_opfor_uniform_B2_rocket_armor";
        weapons[]=
        {
            "Aux501_Weaps_B2_rocket_cannon",
            "Put"
        };
        magazines[]=
        {
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3"
        };
        respawnWeapons[]=
        {
            "Aux501_Weaps_B2_rocket_cannon",
            "Put"
        };
        respawnMagazines[]=
        {
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3"
        };
        hiddenSelections[]=
        {
            "camo_arms",
            "legs",
            "torso"
        };
        hiddenSelectionsMaterials[]=
        {
            "RD501_Droids\data\B2\standard\arms.rvmat",
            "RD501_Droids\data\B2\standard\legs.rvmat",
            "RD501_Droids\data\B2\standard\torso.rvmat"            
        };
        hiddenSelectionsTextures[]=
        {
            "RD501_Droids\data\B2\heavy\super_b2_torso.paa",
            "RD501_Droids\data\B2\rocket\rocket_b2_legs.paa",
            "RD501_Droids\data\B2\rocket\rocket_b2_arms.paa"
        };
    };
};