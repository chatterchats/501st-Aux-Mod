#include "../../RD501_main/config_macros.hpp"

class CfgPatches
{
    class RD501_patch_ammo
    {
        author=RD501;
        addonRootClass = MACRO_QUOTE(RD501_patch_weapons);
        requiredAddons[]=
        {
            RD501_patch_weapons
        };
        requiredVersion=0.1;
        units[]={};
        weapons[]={};
    };
};

class CfgAmmo
{
	class 3AS_EC20_BluePlasma;
	class 3AS_EC30_BluePlasma;
	class 3AS_EC40_BluePlasma;
	class 3AS_EC50_BluePlasma;
	class 3AS_EC60_BluePlasma;
	class 3AS_EC70_BluePlasma;
	class 3AS_EC80_BluePlasma;
	class JLTS_bullet_carbine_red;
	class G_40mm_HE;
	class G_40mm_Smoke;
	class 3AS_CoreDetonator_1RND;
	class 3AS_Detonator_1RND;
	class B_12Gauge_Pellets_Submunition;
	class B_12Gauge_Pellets_Submunition_Deploy;
	class FlareBase;
	class SmokeShell;
	class SmokeShellOrange;
	class JLTS_bullet_stun;
	class 442_thermal_det;
	class JLTS_bullet_carbine_orange;
	class JLTS_bullet_carbine_yellow;
	class JLTS_bullet_carbine_blue;
	class FlameRound;

	class macro_new_ammo(40mwemp) : JLTS_bullet_carbine_blue
	{
		hit=0.01;
		airLock=1;
		typicalSpeed=1100;
		caliber=1;
		airFriction=0;
		explosive = 0;
		rd501_emp_vehicle_enabled=1;
		rd501_emp_vehicle_duration=15;
		model = "SWLW_main\Effects\laser_blue.p3d";
		tracerscale = 1.5;
	};
	class macro_new_ammo(devastator_small):JLTS_bullet_carbine_orange
	{
		hit=25;
		airLock=1;
		typicalSpeed=1000;
		caliber=2.4;
		airFriction=0;
		waterFriction=-0.009;
		explosive = 0;
	};
	class macro_new_ammo(devastator_large):JLTS_bullet_carbine_orange
	{
		cost=50;
		caliber=3;
		dangerRadiusBulletClose=16;
		dangerRadiusHit=40;
		explosionAngle=60;
		explosive=0.80000001;
		hit=100;
		effectFlare="FlareShell";
		effectsFire="CannonFire";
		explosionDir="explosionDir";
		explosionEffects="ATMissileExplosion";
		explosionEffectsDir="explosionDir";
		explosionPos="explosionPos";
		explosionType="explosive";
		indirectHit=5;
		indirectHitRange=3;
		muzzleEffect="BIS_fnc_effectFiredRocket";
		tracerScale=3;
	};
	class macro_new_ammo(viper) : 3AS_EC80_BluePlasma
	{
		hit=100;
		thrust=210;
		explosive=0.4;
		thrustTime=1.5;
		airLock=1;
		typicalSpeed=1100;
		caliber=5;
		airFriction=0;
		tracersevery=1;
		tracerScale=1;
		tracerStartTime=0;
		tracerEndTime=10;
		model = "\MRC\JLTS\weapons\Core\effects\laser_orange.p3d";
	};
///////////////////////////////////////////////////////////////////////
///////////////////////////UGL////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
	class RD501_grenade_emp_ammo: G_40mm_HE
    {
		JLTS_isEMPAmmo=1;
		hit = 0.1;
		indirectHit = 0.01;
		indirectHitRange = 12;
		timeToLive =30;
		model = "\MRC\JLTS\weapons\Core\effects\emp_blue.p3d";
		fuseDistance = 1;
		explosive = 0.0001;
		deflecting = 5;
		caliber = 5;
		ace_frag_enabled = 0;
		ace_frag_force = 0;
		ace_frag_classes[] = {""};
		ace_frag_metal = 0;
		ace_frag_charge = 0;
		ace_frag_gurney_c = 0;
		ace_frag_gurney_k = "0";
		ace_rearm_caliber = 0;
		explosionEffects="JLTS_fx_exp_EMP";
		grenadeBurningSound[]=
		{
			"EMPSoundLoop1",
			0.5
		};
		EMPSoundLoop1[]=
		{
			"MRC\JLTS\weapons\Grenades\sounds\grenade_burning.wss",
			0.125893,
			1,
				70
			};
		SoundSetExplosion[]=
		{
			"JLTS_GrenadeEMP_Exp_SoundSet",
			"JLTS_GrenadeEMP_Tail_SoundSet",
			"Explosion_Debris_SoundSet"
		};
		aiAmmoUsageFlags=0;
		class CamShakeExplode
		{
			distance=10;
			duration=1;
			frequency=20;
			power=0;
		};
    };
	class macro_new_ammo(Devastator_dioxis) :G_40mm_Smoke
	{
		hit = 2;
		indirectHit = 1;
		indirectHitRange = 1;
		timeToLive =30;
		model="\3AS\3AS_Equipment\model\3AS_thermaldet.p3d";
		effectsSmoke="RD501_DioxisSmokeShellEffect";
		smokeColor[]={0.21250001,0.75580001,0.35909998,1};
	};
	class macro_new_ammo(Devastator_stun) :G_40mm_HE
	{
		hit = 1;
		indirectHit = 1;
		indirectHitRange = 6;
		timeToLive =30;
		explosionEffects="JLTS_fx_exp_EMP";
		model="\3AS\3AS_Equipment\model\3AS_thermaldet.p3d";
		RD501_stunDuration=30;
	};
///////////////////////////////////////////////////////////////////////
///////////////////////////Grenades///////////////////////////////////
//////////////////////////////////////////////////////////////////////
	
	class macro_new_ammo(dioxis) : SmokeShell
	{
		effectsSmoke="RD501_DioxisSmokeShellEffect";
	};
	class macro_new_ammo(blueshadow) : SmokeShell
	{
		effectsSmoke="RD501_DioxisBlueSmokeShellEffect";
	};

///////////////////////////////////////////////////////////////////////
/////////////////////////Rockets///////////////////////////////////////
//////////////////////////////////////////////////////////////////////
	class ammo_Penetrator_RPG32V;
	class ACE_Javelin_FGM148_static;
	class R_PG32V_F;
	class Mo_cluster_AP;
	class R_TBG32V_F;
	/*class macro_new_ammo(striker):ACE_Javelin_FGM148_static
	{
		hit=1650;
		effectsMissileInit="RocketBackEffectsStaticRPG";
		initTime=0.1;
		irLock = 1;
        laserLock = 0;
        airLock = 0;
		class ace_missileguidance {
            enabled = 1;

            minDeflection = 0.00005;      // Minium flap deflection for guidance
            maxDeflection = 0.025;       // Maximum flap deflection for guidance
            incDeflection = 0.00005;      // The incrmeent in which deflection adjusts.

            canVanillaLock = 0;

            // Guidance type for munitions
            defaultSeekerType = "Optic";
            seekerTypes[] = { "Optic" };

            defaultSeekerLockMode = "LOBL";
            seekerLockModes[] = { "LOBL" };

            seekerAngle = 180;           // Angle in front of the missile which can be searched
            seekerAccuracy = 1;         // seeker accuracy multiplier

            seekerMinRange = 0;
            seekerMaxRange = 2500;      // Range from the missile which the seeker can visually search

            seekLastTargetPos = 1;      // seek last target position [if seeker loses LOS of target, continue to last known pos]

            // Attack profile type selection
            defaultAttackProfile = "JAV_TOP";
            attackProfiles[] = { "JAV_TOP", "JAV_DIR" };
            useModeForAttackProfile = 1;
        };
	};*/
};