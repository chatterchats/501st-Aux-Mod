#include "../../RD501_main/config_macros.hpp"

class CfgPatches
{
    class RD501_patch_magazines
    {
        author=RD501;
        addonRootClass=MACRO_QUOTE(RD501_patch_weapons);
        requiredAddons[]=
        {
            RD501_patch_weapons
        };
        requiredVersion=0.1;
        units[]={};
        weapons[]={};
    };
};

class CfgMagazines
{
	class 3AS_45Rnd_EC50_Mag;
	class 1Rnd_HE_Grenade_shell;
	class JLTS_stun_mag_long;
	class 1Rnd_Smoke_Grenade_shell;
	class UGL_FlareWhite_F;
	class 3AS_ThermalDetonator;
	class 3AS_BaridumCore;
	class 3AS_ThrowableCharge;
	class 2Rnd_12Gauge_Pellets;
	class SmokeShell;
	class 442_thermal_det_mag;
	class Aux501_Weapons_Mags_E5100;
	class DBA_B2_Rocket;
	class DBA_B2_Mag;
	class JLTS_e5c_mag;
	////////////////////////////////////////////////////
	/////////////Rifle/Pistol Magazines/////////////////
	////////////////////////////////////////////////////
	class macro_new_mag(40mwemp,1) : 3AS_45Rnd_EC50_Mag
	{
		displayName="1 Round 40MW EMP Cell";
		count = 1;
		ammo=MACRO_QUOTE(macro_new_ammo(40mwemp));
		initSpeed=1100;
		picture="\RD501_Weapons\_mag\data\rd501_icon_mag_dc15a_20mw.paa";
		descriptionShort="DC15X EMP Round";
		model = "\MRC\JLTS\weapons\DC15x\DC15x_mag.p3d";
	};
	class macro_new_mag(Devastator,50) : 3AS_45Rnd_EC50_Mag
	{
		displayName="Devastator Primary Magazine";
		count = 50;
		mass=4;
		ammo=MACRO_QUOTE(macro_new_ammo(devastator_small));
		initSpeed=1100;
		descriptionShort="High power magazine";
	};
	class macro_new_mag(Devastator,2) : 3AS_45Rnd_EC50_Mag
	{
		displayName="Devastator High Energy Magazine";
		count = 2;
		mass=4;
		ammo=MACRO_QUOTE(macro_new_ammo(devastator_large));
		initSpeed=1100;
		descriptionShort="High power magazine";
	};
	class macro_new_mag(viper,5) : 3AS_45Rnd_EC50_Mag
	{
		displayName="Viper Magazine";
		count = 5;
		mass=4;
		ammo=MACRO_QUOTE(macro_new_ammo(viper));
		initSpeed=1100;
		descriptionShort="High power magazine";
	};
	class macro_new_mag(r2_charge_pack,10):3AS_45Rnd_EC50_Mag
	{
		displayName="R2 Charge Pack";
		descriptionShort = "Charge Pack";
		displayNameShort = "Charge Pack";
		count=10;
		picture="\RD501_Weapons\_mag\data\rd501_icon_mag_battery.paa";
		ammo=MACRO_QUOTE(macro_new_ammo(40mw));
	};

	////////////////////////////////////////////////////
	/////////Grenade Launcher Magazines/////////////////
	////////////////////////////////////////////////////
	class macro_new_mag(UGL_EMP,1) : 1Rnd_HE_Grenade_shell
	{
		ammo = "RD501_grenade_emp_ammo";
		count = 1;
		descriptionShort = "1Rnd EMP DC-15A Grenade";
		displayName = "1 Rnd EMP DC-15A Grenades";
		displayNameShort = "1Rnd EMP DC-15A";
		picture = "\MRC\JLTS\weapons\grenades\data\ui\grenade_emp_ui_ca.paa";
		mass = 50;
		maxLeadSpeed = 25;
		maxThrowHoldTime = 2;
		maxThrowIntensityCoef = 1.4;
		minThrowIntensityCoef = 0.3;
		modelSpecial = "";
		nameSound = "";
		quickReload = 0 ;
		reloadAction = "";
		scope = 2;
		initSpeed = 220;
		selectionFireAnim = "zasleh";
		simulation = "ProxyMagazines";
		type = 16;
		useAction = 0;
		useActionTitle = "";
		value = 1;
		weaponpoolavailable = 1;
		weight = 0;
		class InventoryPlacements;
		class Library 
		{
			libTextDesc = "";
		};
	};
	class macro_new_mag(Devastator_stun,1):1Rnd_HE_Grenade_shell
	{
		displayName = "1 Rd Devastator Stun Magazine";
		displayNameShort = "1 Rnd Devastator Stun Magazine";
		count=1;
		ammo = MACRO_QUOTE(macro_new_ammo(Devastator_stun));
		descriptionShort = "1Rd Devastator Stun Magazine";
	};
	class macro_new_mag(Devastator_dioxis,1):1Rnd_Smoke_Grenade_shell
	{
		displayName = "1 Rd Devastator Dioxis Magazine";
		displayNameShort = "1 Rnd Devastator Dioxis Magazine";
		count=1;
		ammo = MACRO_QUOTE(macro_new_ammo(Devastator_dioxis));
		descriptionShort = "1Rd Devastator Dioxis Magazine";
	};
};