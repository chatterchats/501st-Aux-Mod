
#include "..\RD501_main\config_macros.hpp"

#include "script_macros.hpp"

#define macro_jp_ver 1.1 Endgame
#define name_jumppack_f  JT-21 (Version ##macro_jp_ver##)
class CfgPatches
{
	class RD501_patch_jumppack
	{
		author=DANKAUTHORS;
		requiredAddons[]=
		{
			MACRO_QUOTE(RD501_patch_particle_effects)
		};
		requiredVersion=0.1;
		units[]={

		};
		weapons[]={};
		vehicles[]={
			MACRO_QUOTE(macro_jumppackClass(neutral,base)),
			MACRO_QUOTE(macro_jumppackClass(neutral,rto))
		};
	};
};

class CfgVehicles
{
	class B_AssaultPack_blk;
	class JLTS_Clone_jumppack_mc;
	class JLTS_Clone_jumppack;
	class JLTS_Clone_jumppack_JT12;
	
	class RD501_JLTS_Clone_jumppack_cdv: JLTS_Clone_jumppack_mc
	{
		scope=2;
		displayname = "[501st] Jumppack CDV";
		tf_encryptionCode="tf_west_radio_code";
		tf_subtype="digital_lr";
		tf_range=35000;
		tf_hasLRradio=1;
		maximumLoad=700;
		JLTS_isJumppack = 0;
		class rd501_jumppack {
            rechargeRateSecond = 8;
            capacity = 150;
			allowedJumpTypes[] = {
				"Forward",
				"Short",
				"Dash",
				"Cancel"
			};
			igniteSound = "RD501_Jumppack\sounds\cdv21Start.ogg";
        };
	};
	class RD501_JLTS_Clone_jumppack_mc: JLTS_Clone_jumppack_JT12
	{
		scope=2;
		displayname = "[501st] AB Jumppack 01";
		tf_dialog="JLTS_clone_lr_programmer_radio_dialog";
		tf_dialogUpdate="call TFAR_fnc_updateLRDialogToChannel;";
		tf_encryptionCode="tf_west_radio_code";
		tf_subtype="digital_lr";
		tf_range=35000;
		tf_hasLRradio=1;
		maximumLoad=700;
		JLTS_isJumppack = 0;
		class rd501_jumppack {
            rechargeRateSecond = 8;
            capacity = 150;
			allowedJumpTypes[] = {
				"Forward",
				"Short",
				"Dash",
				"Cancel"
			};
			igniteSound = "RD501_Jumppack\sounds\cdv21Start.ogg";
        };
	};	
	class RD501_JLTS_Clone_droppack : RD501_JLTS_Clone_jumppack_mc
	{
		scope=2;
		displayname= "[501st] JT21 Droppack";
		picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_jumppack_ui_ca.paa";
		model="\MRC\JLTS\characters\CloneArmor\CloneJumppack.p3d";
		hiddenSelections[]=
		{
			"camo1"
		};
		hiddenSelectionsTextures[]=
		{
			"\MRC\JLTS\characters\CloneArmor\data\Clone_jumppack_co.paa"
		};
		maximumLoad=50;
		JLTS_isJumppack = 0;
        class rd501_jumppack {
            rechargeRateSecond = 0;
            capacity = 0;
			allowedJumpTypes[] = {};
			igniteSound = "RD501_Jumppack\sounds\cdv21Start.ogg";
        };
	};
	class RD501_JLTS_Clone_jumppack_mc_2 : RD501_JLTS_Clone_jumppack_mc {
		displayname = "[501st] AB Jumppack 02";
		hiddenSelectionsTextures[] = { "\MRC\JLTS\characters\CloneLegions\data\Clone_501stTrooper_jumppack_JT12_co.paa" };
	};

	class RD501_JLTS_Clone_jumppack_mc_hq_1 : RD501_JLTS_Clone_jumppack_mc {
		displayname = "[501st] AB Jumppack (HQ 1)";
		hiddenSelectionsTextures[] = { "RD501_Jumppack\textures\acklay_skin_set\JT-12_1HQ.paa" };
	};

	class RD501_JLTS_Clone_jumppack_mc_1_1 : RD501_JLTS_Clone_jumppack_mc {
		displayname = "[501st] AB Jumppack (1-1)";
		hiddenSelectionsTextures[] = { "RD501_Jumppack\textures\acklay_skin_set\JT-12_1-1.paa" };
	};

	class RD501_JLTS_Clone_jumppack_mc_1_2 : RD501_JLTS_Clone_jumppack_mc {
		displayname = "[501st] AB Jumppack (1-2)";
		hiddenSelectionsTextures[] = { "RD501_Jumppack\textures\acklay_skin_set\JT-12_1-2.paa" };
	};

	class RD501_JLTS_Clone_jumppack_mc_1_3 : RD501_JLTS_Clone_jumppack_mc {
		displayname = "[501st] AB Jumppack (1-3)";
		hiddenSelectionsTextures[] = { "RD501_Jumppack\textures\acklay_skin_set\JT-12_1-3.paa" };
	};

	class RD501_JLTS_Clone_jumppack_mc_MED : RD501_JLTS_Clone_jumppack_mc {
		displayname = "[501st] AB Jumppack MED";
		maximumLoad=1000;
		hiddenSelectionsTextures[] = { "RD501_Jumppack\textures\JT-12_MED.paa" };
	};

	class RD501_JLTS_Clone_jumppack_mc_RTO : RD501_JLTS_Clone_jumppack_mc {
		displayname = "[501st] AB Jumppack RTO";
		hiddenSelectionsTextures[] = { "RD501_Jumppack\textures\JT-12_RTO.paa" };
	};

	class RD501_JLTS_Clone_jumppack_mc_INS : RD501_JLTS_Clone_jumppack_mc {
		displayname = "[501st] AB Jumppack Instructor";
		hiddenSelectionsTextures[] = { "RD501_Jumppack\textures\JT-12_PG_INS.paa" };
		class rd501_jumppack {
            rechargeRateSecond = 100;
            capacity = 300;
			allowedJumpTypes[] = {
				"Forward",
				"Short",
				"Dash",
				"Cancel"
			};
			igniteSound = "RD501_Jumppack\sounds\cdv21Start.ogg";
        };
	};

	class RD501_JLTS_Clone_jumppack_cdv_avi : RD501_JLTS_Clone_jumppack_cdv {
		displayname = "[501st] CDV - AVI Jumppack";
		hiddenSelectionsTextures[] = { "RD501_Jumppack\textures\razor.paa" };
	};

	class RD501_JLTS_Clone_jumppack_cdv_ltng : RD501_JLTS_Clone_jumppack_cdv {
		displayname = "[501st] CDV - LTNG Jumppack";
		hiddenSelectionsTextures[] = { "RD501_Jumppack\textures\lightning.paa" };
	};

	class RD501_JLTS_Clone_jumppack_cdv_bn : RD501_JLTS_Clone_jumppack_cdv
	{
		displayname = "[501st] CDV - BN Jumppack";
		hiddenSelectionsTextures[] = { "RD501_Jumppack\textures\cdv_hailstorm.paa" };
	};
	class RD501_JLTS_Clone_jumppack_cdv_bnm : RD501_JLTS_Clone_jumppack_cdv
	{
		displayname = "[501st] CDV - BN Med Jumppack";
		maximumLoad=1000;
		hiddenSelectionsTextures[] = { "RD501_Jumppack\textures\cdv_hailstorm.paa" };
	};
};