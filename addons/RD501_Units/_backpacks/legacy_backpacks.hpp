//POOPY INFANTRY RTO BACKPACK
//Why is infantry so weird.

	
class RD501_JLTS_Clone_backpack: JLTS_Clone_backpack
{
	maximumload=700;
	Displayname="[OLD] Combat Pack (Base)";
	hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\missing_co.paa"};
};
class RD501_JLTS_Clone_backpack_s: JLTS_Clone_backpack_s
{
	maximumload=700;
	Displayname="[OLD] Combat Pack (Straps)";
	hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\missing_co.paa"};
};
class RD501_JLTS_Clone_backpack_invisible: JLTS_Clone_backpack_s
{
	maximumload=700;
	Displayname="[OLD] Combat Invisible Backpack";
	model="RD501_Units\nothing.p3d";
};
class RD501_JLTS_Clone_backpack_medic: JLTS_Clone_backpack_medic
{
	maximumload=1000;
	Displayname="[OLD] Combat Pack (Medical)";
	hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\missing_co.paa"};
};
class RD501_JLTS_Clone_backpack_eod: JLTS_Clone_backpack_eod
{
	maximumload=700;
	Displayname="[OLD] Combat Pack (EOD)";
	hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\missing_co.paa"};
};
class RD501_JLTS_Clone_backpack_bomb: JLTS_Clone_backpack_bomb
{
	maximumload=700;
	Displayname="[OLD] Combat Pack (Engineer)";
	hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\missing_co.paa"};
};
class RD501_JLTS_Clone_backpack_RTO: JLTS_Clone_backpack_RTO
{
	maximumload=700;
	tf_range=35000;
	Displayname="[OLD] Radio Pack (Large)";
	hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\missing_co.paa"};
};
class RD501_JLTS_Clone_backpack_s_RTO: JLTS_Clone_backpack_s_RTO
{
	maximumload=700;
	tf_range=35000;
	Displayname="[OLD] Radio Pack (Straps)";
	hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\missing_co.paa"};
};
class RD501_JLTS_Clone_belt_bag: JLTS_Clone_belt_bag
{
	maximumload=700;
	Displayname="[OLD] Belt Bag";
	hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\missing_co.paa"};
};
class RD501_JLTS_Clone_RTO_pack: JLTS_Clone_RTO_pack
{
	maximumload=700;
	tf_range=35000;
	Displayname="[OLD] Radio Pack (Small)";
	hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\missing_co.paa"};
};
class RD501_JLTS_Clone_RTO_pack_blue: JLTS_Clone_RTO_pack
{
	maximumload=700;
	tf_range=35000;
	Displayname="[OLD] Radio Pack (Small)";
	hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\missing_co.paa"};
};
class RD501_JLTS_Clone_RTO_pack_MED: JLTS_Clone_RTO_pack
{
	maximumload=1000;
	tf_range=35000;
	Displayname="[OLD] Radio Pack (Medical)";
	hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\missing_co.paa"};
};
class RD501_JLTS_Clone_LR_attachment: JLTS_Clone_LR_attachment
{
	maximumload=700;
	tf_range=35000;
	Displayname="[OLD] Radio Pack (Attachment)";
	hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\missing_co.paa"};
};
class RD501_JLTS_Clone__invis_LR_attachment: JLTS_Clone_LR_attachment
{
	maximumload=700;
	tf_range=35000;
	Displayname="[OLD] Command Invisible Pack";
	model="RD501_Units\nothing.p3d";
	hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\missing_co.paa"};
};
class RD501_JLTS_Clone_ARC_backpack: JLTS_Clone_ARC_backpack
{
	maximumload=700;
	tf_range=35000;
	Displayname="[501st] Specialist Pack (ARC)";
	hiddenSelectionsTextures[] = 
	{
		"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\missing_co.paa"
		
	};
};
class RD501_JLTS_Clone_Flip_backpack: JLTS_Clone_backpack
{
	maximumload = 0;
	Displayname="[OLD] Grav Lift Pack";
	hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\missing_co.paa"};
};
class lsd_gar_marine_backpack;
class RD501_LS_snow_backpack: lsd_gar_marine_backpack
{	
	maximumload = 700;
	Displayname = "[OLD] Snow Pack";
	hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\missing_co.paa"};
};
class RD501_LS_snow_backpack_LR: lsd_gar_marine_backpack
{	
	maximumload = 700;
	Displayname = "[OLD] Snow Pack (LR)";
	tf_dialog = "JLTS_clone_rto_radio_dialog";
	tf_dialogUpdate = "call TFAR_fnc_updateLRDialogToChannel;";
	tf_encryptionCode = "tf_west_radio_code";
	tf_hasLRradio = 1;
	tf_range=35000;
	tf_subtype = "digital_lr";
	hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\missing_co.paa"};
};