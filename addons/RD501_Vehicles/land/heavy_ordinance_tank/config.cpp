//Get this addons macro

//get the macro for the air RD501_patch_vehicles

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

//General name of the vehicle
#define vehicle_addon heavy_ordinance_tank
#define patch_name MODNAME##vehicle_addon##_Patches
#define vehicle_classname MODNAME##_##vehicle_addon

#define new_hot_class(name) vehicle_classname##_##name

class CfgPatches
{
	class RD501_patch_heavy_ordinance_tank
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			macro_new_vehicle(heavy_ordinance_tank,CIS)
		};
		weapons[]=
		{
			
		};
	};
};


#include "../../common/sensor_templates.hpp"
class DefaultEventhandlers ;

class CfgVehicles
{
	class Land;
	class LandVehicle: Land
	{
		class NewTurret;
	};
	class Tank: LandVehicle
	{
		class NewTurret;
		class Sounds;
		class HitPoints;
		class CommanderOptics;
	};
	class Tank_F: Tank
	{
		class Turrets
		{
			class MainTurret: NewTurret
			{
				class Turrets
				{
					class CommanderOptics;
				};
				class Components;
			};
		};
	};
	class 442_baw_arty_base: tank_f
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				class Turrets{};
			};
		};
	};
	class macro_new_vehicle(heavy_ordinance_tank,CIS) : 442_baw_arty_base
	{
		displayName = "CIS Trebuchet";
		crew=MACRO_QUOTE(macro_new_unit_class(opfor,B1_crew));
		scope = 2;
		side=0;
		scopeCurator=2;
		hiddenSelectionsTextures[] = {"ls_vehicles_ground\bawhag\data\body_co.paa","ls_vehicles_ground\bawhag\data\eyes_co.paa","ls_vehicles_ground\bawhag\data\gun_co.paa"};
		icon = "\ls_vehicles_ground\_ui\icons\aat_icon.paa";
		faction = MACRO_QUOTE(macro_faction(CIS));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(arty));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(arty));
		author = "RD501";
		forceInGarage=1;
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				weapons[] = {"RD501_CIS_155mm_howitzer"};
				magazines[] = 
				{
					"RD501_32Rnd_155mm_cis_he_mag",
					"RD501_32Rnd_155mm_cis_he_mag",
					"RD501_32Rnd_155mm_cis_he_mag",
					"2Rnd_155mm_Mo_guided",
					"2Rnd_155mm_Mo_guided",
					"6Rnd_155mm_Mo_mine",
					"2Rnd_155mm_Mo_Cluster",
					"6Rnd_155mm_Mo_smoke",
					"2Rnd_155mm_Mo_LG",
					"6Rnd_155mm_Mo_AT_mine"
				};
			};
		};
	};
};