#include "../../../RD501_main/config_macros.hpp"

class CfgPatches
{
	class RD501_patch_n99
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			macro_new_vehicle(cis,n99)
		};
		weapons[]=
		{
			
		};
	};
};
class CfgVehicles
{
	
    class 3AS_N99_base_F;
	class macro_new_vehicle(cis,n99): 3AS_N99_base_F
	{
        scope = 2;
		scopeCurator = 2;
		displayName = "CIS NR-N99 Snail Tank";
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(tank));
		vehicleClass = MACRO_QUOTE(macro_editor_vehicle_type(tank));
		side = 0;
		faction = MACRO_QUOTE(macro_faction(CIS));
		crew = MACRO_QUOTE(macro_new_unit_class(opfor,B1_crew));
        smokeLauncherAngle = 120;
        armor = 250;
    };
};