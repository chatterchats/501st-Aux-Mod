#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"


class CfgPatches
{
	class RD501_patch_eweb_weapons
	{
		author=DANKAUTHORS;
		addonRootClass= MACRO_QUOTE(RD501_patch_vehicle_weapons);
		requiredAddons[]=
		{
			"RD501_patch_vehicle_weapons",
			"A3_Weapons_F"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
			"RD501_eweb_blaster"
		};
	};
};
class CfgWeapons
{
	class MGun;
    class LMG_RCWS: MGun
	{
		class manual: Mgun{};
		class close: manual{};
		class short: close{};
		class medium: close{};
		class far: close{};
	};

    class RD501_eweb_blaster: LMG_RCWS
	{
		displayName = "Repeating Blaster Cannon";
		magazineReloadTime = 5;
		ballisticsComputer = 2;
		aiDispersionCoefY = 0.5;
		aiDispersionCoefX = 0.5;
		fireLightDiffuse[] = {1,0,0};
        fireLightAmbient[] = {1,0,0};
		magazines[] = {"Aux501_Weapons_Mags_CIS_eweb"};
		muzzleEnd = "konec hlavne";
		muzzlePos = "usti hlavne";
		selectionFireAnim = "zasleh";
		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";
		class GunParticles
		{
			class effect1
			{
				positionName = "konec hlavne2";
				directionName = "Usti hlavne2";
				effectName = "MachineGunCloud";
			};
		};
		class manual: manual
		{
			displayName = "Heavy Repeater";
			sounds[] = {"StandardSound"};
			class BaseSoundModeType
			{
				closure1[] = {"A3\sounds_f\weapons\gatling\gatling_rotation_short_2",0.316228,1,20};
				closure2[] = {"A3\sounds_f\weapons\gatling\gatling_rotation_short_3",0.316228,1,20};
				soundClosure[] = {"closure1",0.5,"closure2",0.5};
			};
			class StandardSound: BaseSoundModeType
			{
				begin1[] = {"DBA_CIS\Addons\DBA_Vehicles\DBA_GroundVehicles\DBA_EWEB\eweb.ogg",0.95,1,2000};
				soundBegin[] = {"begin1",1};
			};
			soundContinuous = 0;
			soundBurst = 0;
			multiplier = 1;
			reloadTime = 0.1;
			dispersion = 0.0115;
			aiRateOfFire = 1;
			aiRateOfFireDistance = 0;
			minRange = 0;
			minRangeProbab = 0.01;
			midRange = 50;
			midRangeProbab = 0.01;
			maxRange = 100;
			maxRangeProbab = 0.01;
			recoil = "recoil_auto_mk200";
			recoilProne = "recoil_auto_prone_mk200";
		};
		class close: manual
		{
			soundBurst = 0;
			aiBurstTerminable = 1;
			showToPlayer = 0;
			burst = 12;
			burstRangeMax = 42;
			aiRateOfFire = 0.5;
			aiRateOfFireDispersion = 1;
			aiRateOfFireDistance = 0;
			minRange = 0;
			minRangeProbab = 0.7;
			midRange = 0;
			midRangeProbab = 0.75;
			maxRange = 300;
			maxRangeProbab = 0.2;
		};
		class short: close
		{
			aiBurstTerminable = 1;
			showToPlayer = 0;
			burst = 8;
			burstRangeMax = 36;
			aiRateOfFire = 1;
			aiRateOfFireDispersion = 2;
			aiRateOfFireDistance = 0;
			minRange = 0;
			minRangeProbab = 0.75;
			midRange = 300;
			midRangeProbab = 0.75;
			maxRange = 600;
			maxRangeProbab = 0.2;
		};
		class medium: close
		{
			aiBurstTerminable = 1;
			showToPlayer = 0;
			burst = 8;
			burstRangeMax = 30;
			aiRateOfFire = 2;
			aiRateOfFireDispersion = 2;
			aiRateOfFireDistance = 0;
			minRange = 0;
			minRangeProbab = 0.75;
			midRange = 600;
			midRangeProbab = 0.65;
			maxRange = 800;
			maxRangeProbab = 0.1;
		};
		class far: close
		{
			aiBurstTerminable = 1;
			showToPlayer = 0;
			burst = 8;
			burstRangeMax = 12;
			aiRateOfFire = 4;
			aiRateOfFireDispersion = 4;
			aiRateOfFireDistance = 800;
			minRange = 0;
			minRangeProbab = 0.65;
			midRange = 1000;
			midRangeProbab = 0.3;
			maxRange = 1500;
			maxRangeProbab = 0.05;
		};
		drySound[] = {"A3\Sounds_F\arsenal\weapons_vehicles\LMG_Minigun_65mm\LMGMinigun65mm_dry",1,1,10};
	};
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_EWHB12_MG;

    class Aux501_Weapons_Mags_CIS_eweb: Aux501_Weapons_Mags_EWHB12_MG
    {
        displayname = "[501st] 250Rnd EWEB Cell";
        displayNameShort = "250Rnd 30MW";
        descriptionShort = "CIS Repeating Blaster Cell";
        count = 250;
        ammo = "Aux501_Weapons_Ammo_cis_eweb";
    };
};
class CfgAmmo
{
    class Aux501_Weapons_Ammo_base_red;
	class Aux501_Weapons_Ammo_cis_eweb: Aux501_Weapons_Ammo_base_red
	{
		hit=20;
		airLock=1;
		typicalSpeed=700;
	};
};

	