/*
 * Author: M3ales, originally Namenai (I think)
 *
 * Arguments:
 * Vehicle
 * Vehicle the action is being performed inside of/on.
 * RGBA
 * The the colour specified as an array [red, green, blue, alpha], with all values in the range 0-1.
 *
 * Return Value:
 * Nothing
 *
 * Example:
 * [vehicle player, [1, 0.5, 0.5, 1]] call rd501_mfd_colours_setColour
 *
 * Public: No
 */

#include "function_macros.hpp"
params["_vehicle","_rgba"];

// Was originally in /RD501_Main/functions/hud_color_change.sqf
(_vehicle) setUserMFDvalue [0, _rgba select 0];
(_vehicle) setUserMFDvalue [1, _rgba select 1];
(_vehicle) setUserMFDvalue [2, _rgba select 2];
(_vehicle) setUserMFDvalue [3, _rgba select 3];