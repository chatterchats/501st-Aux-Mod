#include "functions\function_macros.hpp"

LOG("PreInit Begin");
private _addonConfig = configFile >> "CfgPatches" >> QUOTE(ADDON);
private _version = getArray(_addonConfig >> "version");
LOG(format["Version: %1", _verison]);

LOG("PREP Begin");
#include "XEH_PREP.sqf"
LOG("PREP Complete");

LOG("Searching for vehicle types");
private _propertyName = QGVAR(enabled);
private _enabledSearchCondition = format["getNumber (_x >> '%1') > 0", _propertyName];
GVAR(validTypes) = _enabledSearchCondition configClasses (configFile >> "CfgVehicles");
LOGF_1("Read %1 types from CfgVehicles", count GVAR(validTypes));

private _configSpecified = getArray(_addonConfig >> "externalVehicles");
LOGF_1("Read %1 types from externalVehicles", count _configSpecified);

GVAR(validTypes) append _configSpecified;

{
	diag_log format["%1: %2", _foreachIndex, _x];
} forEach(GVAR(validTypes));

private _validTypesCount = count GVAR(validTypes);
if(_validTypesCount > 0) then {
	LOGF_1("Enabled for %1 vehicle types", _validTypesCount);
}else {
	LOG_ERRORF_1("No vehicles specified on externalVehicles, or in CfgVehicles with %1 were found. This may be an error, or you don't like using this feature. Maybe you add some more later, so we'll still continue.",QGVAR(enabled));
};

// Vehicle MFD/Colour changes
[QGVAR(custom), "COLOR", ["Custom Hud Colour", "Its a colour, pick."], ["RD501", "Vehicle HUD Colour"], [0.5,0.5,0.5], 2, {}, false] call CBA_fnc_addSetting;
[QGVAR(custom_alpha), "SLIDER", ["Custom Hud Alpha", "Its transparency, pick."], ["RD501", "Vehicle HUD Colour"], [0, 1, 1, 4], 2, {}, false] call CBA_fnc_addSetting;

LOG("PreInit Complete");