params ["_args", "_handle"];
_args params [
	["_node", objNull],
	["_pos", [0, 0, 0]],
	["_color", [0, 0, 0]],
	["_active", false],
	["_light", objNull],
	["_index", 0],
	["_maxIndex", 0],
	["_counter", 0],
	["_brightness", 100],
	["_intensity", 1000],
	["_daylightMultiplier", 100]
];

if (isNull _node or not (alive _node) or not (profileNamespace getVariable ["RD501_UTIL_LightsEnabled", true])) exitWith {
	if (not (isNull _light)) then {
		deleteVehicle _light;
	};

	[_handle] call CBA_fnc_removePerFrameHandler;
};

_prevCounter = _counter;

_counter = _counter + 1;
if (_counter >= _maxIndex) then {
	_counter = 0;
};

_args set [7, _counter];

if (not (_prevCounter isEqualTo _index)) exitWith {};

if (isNull _light) then {
	_light = "#lightpoint" createVehicleLocal _pos;

	_light setLightColor _color;
	_light setLightAmbient _color;
	_light setLightFlareSize 1;
	_light setLightFlareMaxDistance 250;
	_light setLightDayLight true;
	_light setLightUseFlare true;
};

_time = daytime;
_dayTime = _time > 6 and _time < 18;

if (_active) then {
	_active = false;
	
	_light setLightBrightness 0;
	_light setLightIntensity 0;
} else {
	_active = true;

	if (_dayTime) then {
		_light setLightBrightness _brightness * _daylightMultiplier;
		_light setLightIntensity _intensity * _daylightMultiplier;
	} else {
		_light setLightBrightness _brightness;
		_light setLightIntensity _intensity;
	};
};

_args set [3, _active];
_args set [4, _light];
