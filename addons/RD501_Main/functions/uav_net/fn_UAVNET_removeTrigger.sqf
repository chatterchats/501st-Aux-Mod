#include "..\loglevel.hh"

// Removes a UAV net.
params [
	// If letf null, all nets will be removed.
	["_netName", objNull]
];

if (isServer) then {
	private _spawnObjectsSet = localNamespace getVariable "RD501_UAVNET_additionalSpawnsRaw";

	private _runSet = [];

	if (not (_netName isEqualType "")) then {
		{
			_runSet pushBack _x;
		} forEach _spawnObjectsSet;
	} else {
		_runSet pushBack _netName;
	};

	[["Running removal operation for", _runSet] joinString " ", LOG_DEBUG, "UAVNET"] call RD501_fnc_logMessage;
	
	{
		private _spawnObjects = _spawnObjectsSet getOrDefault [_x, []];
		private _additionalSpawns = [];
		{
			private _type = typeOf _x;
			private _pos = getPosATL _x;
			private _rot = getDir _x;

			_additionalSpawns pushBack [_type, _pos, _rot];

			deleteVehicle _x;
		} forEach _spawnObjects;
		
		_spawnObjectsSet set [_x, []];

		if ((count _spawnObjects) > 0) then {
			private _additionalSpawnsSet = localNamespace getVariable "RD501_UAVNET_additionalSpawns";
			_additionalSpawnsSet set [_x, _additionalSpawns];
		};

		private _triggerSet = localNamespace getVariable "RD501_UAVNET_triggerObject";
		private _trigger = _triggerSet getOrDefault [_x, objNull];
		if (not (isNull _trigger)) then {
			deleteVehicle _trigger;
		};

		_triggerSet set [_x, objNull];
	} forEach _runSet;
};