#include "..\loglevel.hh"

private _diag = findDisplay 501001;
private _defaultsSelector = _diag displayCtrl 005;

private _defaults = profileNamespace getVariable "RD501_TEXSEL_DefaultTextureConfigurations";
if (isNil '_defaults') then {
	_defaults = createHashMap;
	profileNamespace setVariable ["RD501_TEXSEL_DefaultTextureConfigurations", _defaults];
};

private _name = _defaultsSelector lbText (lbCurSel _defaultsSelector);
private _class = typeOf (vehicle player);

[["Found name:", _name, "within class:", _class, "to remove from default options."] joinString " ", LOG_INFO, "TEXSEL"] call RD501_fnc_logMessage;

if (_class in _defaults) then {
	private _group = _defaults get _class;
	private _dataSet = _group select { (_x select 0) isEqualTo _name };

	[["Gathered dataset", _dataSet, "for group", _group], LOG_DEBUG, "TEXSEL"] call RD501_fnc_logMessage;

	if ((count _dataSet) > 0) then {
		private _data = _dataSet select 0;
		_group = _group select { not ((_x select 0) isEqualTo _name) };

		_defaults set [_class, _group];

		[["Removed", _data, "from group", _group], LOG_DEBUG, "TEXSEL"] call RD501_fnc_logMessage;

		lbClear _defaultsSelector;

		{
			_defaultsSelector lbAdd (_x select 0);
		} forEach _group;
	};
};
