#include "..\loglevel.hh"

private _diag = findDisplay 501001;
private _groupSelector = _diag displayCtrl 001;
private _textureSelector = _diag displayCtrl 002;

private _textures = localNamespace getVariable ["RD501_TEXSEL_EligibleTextureConfigurations", createHashMap];

private _group = _groupSelector lbText (lbCurSel _groupSelector);
private _name = _textureSelector lbText (lbCurSel _textureSelector);

private _groupData = _textures getOrDefault [_group, []];
private _textureDataIndex = _groupData findIf { (_x select 0) == _name };

if (_textureDataIndex == -1) exitWith {
	[["No texture data found for", _group, _name] joinString " ", LOG_ERROR, "TEXSEL"] call RD501_fnc_logMessage;
};

private _textureData = _groupData select _textureDataIndex;
private _vic = vehicle player;

if (isNull _vic) exitWith {
	["No vehicle to change the texture for.", LOG_ERROR, "TEXSEL"] call RD501_fnc_logMessage;
};

{
	if (not (_x isEqualTo "")) then {
		_vic setObjectTextureGlobal [_forEachIndex, _x];
	};
} forEach (_textureData select 1);
