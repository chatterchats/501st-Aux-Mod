#include "..\loglevel.hh"

// Example call
// [launch_pad_one, 180, 50, 750, -15, 5, 500] call RD501_fnc_internal_CAT_launch;

params ["_pad", "_dir", "_speed", "_launchSpeed", "_launchRot", "_launchHeight", "_maxDist", "_hoverCycles", "_sleepTimer", "_vic"];

[["Attempting launch with params:", _pad, _dir, _speed, _launchSpeed, _launchRot, _launchHeight] joinString " ", LOG_DEBUG, "CATAPULT"] call RD501_fnc_logMessage;

// [_vic, _dir] call BIS_fnc_aircraftCatapultLaunch;

// Set the defaults.
localNamespace setVariable ["RD501_CAT_launchSettings", _this];

// Start the launch process.
[["Found vic:", _vic] joinString " ", LOG_DEBUG, "CATAPULT"] call RD501_fnc_logMessage;

_vic setDir _dir;

[["Rotated vic to:", _dir] joinString " ", LOG_DEBUG, "CATAPULT"] call RD501_fnc_logMessage;

private _alt = getPosASL _vic;
_alt = _alt vectorAdd [0, 0, _launchHeight];
_vic setPosASL _alt;

[["Set vic position:", _alt] joinString " ", LOG_DEBUG, "CATAPULT"] call RD501_fnc_logMessage;

// Do funky hold position thing.
private _sin = sin _dir;
private _cos = cos _dir;
private _vertSin = sin _launchRot;

// Build the arg list.
private _args = [
	_speed,
	_launchSpeed,
	_launchRot,
	_maxDist,
	_hoverCycles,
	_vic,
	_alt,
	_sin,
	_cos,
	_vertSin,
	getPosASL _pad
];

[{
	ARG_HOVER_CYCLES = 4;

	params ["_args", "_handle"];
	_args params ["_speed", "_launchSpeed", "_launchRot", "_maxDist", "_hoverCycles", "_vic", "_alt", "_sin", "_cos", "_vertSin", "_padPos"];

	if (_hoverCycles > 0) exitWith {
		_vic setPosASL _alt;

		_vic setVectorDir [
			_sin,
			_cos,
			0
		];

		[_vic, _launchRot, 0] call BIS_fnc_setPitchBank;

		_args set [ARG_HOVER_CYCLES, _hoverCycles - 1];
	};

	if ((speed _vic <= _launchSpeed) and (((getPosASL _vic) distance _padPos) <= _maxDist)) exitWith {
		private _vel = velocity _vic;
		// private _vDir = vectorDir _vic;
		// private _vUp = vectorUp _vic;

		_vic setVectorDir [
			_sin,
			_cos,
			0
		];

		[_vic, _launchRot, 0] call BIS_fnc_setPitchBank;

		_vic setVelocity [
			(_vel select 0) + _sin * _speed,
			(_vel select 1) + _cos * _speed,
			(_vel select 2) + _vertSin * _speed
		];

		[["Pushed vic to:", getPos _vic] joinString " ", LOG_TRACE, "CATAPULT"] call RD501_fnc_logMessage;
	};

	[_handle] call CBA_fnc_removePerFrameHandler;
}, _sleepTimer, _args] call CBA_fnc_addPerFrameHandler;