#include "..\loglevel.hh"

// Handles the man fired event.

params ["_unit", "_weapon", "_muzzle", "_node", "_ammo", "_magazine", "_projectile"];

private _config = configFile >> "CfgAmmo" >> _ammo;
private _deployable = getNumber (_config >> "rd501_fired_script_enabled");

if (_deployable isEqualTo 1) then {
	private _script = getText (_config >> "rd501_fired_script");

	if ((isNil "_script") or (_script isEqualTo "")) then {
		["RD501 Fired script was enabled but no script was provided in the config.", LOG_WARN, "EVNT"] call RD501_fnc_logMessage;
	} else {
		_this call (compile _script);
	};
};