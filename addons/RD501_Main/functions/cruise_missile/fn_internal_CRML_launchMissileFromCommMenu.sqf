#include "..\loglevel.hh"

// Launches a missile at the requested support target.
params ["_args", "_type"];

_args params ["_caller", "_pos", "_target", "_is3D", "_id"];

private _trueTarget = objNull;

if (isNull _trueTarget and not (((_pos select 0) == 0) and ((_pos select 1) == 0))) then {
	_trueTarget = "HeliHEmpty" createVehicle _pos;
};

if (isNull _trueTarget) then {
	[["Failed to find a valid target for the", _type, "missile support request."] joinString  " ", LOG_WARN, "CRML COMM MENU"] call RD501_fnc_logMessage;
	hint "No Target Found\nTry using a laser designator or your map.";
};

switch (_type) do {
	case "cruise": {
		[_caller, _trueTarget, "RD501_Cruise_Missile_Ammo_Base", {}, [true, side _caller, "Cruise Missile"], true] call RD501_fnc_CRML_launchMissile;
		["Launched cruise missile from vanilla support action.", LOG_INFO, "CRML COMM MENU"] call RD501_fnc_logMessage;
	};
	case "mash": {
		[_caller, _trueTarget, side _caller] call RD501_fnc_CRML_launchMashMissile;
		["Launched MASH missile from vanilla support action.", LOG_INFO, "CRML COMM MENU"] call RD501_fnc_logMessage;
	};
	default {
		[["The passed type:", _type, "is not configured."] joinString  " ", LOG_WARN, "CRML COMM MENU"] call RD501_fnc_logMessage;
	};
};
