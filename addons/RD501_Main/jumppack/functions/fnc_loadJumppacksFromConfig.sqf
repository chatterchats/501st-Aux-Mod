/*
 * Author: M3ales
 * Loads jumppacks from config and logs the information.
 *
 * Arguments:
 * _jumpTypes: List of jump types that are loaded. <ARRAY>
 *
 * Return Value:
 * _store: List of backpacks with jumppack configs. <ARRAY>
 *
 * Example:
 * [["rd501_test_jump"]] call rd501_jumppack_fnc_loadJumppacksFromConfig;
 *
 * Public: Yes
 */

#include "function_macros.hpp"

params["_jumpTypes"];

private _classes = configProperties [
	configFile >> "CfgVehicles",
	QUOTE(isClass(_x) && {
		isClass(_x >> QUOTE(QUOTE(ADDON)))
	} && {
		[ARR_2(_x, QUOTE(QUOTE(Bag_Base)))] call FUNC(inheritsFromConfig)
	}),
	true
];

private _store = [];

{
	private _config = _x >> QUOTE(ADDON);
	private _capacity = [_config, "capacity", 0] call BIS_fnc_returnConfigEntry;
	private _rechargeRate = [_config, "rechargeRateSecond", 0] call BIS_fnc_returnConfigEntry;
	private _displayName = [_x, "displayName", configName _x] call BIS_fnc_returnConfigEntry;
	private _picture = [_x, "picture", ""] call BIS_fnc_returnConfigEntry;
	private _allowedJumpTypes = [_config, "allowedJumpTypes", []] call BIS_fnc_returnConfigEntry;

	LOGF_2("Found %1 allowed jump types :: %2", count _allowedJumpTypes, str _allowedJumpTypes);
	_allowedJumpTypes = _allowedJumpTypes apply {
		private _jumpTypeIdentifier = _x;
		private _jumpTypeIndex = _jumpTypes findIf {
			_jumpTypeIdentifier isEqualTo (_x select 0)
		};
		if (_jumpTypeIndex isEqualTo -1) then {
			LOG_ERRORF_1("Could not find a matching jump type for identifier %1", str _jumpTypeIdentifier);
		};
		LOGF_2("Mapped jump type '%1' to index %2", _jumpTypeIdentifier, _jumpTypeIndex);
		_jumpTypeIndex
	};
	_allowedJumpTypes = _allowedJumpTypes - [-1];

	_store pushBack [configName _x, _capacity, _rechargeRate, _displayName, _picture, _allowedJumpTypes];
} forEach(_classes);

LOGF_1("Found %1 backpacks with jumppack configs", count _store);

{
	LOGF_1("%1", _x select 0);
} forEach _store;

_store
