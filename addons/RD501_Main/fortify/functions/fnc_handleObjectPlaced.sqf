#include "function_macros.hpp"
params ["_player", "_side", "_objectPlaced"];
if!(local _player) exitWith {};
if (GVAR(useAmmo)) then
{
	LOGF_1("Player placed %1, removing currency item", _objectDeleted);
	[QGVAR(removeAmmo), [_player]] call CBA_fnc_localEvent;
};
