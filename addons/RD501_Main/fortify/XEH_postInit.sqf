#include "functions\function_macros.hpp"

// Eventhandlers to be executed for acting player only
[QGVAR(addAmmo), {
    player addItem GVAR(currencyItem);
}] call CBA_fnc_addEventHandler;

[QGVAR(removeAmmo), {
    player removeItem GVAR(currencyItem);
}] call CBA_fnc_addEventHandler;

// Register EventHandler for ACEX Fortify Events
[FUNC(deployHandler)] call acex_fortify_fnc_addDeployHandler;
["acex_fortify_objectPlaced", FUNC(handleObjectPlaced)] call CBA_fnc_addEventHandler;
["acex_fortify_objectDeleted", FUNC(handleObjectDeleted)] call CBA_fnc_addEventHandler;

// Cater for future release of ACE where they update the name
[FUNC(deployHandler)] call ace_fortify_fnc_addDeployHandler;
["ace_fortify_objectPlaced", FUNC(handleObjectPlaced)] call CBA_fnc_addEventHandler;
["ace_fortify_objectDeleted", FUNC(handleObjectDeleted)] call CBA_fnc_addEventHandler;

[GVAR(placeableObjects)] call FUNC(registerPreset);
